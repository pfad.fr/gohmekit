package fritzbox

import (
	"errors"
	"sync/atomic"
	"time"

	"code.pfad.fr/fritzgox/aha"
	"code.pfad.fr/gohmekit/hapip/characteristic"
	"code.pfad.fr/gohmekit/hapip/characteristic/service"
	"code.pfad.fr/gohmekit/hapip/characteristic/service/accessory"
)

func NewDevices(client *aha.Client) (map[string]accessory.Basic, func() (*aha.DeviceListInfos, error), error) {
	infos, err := client.GetDeviceListInfos()
	if err != nil {
		return nil, client.GetDeviceListInfos, err
	}
	acc := make(map[string]accessory.Basic, len(infos.Devices))
	callbacks := make(map[string][]func(aha.Device))
	for _, device := range infos.Devices {
		id := device.Identifier
		a := accessory.Basic{
			Identify: func(bool) error {
				return errors.New("identify not implemented")
			},
			Manufacturer:     device.Manufacturer,
			Model:            device.Productname,
			Name:             device.Name,
			SerialNumber:     id,
			FirmwareRevision: device.FirmwareVersion,
		}
		// TODO: handle device.Present (Status Fault)
		if therm := device.Thermostat; therm != nil {
			var lastTherm atomic.Pointer[aha.Thermostat]
			lastTherm.Store(therm)

			zero := uint8(0)
			one := uint8(1) // only "off" and "heat" are available
			currentHeatingCoolingState := characteristic.CurrentHeatingCoolingState(
				convCurrentHeatingCoolingState(therm.TargetTemp, therm.ActualTemp),
			)
			currentHeatingCoolingState.MinValue = &zero
			currentHeatingCoolingState.MaxValue = &one

			currentTemperature := characteristic.CurrentTemperature(float64(therm.ActualTemp))
			currentTemperature.MinStep = 0.5
			currentTemperature.MinValue = floatRef(0)
			currentTemperature.MaxValue = floatRef(60)

			disableBoost := func(otherErr error) error {
				th := lastTherm.Load()
				if !th.BoostActive {
					return otherErr
				}
				err := client.SetThermostatBoost(id, 0)
				if err == nil {
					th.BoostActive = false
					lastTherm.Store(th)
				}
				if otherErr == nil {
					return err
				}
				return otherErr
			}

			targetTemperature := characteristic.TargetTemperature(float64(adjustedTargetTemp(*therm)), func(f float64) error {
				if f > 28 { // max: activate boost
					th := lastTherm.Load()
					boostErr := client.SetThermostatBoost(id, 20*time.Minute)
					if boostErr == nil {
						th.BoostActive = true
						lastTherm.Store(th)
					}

					if err := client.SetThermostatTargetTemp(id, th.ComfortTemp); err != nil {
						return err
					}
					return boostErr
				}
				return disableBoost(
					client.SetThermostatTargetTemp(id, aha.ThermostatTemp(f)),
				)
			})
			targetTemperature.MinStep = 0.5
			targetTemperature.MinValue = floatRef(8)  // 8 actually, but homekit says 10
			targetTemperature.MaxValue = floatRef(29) // 28 actually, but 28.5 for "max"

			currentTarget := characteristic.TargetHeatingCoolingState_Heat
			if therm.TargetTemp == aha.ThermostatTempOff {
				currentTarget = characteristic.TargetHeatingCoolingState_Off
			}
			targetHeatingCoolingState := characteristic.TargetHeatingCoolingState(currentTarget, func(u uint8) error {
				if characteristic.TargetHeatingCoolingState_Type(u) == characteristic.TargetHeatingCoolingState_Off {
					return disableBoost(
						client.SetThermostatTargetTemp(id, aha.ThermostatTempOff),
					)
				}

				th := lastTherm.Load()
				t := th.TargetTemp
				if t < th.LowTemp {
					t = th.LowTemp
				}
				if err := client.SetThermostatTargetTemp(id, aha.ThermostatTemp(t)); err != nil {
					return err
				}
				targetTemperature.Update(float64(t))
				return nil
			})
			targetHeatingCoolingState.MinValue = &zero
			targetHeatingCoolingState.MaxValue = &one

			temperatureDisplayUnits := characteristic.TemperatureDisplayUnits(characteristic.TemperatureDisplayUnits_Celsius, func(u uint8) error {
				return errors.New("TemperatureDisplayUnits cannot be changed")
			})
			temperatureDisplayUnits.MinValue = &zero // min: 0 (only celsius)
			temperatureDisplayUnits.MaxValue = &zero // max: 0 (only celsius)

			a.AdditionalServices = append(a.AdditionalServices,
				service.Thermostat(
					currentHeatingCoolingState,
					targetHeatingCoolingState,
					currentTemperature,
					targetTemperature,
					temperatureDisplayUnits,
				).AsPrimary(),
			)
			callbacks[id] = append(callbacks[id], func(d aha.Device) {
				t := d.Thermostat
				lastTherm.Store(t)
				currentHeatingCoolingState.Update(uint8(convCurrentHeatingCoolingState(t.TargetTemp, t.ActualTemp)))

				currentTarget := characteristic.TargetHeatingCoolingState_Heat
				if t.TargetTemp == aha.ThermostatTempOff {
					currentTarget = characteristic.TargetHeatingCoolingState_Off
				}
				targetHeatingCoolingState.Update(uint8(currentTarget))

				currentTemperature.Update(float64(t.ActualTemp))

				targetTemperature.Update(float64(adjustedTargetTemp(*t)))
			})
		}
		if sens := device.TemperatureSensor; sens != nil {
			currentTemperature := characteristic.CurrentTemperature(float64(sens.Celsius))
			lowBattery := characteristic.StatusLowBattery(statusLowBattery(device.BatteryLow))

			a.AdditionalServices = append(a.AdditionalServices,
				service.TemperatureSensor(currentTemperature, lowBattery),
			)
			callbacks[id] = append(callbacks[id], func(d aha.Device) {
				currentTemperature.Update(float64(d.TemperatureSensor.Celsius))
				lowBattery.Update(uint8(statusLowBattery(d.BatteryLow)))
			})
		}
		acc[id] = a
	}

	return acc, func() (*aha.DeviceListInfos, error) {
		infos, err := client.GetDeviceListInfos()
		if err != nil {
			return nil, err
		}

		for _, d := range infos.Devices {
			for _, cb := range callbacks[d.Identifier] {
				cb(d)
			}
		}

		return infos, nil
	}, nil
}

func adjustedTargetTemp(t aha.Thermostat) aha.ThermostatTemp {
	if t.BoostActive || t.TargetTemp == aha.ThermostatTempOn {
		return 29 // max
	}
	if t.TargetTemp == aha.ThermostatTempOff {
		// opposite of NextChange state
		if t.NextChange.WishTemp == t.ComfortTemp {
			return t.LowTemp
		}
		if t.ComfortTemp == 0 {
			// no communication with device (all 0, WishTemp == -1)
			// return max temp
			return 29
		}
		return t.ComfortTemp
	}
	return t.TargetTemp
}

func floatRef(v float64) *float64 {
	return &v
}

func convCurrentHeatingCoolingState(target, actual aha.ThermostatTemp) characteristic.CurrentHeatingCoolingState_Type {
	if target != aha.ThermostatTempOff && target > actual {
		return characteristic.CurrentHeatingCoolingState_Heat
	}
	return characteristic.CurrentHeatingCoolingState_Off
}

func statusLowBattery(v bool) characteristic.StatusLowBattery_Type {
	if v {
		return characteristic.StatusLowBattery_BatteryLevelLow
	}
	return characteristic.StatusLowBattery_BatteryLevelNormal
}
