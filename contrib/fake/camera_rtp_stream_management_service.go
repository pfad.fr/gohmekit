package fake

import (
	"fmt"

	"code.pfad.fr/gohmekit/hapip"
	"code.pfad.fr/gohmekit/hapip/characteristic"
	"code.pfad.fr/gohmekit/hapip/characteristic/service"
)

func cameraRTPStreamManagementService() hapip.Service {
	return service.CameraRTPStreamManagement(
		characteristic.SupportedVideoStreamConfiguration(characteristic.SupportedVideoStreamConfigurationResponse{
			Codecs: []struct {
				Type       byte                                  `tlv8:"1"`
				Parameters []characteristic.VideoCodecParameters `tlv8:"2"`
				Attributes []characteristic.VideoAttributes      `tlv8:"3"`
			}{
				{
					Type: 0, // H264
					Parameters: []characteristic.VideoCodecParameters{
						{
							ProfileID:         0, // baseline
							Level:             0, // 3.1
							PacketizationMode: 0, // non-interleaved
						},
						{
							ProfileID:         1, // main
							Level:             1, // 3.2
							PacketizationMode: 0, // non-interleaved
						},
						{
							ProfileID:         2, // high
							Level:             2, // 4
							PacketizationMode: 0, // non-interleaved
						},
					},
					Attributes: []characteristic.VideoAttributes{
						{1920, 1080, 30}, // 1080p
						{1280, 720, 30},  // 720p
						{640, 360, 30},
						{480, 270, 30},
						{320, 180, 30},
						{1280, 960, 30},
						{1024, 768, 30}, // XVGA
						{640, 480, 30},  // VGA
						{480, 360, 30},  // HVGA
						{320, 240, 15},  // QVGA (Apple Watch)
					},
				},
			},
		}),
		characteristic.SupportedAudioStreamConfiguration(characteristic.SupportedAudioStreamConfigurationResponse{
			Codecs: []struct {
				Type       byte                                  `tlv8:"1"`
				Parameters []characteristic.AudioCodecParameters `tlv8:"2"`
			}{
				{
					Type: 3, // opus
					Parameters: []characteristic.AudioCodecParameters{
						{
							Channels:        1,
							ConstantBitrate: false,
							SampleRate:      2, // 24 Khz
						},
					},
				},
				{
					Type: 2, // AAC ELD
					Parameters: []characteristic.AudioCodecParameters{
						{
							Channels:        1,
							ConstantBitrate: false,
							SampleRate:      1, // 16 Khz
						},
					},
				},
			},
			ComfortNoise: false,
		}),
		characteristic.SupportedRTPConfiguration(
			characteristic.NewSupportedRTPConfigurationResponse(0), // only CryptoSuite_AES_CM_128_HMAC_SHA1_80
		),
		characteristic.SelectedRTPStreamConfiguration(
			characteristic.SelectedRTPStreamConfigurationResponse{},
			func(req characteristic.SelectedRTPStreamConfigurationRequest) (characteristic.SelectedRTPStreamConfigurationResponse, error) {
				resp := characteristic.SelectedRTPStreamConfigurationResponse(req)
				return resp, fmt.Errorf("unsupported RTP command: %d", req.Session.Command)
			},
		),
		characteristic.StreamingStatus(
			characteristic.StreamingStatusResponse{
				Status: 0,
			},
		),
		characteristic.SetupEndpoints(characteristic.SetupEndpointsResponse{
			Status: 2, // error until first write
		}, func(req characteristic.SetupEndpointsRequest) (characteristic.SetupEndpointsResponse, error) {
			return characteristic.SetupEndpointsResponse{
				SessionID: req.SessionID,
				Status:    1, // always return "busy"
			}, nil
		}),
	)
}
