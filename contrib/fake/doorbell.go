package fake

import (
	"fmt"
	"math"
	"net/http"
	"sync/atomic"
	"time"

	"code.pfad.fr/gohmekit/hapip/characteristic"
	"code.pfad.fr/gohmekit/hapip/characteristic/service"
	"code.pfad.fr/gohmekit/hapip/characteristic/service/accessory"
)

func NewDoorbell(info accessory.Basic, imageResource func(w, h int, ringedAgo time.Duration) ([]byte, error)) (accessory.WithResource, func(characteristic.ProgrammableSwitchEvent_Type)) {
	switchEvent, localWrite := characteristic.ProgrammableSwitchEventForIP()

	var lastRing atomic.Value

	info.AdditionalServices = append(info.AdditionalServices,
		service.Doorbell(switchEvent).AsPrimary(),

		cameraRTPStreamManagementService(),
		service.Speaker(characteristic.Mute(true, func(v bool) error {
			return fmt.Errorf("can't mute fake speaker to %t", v)
		})),
		service.Microphone(characteristic.Volume(100, func(v uint8) error {
			return fmt.Errorf("can't set fake microphone volume to %d", v)
		}), characteristic.Mute(true, func(v bool) error {
			return fmt.Errorf("can't mute fake microphone to %t", v)
		})),
	)

	return info.WithResourceHandler(func(rw http.ResponseWriter, typ string, width, height int) error {
			if typ != "image" {
				rw.WriteHeader(http.StatusBadRequest)
				return fmt.Errorf("unsupported resource type: %s", typ)
			}
			var ringedAgo time.Duration = math.MaxInt64
			if t := lastRing.Load(); t != nil {
				ringedAgo = time.Since(t.(time.Time))
			}
			buf, err := imageResource(width, height, ringedAgo)
			if err != nil {
				rw.WriteHeader(http.StatusInternalServerError)
				return err
			}
			_, err = rw.Write(buf)
			return err
		}),
		func(pse characteristic.ProgrammableSwitchEvent_Type) {
			lastRing.Store(time.Now())
			localWrite(pse)
		}
}
