package fake

import (
	"bytes"
	"image"
	"image/jpeg"
	"sync"

	"golang.org/x/image/draw"
)

type JPEGCache[Parameter comparable] struct {
	Original func(p Parameter) (image.Image, error)
	mu       sync.Mutex
	cached   map[struct {
		w, h int
		p    Parameter
	}][]byte
}

func (j *JPEGCache[Parameter]) GetImage(w, h int, p Parameter) ([]byte, error) {
	if buf := j.getCached(w, h, p); len(buf) > 0 {
		return buf, nil
	}

	img, err := j.Original(p)
	if err != nil {
		return nil, err
	}
	fitted := fitSize(img)(w, h)
	var buf bytes.Buffer
	if err = jpeg.Encode(&buf, fitted, nil); err != nil {
		return nil, err
	}

	j.mu.Lock()
	defer j.mu.Unlock()
	key := struct {
		w int
		h int
		p Parameter
	}{w, h, p}
	j.cached[key] = buf.Bytes()
	return j.cached[key], nil
}

func (j *JPEGCache[Parameter]) getCached(w, h int, p Parameter) []byte {
	j.mu.Lock()
	defer j.mu.Unlock()

	if j.cached == nil {
		j.cached = make(map[struct {
			w int
			h int
			p Parameter
		}][]byte)
	}
	key := struct {
		w int
		h int
		p Parameter
	}{w, h, p}
	return j.cached[key]
}

func fitSize(src image.Image) func(w, h int) *image.RGBA {
	return func(w, h int) *image.RGBA {
		dr := image.Rect(0, 0, w, h)
		dst := image.NewRGBA(dr)

		sr := src.Bounds()
		if sr.Dx()*h > sr.Dy()*w { // dst in "more" portrait than src
			dy := (w * sr.Dy()) / sr.Dy()
			dr.Min.Y = (h - dy) / 2
			dr.Max.Y = h - dr.Min.Y
		} else {
			dx := (h * sr.Dx()) / sr.Dy()
			dr.Min.X = (w - dx) / 2
			dr.Max.X = w - dr.Min.X
		}

		draw.CatmullRom.Scale(dst, dr, src, sr, draw.Over, nil)

		return dst
	}
}
