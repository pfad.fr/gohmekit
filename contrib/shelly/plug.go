package shelly

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"regexp"
	"time"

	"code.pfad.fr/gohmekit/hapip"
	"code.pfad.fr/gohmekit/hapip/characteristic"
	"code.pfad.fr/gohmekit/hapip/characteristic/service"
	"code.pfad.fr/gohmekit/hapip/characteristic/service/accessory"
)

type responseSettingsCommon struct {
	Device struct {
		Type     string `json:"type"`
		MAC      string `json:"mac"`
		Hostname string `json:"hostname"`
	} `json:"device"`
	Name         string `json:"name"`
	Firmware     string `json:"fw"`
	Discoverable bool   `json:"discoverable"`
	BuildInfo    struct {
		BuildID        string    `json:"build_id"`
		BuildTimestamp time.Time `json:"build_timestamp"`
		BuildVersion   string    `json:"build_version"`
	} `json:"build_info"`
}

type responseSettingsRelay struct {
	responseSettingsCommon
	LEDStatusDisable bool `json:"led_status_disable"`
	Relays           []struct {
		Name          interface{} `json:"name"`
		ApplianceType string      `json:"appliance_type"`
		Ison          bool        `json:"ison"`
		HasTimer      bool        `json:"has_timer"`
		DefaultState  string      `json:"default_state"`
		AutoOn        float64     `json:"auto_on"`
		AutoOff       float64     `json:"auto_off"`
		Schedule      bool        `json:"schedule"`
		ScheduleRules []string    `json:"schedule_rules"`
		MaxPower      int         `json:"max_power"`
	} `json:"relays"`
}

type responseRelay struct {
	Ison           bool   `json:"ison"`
	HasTimer       bool   `json:"has_timer"`
	TimerStarted   int    `json:"timer_started"`
	TimerDuration  int    `json:"timer_duration"`
	TimerRemaining int    `json:"timer_remaining"`
	Overpower      bool   `json:"overpower"`
	Source         string `json:"source"`
}

var revisionRegex = regexp.MustCompile(`v\d+\.\d+(\.\d+)?`)

func firmwareRevision(long string) string {
	found := revisionRegex.FindString(long)
	if len(found) == 0 {
		return ""
	}
	return found[1:] // skip leading v
}

type device struct {
	url *url.URL
}

func (d device) getJSON(timeout time.Duration, url string, v any) error {
	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()
	return d.getCtxJSON(ctx, url, v)
}

func (d device) getCtxJSON(ctx context.Context, url string, v any) error {
	req, err := http.NewRequestWithContext(ctx, "GET", url, nil)
	if err != nil {
		return err
	}
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	if resp.StatusCode >= 300 {
		return fmt.Errorf("unexpected status code: %d", resp.StatusCode)
	}
	if v == nil {
		return nil
	}
	return json.NewDecoder(resp.Body).Decode(v)
}

func (d device) settings() (rsr responseSettingsRelay, err error) {
	return rsr, d.getJSON(15*time.Second, d.url.JoinPath("settings").String(), &rsr)
}
func (d device) settingLED(disable string) error {
	u := d.url.JoinPath("settings")
	u.RawQuery = url.Values{"led_power_disable": []string{disable}}.Encode()
	return d.getJSON(3*time.Second, u.String(), nil)
}

func (d device) relay(ctx context.Context) (rr responseRelay, err error) {
	return rr, d.getCtxJSON(ctx, d.url.JoinPath("relay", "0").String(), &rr)
}

func (d device) relayTurn(turn string) error {
	u := d.url.JoinPath("relay", "0")
	u.RawQuery = url.Values{"turn": []string{turn}}.Encode()
	return d.getJSON(3*time.Second, u.String(), nil)
}

func (d device) identify(initialLEDState bool) func(v bool) error {
	return func(v bool) error {
		// turn on
		err := d.relayTurn("on")
		if err != nil {
			return err
		}
		for i := 0; i < 3; i++ {
			err = d.settingLED("false")
			if err != nil {
				return err
			}
			time.Sleep(500 * time.Millisecond)

			err = d.settingLED("true")
			if err != nil {
				return err
			}
			time.Sleep(500 * time.Millisecond)
		}
		state := "false"
		if initialLEDState {
			state = "true"
		}

		return d.settingLED(state)
	}
}

// NewPlug creates a proxy to a Shelly Plug S device.
func NewPlug(addr string) (accessory.Basic, error) {
	u, err := url.Parse(addr)
	if err != nil {
		return accessory.Basic{}, err
	}
	d := device{
		url: u,
	}

	resp, err := d.settings()
	if err != nil {
		return accessory.Basic{}, err
	}

	remoteRead := func(ctx context.Context) (bool, error) {
		resp, err := d.relay(ctx)
		if err != nil {
			return false, err
		}
		return resp.Ison, nil
	}
	remoteWrite := func(v bool) error {
		turn := "on"
		if !v {
			turn = "off"
		}
		return d.relayTurn(turn)
	}

	// TODO check regularly the isOn and call on.Update(val bool)
	on := characteristic.On(
		resp.Relays[0].Ison,
		remoteWrite,
	).WithRemoteRead(remoteRead)
	return accessory.Basic{
		Identify:         d.identify(resp.LEDStatusDisable),
		Manufacturer:     "Shelly",
		Model:            resp.Device.Type,
		Name:             resp.Name,
		SerialNumber:     resp.Device.MAC,
		FirmwareRevision: firmwareRevision(resp.Firmware),

		AdditionalServices: []hapip.Service{
			service.Switch(on),
		},
	}, nil
}
