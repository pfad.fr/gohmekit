package camera

/*
import (
	"bufio"
	"encoding/hex"
	"fmt"
	"image"
	"image/color"
	"image/jpeg"
	"io"
	"math"
	"math/rand"
	"net"
	"net/http"
	"os"
	"strconv"
	"time"

	"code.pfad.fr/gohmekit/hapip"
	"code.pfad.fr/gohmekit/hapip/characteristic"
	"code.pfad.fr/gohmekit/hapip/characteristic/service"
	"code.pfad.fr/gohmekit/hapip/characteristic/service/accessory"
	"github.com/pion/rtp"
	"github.com/pion/srtp/v2"
	"gopkg.in/metakeule/loop.v4"
)

func NewCamera(info accessory.Information) accessory.InformationWithResource {
	info.AdditionalServices = append(info.AdditionalServices,
		rtpService(),
	)
	return info.WithResourceHandler(threeCircles)
}

func threeCircles(rw http.ResponseWriter, typ string, width int, height int) error {
	if typ != "image" {
		rw.WriteHeader(http.StatusBadRequest)
		return fmt.Errorf("unsupported resource type: %s", typ)
	}
	img := newImage(width, height)
	return jpeg.Encode(rw, img, nil)
}

// http://tech.nitoyon.com/en/blog/2015/12/31/go-image-gen/

type Circle struct {
	X, Y, R float64
}

func (c *Circle) Brightness(x, y float64) uint8 {
	dx, dy := c.X-x, c.Y-y
	d := math.Sqrt(dx*dx+dy*dy) / c.R
	if d > 1 {
		// outside
		return 0
	} else {
		// inside
		return uint8((1 - math.Pow(d, 5)) * 255)
	}
}

func newImage(w, h int) *image.RGBA {
	hw, hh := float64(w/2), float64(h/2)
	r := 40.0
	angle := 2 * math.Pi / 3
	cr := &Circle{hw - r*math.Sin(0), hh - r*math.Cos(0), 60}
	cg := &Circle{hw - r*math.Sin(angle), hh - r*math.Cos(angle), 60}
	cb := &Circle{hw - r*math.Sin(-angle), hh - r*math.Cos(-angle), 60}

	m := image.NewRGBA(image.Rect(0, 0, w, h))
	for x := 0; x < w; x++ {
		for y := 0; y < h; y++ {
			c := color.RGBA{
				cr.Brightness(float64(x), float64(y)),
				cg.Brightness(float64(x), float64(y)),
				cb.Brightness(float64(x), float64(y)),
				255,
			}
			m.Set(x, y, c)
		}
	}
	return m
}

func rtpService() hapip.Service {
	return service.CameraRTPStreamManagement(
		characteristic.SupportedVideoStreamConfiguration(characteristic.SupportedVideoStreamConfigurationResponse{
			Codecs: []struct {
				Type       byte                                  `tlv8:"1"`
				Parameters []characteristic.VideoCodecParameters `tlv8:"2"`
				Attributes []characteristic.VideoAttributes      `tlv8:"3"`
			}{
				{
					Type: 0, // H264
					Parameters: []characteristic.VideoCodecParameters{
						{
							ProfileID:         0, // baseline
							Level:             0, // 3.1
							PacketizationMode: 0, // non-interleaved
						},
						{
							ProfileID:         1, // main
							Level:             1, // 3.2
							PacketizationMode: 0, // non-interleaved
						},
						{
							ProfileID:         2, // high
							Level:             2, // 4
							PacketizationMode: 0, // non-interleaved
						},
					},
					Attributes: []characteristic.VideoAttributes{
						{1920, 1080, 30}, // 1080p
						{1280, 720, 30},  // 720p
						{640, 360, 30},
						{480, 270, 30},
						{320, 180, 30},
						{1280, 960, 30},
						{1024, 768, 30}, // XVGA
						{640, 480, 30},  // VGA
						{480, 360, 30},  // HVGA
						{320, 240, 15},  // QVGA (Apple Watch)
					},
				},
			},
		}),
		characteristic.SupportedAudioStreamConfiguration(characteristic.SupportedAudioStreamConfigurationResponse{
			Codecs: []struct {
				Type       byte                                  `tlv8:"1"`
				Parameters []characteristic.AudioCodecParameters `tlv8:"2"`
			}{
				{
					Type: 3, // opus
					Parameters: []characteristic.AudioCodecParameters{
						{
							Channels:        1,
							ConstantBitrate: false,
							SampleRate:      2, // 24 Khz
						},
					},
				},
				{
					Type: 2, // AAC ELD
					Parameters: []characteristic.AudioCodecParameters{
						{
							Channels:        1,
							ConstantBitrate: false,
							SampleRate:      1, // 16 Khz
						},
					},
				},
			},
			ComfortNoise: false,
		}),
		characteristic.SupportedRTPConfiguration(
			characteristic.NewSupportedRTPConfigurationResponse(0), // only CryptoSuite_AES_CM_128_HMAC_SHA1_80
		),
		characteristic.SelectedRTPStreamConfiguration(
			characteristic.SelectedRTPStreamConfigurationResponse{},
			func(req characteristic.SelectedRTPStreamConfigurationRequest) (characteristic.SelectedRTPStreamConfigurationResponse, error) {
				id := hex.EncodeToString(req.Session.Identifier)

				resp := characteristic.SelectedRTPStreamConfigurationResponse(req)
				sess, ok := sessions[id]
				if !ok {
					return resp, fmt.Errorf("unknown session: %s", id)
				}

				fmt.Printf("SelectedRTPStreamConfiguration: %s %d\n", id, req.Session.Command)

				fmt.Printf("TODO SelectedRTPStreamConfiguration: %#v\n", sess)

				switch req.Session.Command {
				case 0: // end
					// TODO: sess.end
					delete(sessions, id)
					return resp, nil
				case 1: // start
					return resp, sess.start()
				case 2: // suspend
				case 3: // resume
				case 4: // reconfigure
				default:
					return resp, fmt.Errorf("unsupported RTP command: %d ", req.Session.Command)
				}
				return resp, fmt.Errorf("unsupported RTP command: %d ", req.Session.Command)
			},
		),
		characteristic.StreamingStatus(
			characteristic.StreamingStatusResponse{
				Status: 0,
			},
		),
		characteristic.SetupEndpoints(characteristic.SetupEndpointsResponse{
			Status: 2, // error until first write
		}, func(req characteristic.SetupEndpointsRequest) (characteristic.SetupEndpointsResponse, error) {
			id := hex.EncodeToString(req.SessionID)

			// TODO ssrc is different for every stream
			ssrcVideo := rand.Int31()
			ssrcAudio := rand.Int31()
			sess := session{
				ControllerAddr: req.ControllerAddr,
				VideoSRTP:      req.VideoSRTP,
				AudioSRTP:      req.AudioSRTP,
				VideoSSRC:      uint32(ssrcVideo),
				AudioSSRC:      uint32(ssrcAudio),
			}

			sessions[id] = sess

			ownIP, err := getOutboundIP(req.ControllerAddr.IP + ":80")
			if err != nil {
				return characteristic.SetupEndpointsResponse{}, err
			}

			fmt.Printf("SetupEndpoints: %s %#v\n", id, sess)

			return characteristic.SetupEndpointsResponse{
				SessionID: req.SessionID,
				Status:    0, // success
				AccessoryAddr: characteristic.Address{
					IPv6:         req.ControllerAddr.IPv6,
					IP:           ownIP.String(),
					VideoRtpPort: req.ControllerAddr.VideoRtpPort,
					AudioRtpPort: req.ControllerAddr.AudioRtpPort,
				},
				VideoSRTP: req.VideoSRTP,
				AudioSRTP: req.AudioSRTP,
				VideoSSRC: uint32(ssrcVideo),
				AudioSSRC: uint32(ssrcAudio),
			}, nil
		}),
	)
}

func getOutboundIP(address string) (net.IP, error) {
	conn, err := net.Dial("udp", address)
	if err != nil {
		return nil, err
	}
	defer conn.Close()

	localAddr := conn.LocalAddr().(*net.UDPAddr)

	return localAddr.IP, nil
}

// TODO: add some locking and a way to delete old sessions?
var sessions = make(map[string]session)

type session struct {
	ControllerAddr characteristic.Address
	VideoSRTP      characteristic.SRTP
	AudioSRTP      characteristic.SRTP
	VideoSSRC      uint32
	AudioSSRC      uint32
}

func (s session) start() error {
	addr := s.ControllerAddr.IP + ":" + strconv.FormatUint(uint64(s.ControllerAddr.VideoRtpPort), 10)
	fmt.Println(addr)
	conn, err := net.Dial("udp", addr)
	if err != nil {
		return err
	}
	params := s.VideoSRTP
	var profile srtp.ProtectionProfile
	switch params.CryptoSuite {
	case 0:
		profile = srtp.ProtectionProfileAes128CmHmacSha1_80
	default:
		return fmt.Errorf("unsupported crypto profile: %d", params.CryptoSuite)
	}
	video, err := srtp.NewSessionSRTP(conn, &srtp.Config{
		Keys: srtp.SessionKeys{
			RemoteMasterKey:  params.MasterKey,
			RemoteMasterSalt: params.MasterSalt,
			LocalMasterKey:   params.MasterKey,
			LocalMasterSalt:  params.MasterSalt,
		},
		Profile: profile,
	})
	if err != nil {
		return err
	}
	ws, err := video.OpenWriteStream()
	if err != nil {
		return err
	}

	stream, err := os.ReadFile("recorded.h264")
	if err != nil {
		return err
	}
	go func() {
		n := uint16(0)
		err := streamH264(loop.New(stream), 40*time.Millisecond, func(t time.Time, b []byte) error {
			err := ws.SetWriteDeadline(time.Now().Add(3 * time.Second))
			if err != nil {
				return err
			}
			_, err = ws.WriteRTP(&rtp.Header{
				// Version
				// Padding
				// Extension
				// Marker
				PayloadType:    99,
				SequenceNumber: n,
				Timestamp:      uint32(t.Unix()),
				SSRC:           s.VideoSSRC,
				//CSRC
				//ExtensionProfile
				//Extensions
			}, b)
			n++
			return err
		})
		fmt.Println("STREAM", n, err)
	}()
	return nil
}

func streamH264(src io.Reader, pause time.Duration, cb func(time.Time, []byte) error) error {
	scanner := bufio.NewScanner(src)
	scanner.Split(ScanFrames)

	var buf []byte
	ticker := time.NewTicker(pause)
	defer ticker.Stop()
	for scanner.Scan() {
		b := scanner.Bytes()
		if len(b) < 5 {
			buf = append(buf, b...)
			continue
		}

		// We buffer the bytes, until it looks like a good h264 frame
		//
		// Thanks to https://yumichan.net/video-processing/video-compression/introduction-to-h264-nal-unit/
		nal_unit_type := b[4] & 0b11111
		if (nal_unit_type == 7 || nal_unit_type == 1) && len(buf) > 0 {
			ts := <-ticker.C
			err := cb(ts, buf)
			if err != nil {
				return err
			}

			buf = append(buf[:0], b...)
		} else {
			buf = append(buf, b...)
		}
	}
	return scanner.Err()
}

// ScanFrames splits the buffer into h264 frames
// Which start with 0x00 00 01 according to https://dsp.stackexchange.com/a/27297
func ScanFrames(data []byte, atEOF bool) (advance int, token []byte, err error) {
	zeros := 0
	for i := 3; i < len(data); i++ {
		if data[i] == 0x00 {
			zeros++
		} else if data[i] == 0x01 && zeros >= 3 {
			//end of frame
			return i - 3, data[:i-3], nil
		} else {
			zeros = 0
		}
	}
	if atEOF && len(data) > 0 {
		// need more data
		return len(data), data, nil
	}
	return 0, nil, nil
}
*/
