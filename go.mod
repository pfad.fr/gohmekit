module code.pfad.fr/gohmekit

go 1.18

require (
	github.com/brutella/hc v1.2.5
	github.com/go-logfmt/logfmt v0.5.1
	github.com/tadglines/go-pkgs v0.0.0-20210623144937-b983b20f54f9
)

require (
	github.com/cenkalti/backoff v2.2.1+incompatible // indirect
	github.com/google/go-cmp v0.5.5 // indirect
	github.com/miekg/dns v1.1.50 // indirect
	github.com/stretchr/testify v1.7.1 // indirect
	github.com/xiam/to v0.0.0-20200126224905-d60d31e03561 // indirect
	golang.org/x/mod v0.6.0-dev.0.20220419223038-86c51ed26bb4 // indirect
	golang.org/x/net v0.0.0-20220809184613-07c6da5e1ced // indirect
	golang.org/x/text v0.3.7 // indirect
	golang.org/x/tools v0.1.12 // indirect
	golang.org/x/xerrors v0.0.0-20220609144429-65e65417b02f // indirect
)

require (
	code.pfad.fr/fritzgox v0.1.3
	github.com/go-kit/log v0.2.1
	github.com/grandcat/zeroconf v1.0.0
	golang.org/x/crypto v0.0.0-20220829220503-c86fa9a7ed90
	golang.org/x/image v0.0.0-20220722155232-062f8c9fd539
	golang.org/x/sys v0.0.0-20220808155132-1c4a2a72c664 // indirect
	gotest.tools/v3 v3.3.0
)
