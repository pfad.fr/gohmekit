// Code generated by cmd/generate/main.go; DO NOT EDIT.

package discovery

type Category uint16

const (
	Unknown = Category(0)
	Other = Category(1)
	Bridge = Category(2)
	Fan = Category(3)
	GarageDoorOpener = Category(4)
	Lightbulb = Category(5)
	DoorLock = Category(6)
	Outlet = Category(7)
	Switch = Category(8)
	Thermostat = Category(9)
	Sensor = Category(10)
	SecuritySystem = Category(11)
	Door = Category(12)
	Window = Category(13)
	WindowCovering = Category(14)
	ProgrammableSwitch = Category(15)
	IPCamera = Category(17)
	VideoDoorbell = Category(18)
	AirPurifier = Category(19)
	Heater = Category(20)
	AirConditioner = Category(21)
	Humidifier = Category(22)
	Dehumidifier = Category(23)
	Sprinklers = Category(28)
	Faucets = Category(29)
	ShowerSystems = Category(30)
	Television = Category(31)
	RemoteControl = Category(32)
)
