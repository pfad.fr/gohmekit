package discovery

import (
	"context"
	"strconv"

	"github.com/grandcat/zeroconf"
)

type Service struct {
	// Name of the service on _hap._tcp.local
	Name string
	// ModelName of the accessory (e.g. ”Device1,1”).
	ModelName string
	// Port where the TCP server is listening
	Port int
	// Device ID must be formatted as ”XX:XX:XX:XX:XX:XX”, where ”XX”
	// is a hexadecimal string representing a byte.
	// This value is also used as the accessoryʼs Pairing Identifier.
	DeviceID string
	// VersionWatcher gives the initial version value and hooks a watcher callback
	VersionWatcher func(func(uint16)) uint16
	// IsPairedWatcher gives the initial IsPaired value and hooks a watcher callback
	IsPairedWatcher func(func(bool)) bool
	// Category of the accessory being announced
	Category Category
}

// Announce the service on the network.
func (s Service) Announce(ctx context.Context) (context.CancelFunc, error) {
	records, updates := recordGenerator(
		s.ModelName,
		s.DeviceID,
		s.Category,
		s.VersionWatcher,
		s.IsPairedWatcher,
	)

	server, err := zeroconf.Register(s.Name, "_hap._tcp", "local.", s.Port, mapSerialize(records), nil)
	if err != nil {
		return nil, err
	}

	ctx, cancel := context.WithCancel(ctx)

	go func() {
		defer server.Shutdown()
		for {
			select {
			case <-ctx.Done():
				return
			case update := <-updates:
				records[update.Key] = update.Value
				server.SetText(mapSerialize(records))
			}
		}
	}()

	return cancel, nil
}

func mapSerialize(i map[string]string) []string {
	o := make([]string, 0, len(i))
	for k, v := range i {
		o = append(o, k+"="+v)
	}
	return o
}

type recordUpdate struct {
	Key, Value string
}

func recordGenerator(
	modelName string,
	formattedDeviceID string,
	category Category,
	versionWatcher func(func(uint16)) uint16,
	isPairedWatcher func(func(bool)) bool,
) (map[string]string, <-chan recordUpdate) { //nolint:whitespace

	uint16Str := func(v uint16) string {
		return strconv.FormatUint(uint64(v), 10)
	}
	boolStr := func(v bool) string {
		if v {
			return "1"
		}
		return "0"
	}

	updates := make(chan recordUpdate)
	status := map[string]string{
		// Current configuration number. Required.
		// Must update when an accessory, service, or characteristic is added or removed on the accessory server.
		// Accessories must increment the config number after a firmware update.
		// This must have a range of 1-65535 and wrap to 1 when it overflows.
		// This value must persist across reboots, power cycles, etc.
		"c#": uint16Str(versionWatcher(func(v uint16) {
			updates <- recordUpdate{
				Key:   "c#",
				Value: uint16Str(v),
			}
		})),

		// Pairing Feature flags (e.g. ”0x3” for bits 0 and 1). Required if non-zero. See Table 5-4 (page 49).
		// "ff": "0", // reserved

		// Device ID (”5.4 Device ID” (page 31)) of the accessory. The Device ID must be formatted as
		// ”XX:XX:XX:XX:XX:XX”, where ”XX” is a hexadecimal string representing a byte. Required.
		// This value is also used as the accessoryʼs Pairing Identifier.
		"id": formattedDeviceID, // 48 bits

		// Model name of the accessory (e.g. ”Device1,1”). Required.
		"md": modelName,

		// Protocol version string ”X.Y” (e.g. ”1.0”). Required if value is not ”1.0”.
		// (see ”6.6.3 IP Protocol Version” (page 61))
		"pv": "1.1",

		// Current state number. Required.
		// This must have a value of ”1”.
		"s#": "1",

		// Status flags (e.g. ”0x04” for bit 3). Value should be an unsigned integer. See Table 6-8 (page 58).
		// Required.
		// 1 when unpaired, 0 when paired
		"sf": boolStr(!isPairedWatcher(func(v bool) {
			updates <- recordUpdate{
				Key:   "sf",
				Value: boolStr(!v),
			}
		})),

		// Accessory Category Identifier. Required. Indicates the category that best describes the primary function
		// of the accessory. This must have a range of 1-65535. This must take values defined in ”13-1 Accessory
		// Categories” (page 252). This must persist across reboots, power cycles, etc.
		"ci": uint16Str(uint16(category)),

		// Setup Hash. See (”?? ??” (page ??)) Required if the accessory supports enhanced setup payload infor-
		// mation.
		// "sh":"",
	}

	return status, updates
}
