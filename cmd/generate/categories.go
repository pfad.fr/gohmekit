package main

import (
	"os"
	"strconv"
)

func writeCategories(cc []Category) error {
	f, err := os.Create("discovery/apple_category_gen.go")
	if err != nil {
		return err
	}
	defer f.Close()

	_, err = f.WriteString(`// Code generated by cmd/generate/main.go; DO NOT EDIT.

package discovery

type Category uint16

const (
`)
	if err != nil {
		return err
	}

	for _, c := range cc {
		txt := "	" + camelCase(c.Name) + " = Category(" + strconv.Itoa(c.Category) + ")\n"
		_, err = f.WriteString(txt)
		if err != nil {
			return err
		}
	}
	_, err = f.WriteString(")\n")
	if err != nil {
		return err
	}

	if err = f.Close(); err != nil {
		return err
	}
	return nil
}
