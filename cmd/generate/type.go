package main

type Metadata struct {
	Categories      []Category       `json:"Categories"`
	Characteristics []Characteristic `json:"Characteristics"`
	Version         string           `json:"Version"`
	Services        []struct {
		OptionalCharacteristics []string `json:"OptionalCharacteristics,omitempty"`
		RequiredCharacteristics []string `json:"RequiredCharacteristics"`
		Name                    string   `json:"Name"`
		UUID                    string   `json:"UUID"`
	} `json:"Services"`
}

type Characteristic struct {
	UUID        string `json:"UUID"`
	Name        string `json:"Name"`
	Constraints struct {
		ValidBits    map[string]string `json:"ValidBits"`
		ValidValues  map[string]string `json:"ValidValues"`
		MinimumValue interface{}       `json:"MinimumValue"`
		StepValue    interface{}       `json:"StepValue"`
		MaximumValue interface{}       `json:"MaximumValue"`
	} `json:"Constraints,omitempty"`
	Format      string   `json:"Format"`
	Permissions []string `json:"Permissions,omitempty"`
	Properties  []string `json:"Properties"`
	Unit        string   `json:"Unit,omitempty"`
}

type Category struct {
	Name     string `json:"Name"`
	Category int    `json:"Category"`
}
