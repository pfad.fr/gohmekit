package main

import (
	"os"
	"testing"

	"gotest.tools/v3/assert"
)

func TestGenerate(t *testing.T) {
	assert.NilError(t, os.Chdir("../.."))

	err := run()
	assert.NilError(t, err)
}
