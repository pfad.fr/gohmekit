package main

import (
	"encoding/json"
	"log"
	"os"
	"regexp"
	"strings"
)

func main() {
	if err := run(); err != nil {
		log.Fatal(err)
	}
}

func run() error {
	m, err := parseMetadata("hapip/metadata.json")
	if err != nil {
		return err
	}

	// fmt.Println(c)
	if err := writeCharacteristic(m.Characteristics); err != nil {
		return err
	}
	if err := writeService(m); err != nil {
		return err
	}
	if err := writeCategories(m.Categories); err != nil {
		return err
	}

	return nil
}

func parseMetadata(path string) (Metadata, error) {
	r, err := os.Open(path)
	if err != nil {
		return Metadata{}, err
	}
	defer r.Close()

	var m Metadata
	return m, json.NewDecoder(r).Decode(&m)
}

var specialChars = regexp.MustCompile(`[^0-9a-zA-Z_ ]`)

func clean(s string) string {
	underscored := strings.ReplaceAll(strings.TrimSpace(s), ".", "")
	return specialChars.ReplaceAllString(underscored, "")
}

func camelCase(s string) string {
	s = clean(s)
	s = titleCase(s)
	return strings.ReplaceAll(s, " ", "")
}

func titleCase(s string) string {
	previousIsSpace := false
	var result string
	for _, r := range s {
		if previousIsSpace {
			result += strings.ToUpper(string(r))
		} else {
			result += string(r)
		}
		previousIsSpace = r == ' '
	}
	return result
}

var appleShort = regexp.MustCompile(`^0*([1-9A-F][0-9A-F]*)-0000-1000-8000-0026BB765291$`)

func shortUUID(s string) string {
	matches := appleShort.FindStringSubmatch(s)
	if len(matches) > 1 {
		return matches[1]
	}
	return s
}
