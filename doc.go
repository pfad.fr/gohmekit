// package gohmekit implements the HomeKit Accessory Protocol (hap).
//
// See the Example to get started.
//
// This library wouldn't have been possible without the amazing open-source work of Matthias Hochgatterer:
//   - https://github.com/brutella/hc
//   - https://github.com/brutella/hap
package gohmekit
