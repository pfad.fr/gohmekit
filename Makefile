test:
	go test -race -cover -timeout=5s ./...

dev:
	go run ./cmd/test/

lint:
	golangci-lint run ./...

generate:
	go run ./cmd/generate


kam:
	go run ./cmd/hkam/
