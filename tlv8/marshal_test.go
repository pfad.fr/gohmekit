package tlv8

import (
	"sync"
	"testing"

	"gotest.tools/v3/assert"
)

func TestMarshal(t *testing.T) {
	t.Run("basic types", func(t *testing.T) {
		type response struct {
			Bool    bool    `tlv8:"1"`
			Uint8   uint8   `tlv8:"8"`
			Uint16  uint16  `tlv8:"16"`
			Uint32  uint32  `tlv8:"32"`
			Uint64  uint64  `tlv8:"64"`
			Int32   int32   `tlv8:"100"`
			Float32 float32 `tlv8:"101"`
			String  string  `tlv8:"102"`
			BSlice  []byte  `tlv8:"103"`

			Byte    byte  `tlv8:"200"`
			Pointer *byte `tlv8:"201"`
			Ignored byte  // no struct tag
		}
		b := byte(57)
		initial := response{
			Bool:    true,
			Uint8:   2,
			Uint16:  3,
			Uint32:  4,
			Uint64:  5,
			Int32:   -42,
			Float32: 0.2,
			String:  "hello",
			BSlice:  []byte{42, 43},

			Byte:    1,
			Pointer: &b,
			Ignored: 42,
		}
		buf, err := Marshal(initial)
		assert.NilError(t, err)
		assert.DeepEqual(t, buf, []byte{
			1, 1, 1,
			8, 1, 2,
			16, 2, 3, 0,
			32, 4, 4, 0, 0, 0,
			64, 8, 5, 0, 0, 0, 0, 0, 0, 0,
			100, 4, 0xd6, 0xff, 0xff, 0xff,
			101, 4, 0xcd, 0xcc, 0x4c, 0x3e,
			102, 5, 'h', 'e', 'l', 'l', 'o',
			103, 2, 42, 43,
			200, 1, 1,
			201, 1, 57,
		})
		var unmarshalled response
		err = Unmarshal(buf, &unmarshalled)
		assert.NilError(t, err)
		initial.Ignored = 0
		assert.DeepEqual(t, initial, unmarshalled)
	})
	t.Run("anonymous slice", func(t *testing.T) {
		type response struct {
			Byte   byte   `tlv8:"1"`
			String string `tlv8:"100"`
		}
		initial := []response{
			{
				Byte:   42,
				String: "he",
			},
			{
				Byte:   57,
				String: "ho",
			},
		}
		buf, err := Marshal(initial)
		assert.NilError(t, err)
		assert.DeepEqual(t, buf, []byte{
			1, 1, 42,
			100, 2, 'h', 'e',
			1, 1, 57,
			100, 2, 'h', 'o',
		})
		var unmarshalled []response
		err = Unmarshal(buf, &unmarshalled)
		assert.NilError(t, err)
		assert.DeepEqual(t, initial, unmarshalled)
	})
	t.Run("flattened slice", func(t *testing.T) {
		type controller struct {
			Identifier  []byte   `tlv8:"0x1"`
			PublicKey   []byte   `tlv8:"0x3"`
			Permissions byte     `tlv8:"0x0B"`
			Separator   struct{} `tlv8:"0xFF"`
		}
		type response struct {
			State       byte         `tlv8:"0x06"`
			Controllers []controller `tlv8:""`
		}
		initial := response{
			State: 2,
			Controllers: []controller{
				{
					Identifier:  []byte("123"),
					PublicKey:   []byte("key123.pub"),
					Permissions: 1,
				},
				{
					Identifier:  []byte("456"),
					PublicKey:   []byte("key456.pub"),
					Permissions: 0,
				},
			},
		}
		buf, err := Marshal(initial)
		assert.NilError(t, err)
		assert.DeepEqual(t, buf, []byte{
			6, 1, 2, // State
			1, 3, 0x31, 0x32, 0x33, // Identifier: 123
			3, 10, 0x6b, 0x65, 0x79, 0x31, 0x32, 0x33, 0x2e, 0x70, 0x75, 0x62, // PublicKey key123.pub
			11, 1, 1, // Permission
			255, 0, // Separator

			1, 3, 0x34, 0x35, 0x36,
			3, 10, 0x6b, 0x65, 0x79, 0x34, 0x35, 0x36, 0x2e, 0x70, 0x75, 0x62, // PublicKey key456.pub
			11, 1, 0, // Permission
			255, 0, // Separator
		})

		var unmarshalled response
		err = Unmarshal(buf, &unmarshalled)
		assert.DeepEqual(t, initial, unmarshalled)
		assert.NilError(t, err)
	})

	t.Run("concurrent unmarshalling", func(t *testing.T) {
		type controller struct {
			Identifier  []byte `tlv8:"0x1"`
			PublicKey   []byte `tlv8:"0x3"`
			Permissions byte   `tlv8:"0x0B"`
		}
		type Empty4 struct {
			Controllers []controller `tlv8:"44"`
		}
		type Empty5 struct {
			Controllers []controller `tlv8:"55"`
		}
		type response struct {
			State       byte         `tlv8:"0x06"`
			Controllers []controller `tlv8:""`
			Empty4      Empty4       `tlv8:""`
			Empty5      Empty5       `tlv8:""`
		}
		initial := response{
			State: 2,
			Controllers: []controller{
				{
					Identifier:  []byte("123"),
					PublicKey:   []byte("key123.pub"),
					Permissions: 1,
				},
				{
					Identifier:  []byte("456"),
					PublicKey:   []byte("key456.pub"),
					Permissions: 0,
				},
			},
			Empty4: Empty4{
				Controllers: []controller{
					{[]byte("41"), []byte("41.pub"), 1},
					{[]byte("42"), []byte("42.pub"), 0},
				},
			},
			Empty5: Empty5{
				Controllers: []controller{
					{[]byte("51"), []byte("51.pub"), 1},
					{[]byte("52"), []byte("52.pub"), 0},
				},
			},
		}
		buf, err := Marshal(initial)
		assert.NilError(t, err)

		var wg sync.WaitGroup
		for i := 0; i < 10; i++ {
			wg.Add(1)
			go func() {
				defer wg.Done()
				var unmarshalled response
				err := Unmarshal(buf, &unmarshalled)
				assert.NilError(t, err)
				assert.DeepEqual(t, initial, unmarshalled)
			}()
		}
		wg.Wait()
	})

	t.Run("two flattened slices", func(t *testing.T) {
		type ident struct {
			Identifier []byte `tlv8:"0x1"`
		}
		type pub struct {
			PublicKey []byte `tlv8:"0x3"`
		}
		type response struct {
			State       byte    `tlv8:"0x06"`
			Controllers []ident `tlv8:""`
			Pub         []pub   `tlv8:""`
		}
		initial := response{
			State: 2,
			Controllers: []ident{
				{Identifier: []byte("123")},
				{Identifier: []byte("456")},
			},
			Pub: []pub{
				{PublicKey: []byte("123.pub")},
				{PublicKey: []byte("456.pub")},
			},
		}
		buf, err := Marshal(initial)
		assert.NilError(t, err)

		var unmarshalled response
		err = Unmarshal(buf, &unmarshalled)
		assert.NilError(t, err)
		assert.DeepEqual(t, initial, unmarshalled)
	})
	t.Run("embedded struct", func(t *testing.T) {
		type KtlvState struct {
			State byte `tlv8:"0x06"`
		}
		type KtlvError struct {
			Error byte `tlv8:"0x07"`
		}
		type response struct {
			KtlvState
			KtlvError
		}
		initial := response{
			KtlvState: KtlvState{2},
			KtlvError: KtlvError{6},
		}
		buf, err := Marshal(initial)
		assert.NilError(t, err)
		assert.DeepEqual(t, buf, []byte{
			6, 1, 2, // State
			7, 1, 6, // Error
		})

		var unmarshalled response
		err = Unmarshal(buf, &unmarshalled)
		assert.NilError(t, err)
		assert.DeepEqual(t, initial, unmarshalled)
	})
	t.Run("video struct", func(t *testing.T) {
		type VideoCodecProfile struct {
			Id byte `tlv8:"1"`
		}
		type VideoCodecLevel struct {
			Level byte `tlv8:"2"`
		}
		type VideoCodecPacketization struct {
			Mode byte `tlv8:"3"`
		}
		type VideoCodecAttributes struct {
			Width     uint16 `tlv8:"1"`
			Height    uint16 `tlv8:"2"`
			Framerate byte   `tlv8:"3"`
		}
		type VideoCodecParameters struct {
			Profiles       []VideoCodecProfile       `tlv8:""`
			Levels         []VideoCodecLevel         `tlv8:"-"`
			Packetizations []VideoCodecPacketization `tlv8:""`
			// CvoEnabled     bool                      `-tlv8:"4,optional"` // ?
			// CvoId       byte                      `-tlv8:"5,optional"` // ? value from [1:14]
		}
		type VideoCodecConfiguration struct {
			Type       byte                   `tlv8:"1"`
			Parameters VideoCodecParameters   `tlv8:"2"`
			Attributes []VideoCodecAttributes `tlv8:"3"`
		}

		type VideoStreamConfiguration struct {
			Codecs []VideoCodecConfiguration `tlv8:"1"`
		}

		initial := VideoStreamConfiguration{
			Codecs: []VideoCodecConfiguration{
				{
					Type: 0,
					Parameters: VideoCodecParameters{
						Profiles: []VideoCodecProfile{
							{Id: 0},
							{Id: 1},
							{Id: 2},
						},
						Levels: []VideoCodecLevel{
							{Level: 0},
							{Level: 1},
							{Level: 2},
						},
						Packetizations: []VideoCodecPacketization{
							{Mode: 0},
						},
					},
					Attributes: []VideoCodecAttributes{
						{
							Width:     127,
							Height:    56,
							Framerate: 30,
						},
						{
							Width:  7,
							Height: 4,
						},
					},
				},
			},
		}
		buf, err := Marshal(initial)
		assert.NilError(t, err)
		t.Log(buf)

		var unmarshalled VideoStreamConfiguration
		err = Unmarshal(buf, &unmarshalled)
		assert.NilError(t, err)
		assert.DeepEqual(t, initial, unmarshalled)
	})
	/* FAILING TESTS
	t.Run("own_own", func(t *testing.T) {
		conf := rtp.DefaultVideoStreamConfiguration()
		buf, err := Marshal(conf)
		assert.NilError(t, err)

		var unmarshalled rtp.VideoStreamConfiguration
		Unmarshal(buf, &unmarshalled)
		assert.DeepEqual(t, conf, unmarshalled)
	})
	t.Run("hap_hap", func(t *testing.T) {
		conf := rtp.DefaultVideoStreamConfiguration()
		buf, err := tlv8.Marshal(conf)
		assert.NilError(t, err)

		var unmarshalled rtp.VideoStreamConfiguration
		tlv8.Unmarshal(buf, &unmarshalled)
		assert.DeepEqual(t, conf, unmarshalled)
	})
	t.Run("hap_own", func(t *testing.T) {
		conf := rtp.DefaultVideoStreamConfiguration()
		buf, err := tlv8.Marshal(conf)
		assert.NilError(t, err)
		t.Log(buf)

		var unmarshalled rtp.VideoStreamConfiguration
		Unmarshal(buf, &unmarshalled)
		assert.DeepEqual(t, conf, unmarshalled)
	})
	t.Run("own_hap", func(t *testing.T) {
		conf := rtp.DefaultVideoStreamConfiguration()
		buf, err := Marshal(conf)
		assert.NilError(t, err)
		t.Log("OWN", buf)

		bufHap, err := tlv8.Marshal(conf)
		assert.NilError(t, err)
		t.Log("HAP", bufHap)
		assert.DeepEqual(t, buf, bufHap)

		var unmarshalled rtp.VideoStreamConfiguration
		tlv8.Unmarshal(buf, &unmarshalled)
		assert.DeepEqual(t, conf, unmarshalled)
	})

	t.Run("hap_separator", func(t *testing.T) {
		type codec struct {
			Attributes []struct {
				Width     uint16 `tlv8:"7"`
				Height    uint16 `tlv8:"8"`
				Framerate byte   `tlv8:"9"`
			} `tlv8:"3"`
		}
		conf := codec{
			Attributes: []struct {
				Width     uint16 `tlv8:"7"`
				Height    uint16 `tlv8:"8"`
				Framerate byte   `tlv8:"9"`
			}{
				{1920, 1080, 30},
				{320, 240, 15},
			},
		}
		buf, err := Marshal(conf)
		assert.NilError(t, err)
		t.Log("OWN", buf)

		bufHap, err := tlv8.Marshal(conf)
		assert.NilError(t, err)
		t.Log("HAP", bufHap)
		// assert.DeepEqual(t, buf, bufHap)

		var unmarshalled codec
		tlv8.Unmarshal(buf, &unmarshalled)
		assert.DeepEqual(t, conf, unmarshalled)
	})
	//*/
}

func BenchmarkMarshal(b *testing.B) {
	type VideoCodecProfile struct {
		Id byte `tlv8:"1"`
	}
	type VideoCodecLevel struct {
		Level byte `tlv8:"2"`
	}
	type VideoCodecPacketization struct {
		Mode byte `tlv8:"3"`
	}
	type VideoCodecAttributes struct {
		Width     uint16 `tlv8:"1"`
		Height    uint16 `tlv8:"2"`
		Framerate byte   `tlv8:"3"`
	}
	type VideoCodecParameters struct {
		Profiles       []VideoCodecProfile       `tlv8:""`
		Levels         []VideoCodecLevel         `tlv8:"-"`
		Packetizations []VideoCodecPacketization `tlv8:""`
		// CvoEnabled     bool                      `-tlv8:"4,optional"` // ?
		// CvoId       byte                      `-tlv8:"5,optional"` // ? value from [1:14]
	}
	type VideoCodecConfiguration struct {
		Type       byte                   `tlv8:"1"`
		Parameters VideoCodecParameters   `tlv8:"2"`
		Attributes []VideoCodecAttributes `tlv8:"3"`
	}

	type VideoStreamConfiguration struct {
		Codecs []VideoCodecConfiguration `tlv8:"1"`
	}

	initial := VideoStreamConfiguration{
		Codecs: []VideoCodecConfiguration{
			{
				Type: 0,
				Parameters: VideoCodecParameters{
					Profiles: []VideoCodecProfile{
						{Id: 0},
						{Id: 1},
						{Id: 2},
					},
					Levels: []VideoCodecLevel{
						{Level: 0},
						{Level: 1},
						{Level: 2},
					},
					Packetizations: []VideoCodecPacketization{
						{Mode: 0},
					},
				},
				Attributes: []VideoCodecAttributes{
					{
						Width:     127,
						Height:    56,
						Framerate: 30,
					},
					{
						Width:  7,
						Height: 4,
					},
				},
			},
		},
	}
	for i := 0; i < b.N; i++ {
		buf, err := Marshal(initial)
		if err != nil {
			b.Fatal(err)
		}
		if len(buf) != 54 {
			b.Fatal("buf has wrong length", len(buf))
		}
	}
}

func BenchmarkMarshaller(b *testing.B) {
	type VideoCodecProfile struct {
		Id byte `tlv8:"1"`
	}
	type VideoCodecLevel struct {
		Level byte `tlv8:"2"`
	}
	type VideoCodecPacketization struct {
		Mode byte `tlv8:"3"`
	}
	type VideoCodecAttributes struct {
		Width     uint16 `tlv8:"1"`
		Height    uint16 `tlv8:"2"`
		Framerate byte   `tlv8:"3"`
	}
	type VideoCodecParameters struct {
		Profiles       []VideoCodecProfile       `tlv8:""`
		Levels         []VideoCodecLevel         `tlv8:"-"`
		Packetizations []VideoCodecPacketization `tlv8:""`
		// CvoEnabled     bool                      `-tlv8:"4,optional"` // ?
		// CvoId       byte                      `-tlv8:"5,optional"` // ? value from [1:14]
	}
	type VideoCodecConfiguration struct {
		Type       byte                   `tlv8:"1"`
		Parameters VideoCodecParameters   `tlv8:"2"`
		Attributes []VideoCodecAttributes `tlv8:"3"`
	}

	type VideoStreamConfiguration struct {
		Codecs []VideoCodecConfiguration `tlv8:"1"`
	}

	initial := VideoStreamConfiguration{
		Codecs: []VideoCodecConfiguration{
			{
				Type: 0,
				Parameters: VideoCodecParameters{
					Profiles: []VideoCodecProfile{
						{Id: 0},
						{Id: 1},
						{Id: 2},
					},
					Levels: []VideoCodecLevel{
						{Level: 0},
						{Level: 1},
						{Level: 2},
					},
					Packetizations: []VideoCodecPacketization{
						{Mode: 0},
					},
				},
				Attributes: []VideoCodecAttributes{
					{
						Width:     127,
						Height:    56,
						Framerate: 30,
					},
					{
						Width:  7,
						Height: 4,
					},
				},
			},
		},
	}
	// marshaller does not exist anymore, since there is a global cache
	for i := 0; i < b.N; i++ {
		buf, err := Marshal(initial)
		if err != nil {
			b.Fatal(err)
		}
		if len(buf) != 54 {
			b.Fatal("buf has wrong length", len(buf))
		}
	}
}
