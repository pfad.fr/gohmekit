package tlv8

import (
	"bytes"
	"errors"
	"io"
	"math"
	"reflect"
	"sync"
)

func Marshal(v any) ([]byte, error) {
	buf := &bytes.Buffer{}
	err := NewEncoder(buf).Encode(v)
	if err != nil {
		return nil, err
	}
	return buf.Bytes(), nil
}

type Encoder struct {
	w io.Writer
}

func NewEncoder(w io.Writer) *Encoder {
	return &Encoder{w}
}

func (e Encoder) Encode(v any) error {
	rv := reflect.ValueOf(v)
	prepared, err := prepareMarshaller(rv.Type())
	if err != nil {
		return err
	}
	return prepared(e.w, rv)
}

// valueMarshaller should only return an error, when an error happens on io.Writer.
type valueMarshaller func(io.Writer, reflect.Value) error

func sliceMarshaller(childType reflect.Type, tag string) (valueMarshaller, error) {
	if childType.Kind() == reflect.Uint8 {
		// byte slice
		tag8, err := parseTag(tag)
		if err != nil {
			return nil, err
		}
		return func(w io.Writer, rv reflect.Value) error {
			return writeTLV(w, tag8, rv.Bytes())
		}, nil
	}

	itemMarshaller, err := prepareMarshaller(childType)
	if err != nil {
		return nil, err
	}

	// embed as-is
	if tag == "" || tag == "-" {
		return func(w io.Writer, rv reflect.Value) error {
			for i := 0; i < rv.Len(); i++ {
				if errf := itemMarshaller(w, rv.Index(i)); errf != nil {
					return errf
				}
			}
			return nil
		}, nil
	}

	// repeat slice tag for each item
	tag8, err := parseTag(tag)
	if err != nil {
		return nil, err
	}
	return func(w io.Writer, rv reflect.Value) error {
		for i := 0; i < rv.Len(); i++ {
			buf := &bytes.Buffer{}
			err := itemMarshaller(buf, rv.Index(i))
			if err != nil {
				return err
			}
			err = writeTLV(w, tag8, buf.Bytes())
			if err != nil {
				return err
			}
		}
		return nil
	}, nil
}

var marshallerCacheLock sync.RWMutex
var marshallerCache = make(map[reflect.Type]valueMarshaller)

func getCachedMarshaller(typ reflect.Type) valueMarshaller {
	marshallerCacheLock.RLock()
	defer marshallerCacheLock.RUnlock()
	return marshallerCache[typ]
}

func prepareMarshaller(typ reflect.Type) (vm valueMarshaller, err error) {
	if cached := getCachedMarshaller(typ); cached != nil {
		return cached, nil
	}
	defer func() {
		if err == nil && vm != nil {
			marshallerCacheLock.Lock()
			defer marshallerCacheLock.Unlock()
			marshallerCache[typ] = vm
		}
	}()

	switch typ.Kind() {
	case reflect.Ptr:
		marshaller, err := prepareMarshaller(typ.Elem())
		if err != nil {
			return nil, err
		}
		return func(w io.Writer, rv reflect.Value) error {
			return marshaller(w, rv.Elem())
		}, nil
	case reflect.Struct:
		type prepareField struct {
			index      int
			marshaller valueMarshaller
		}
		var fields []prepareField
		for i := 0; i < typ.NumField(); i++ {
			tf := typ.Field(i)
			tag, ok := tf.Tag.Lookup("tlv8")
			if !ok && !tf.Anonymous {
				continue
			}

			if tf.Type.Kind() == reflect.Slice {
				fieldMarshaller, err := sliceMarshaller(tf.Type.Elem(), tag)
				if err != nil {
					return nil, err
				}

				fields = append(fields, prepareField{
					index:      i,
					marshaller: fieldMarshaller,
				})
				continue
			}

			fieldMarshaller, err := prepareMarshaller(tf.Type)
			if err != nil {
				return nil, err
			}

			// embed as-is
			if tag == "" || tag == "-" {
				fields = append(fields, prepareField{
					index:      i,
					marshaller: fieldMarshaller,
				})
				continue
			}

			// prefix with tag and length
			tag8, err := parseTag(tag)
			if err != nil {
				return nil, err
			}
			fields = append(fields, prepareField{
				index: i,
				marshaller: func(w io.Writer, rv reflect.Value) error {
					buf := &bytes.Buffer{}
					err := fieldMarshaller(buf, rv)
					if err != nil {
						return err
					}
					return writeTLV(w, tag8, buf.Bytes())
				},
			})
		}
		return func(w io.Writer, rv reflect.Value) error {
			for _, field := range fields {
				err := field.marshaller(w, rv.Field(field.index))
				if err != nil {
					return err
				}
			}
			return nil
		}, nil
	case reflect.Slice:
		return sliceMarshaller(typ.Elem(), "")
	case reflect.Uint8:
		return uintMarshaller(8), nil
	case reflect.Uint16:
		return uintMarshaller(16), nil
	case reflect.Uint32:
		return uintMarshaller(32), nil
	case reflect.Uint64:
		return uintMarshaller(64), nil
	case reflect.Int32:
		return func(w io.Writer, rv reflect.Value) error {
			v := uint32(rv.Int())
			return uint32Marshal(w, v)
		}, nil
	case reflect.Float32:
		return func(w io.Writer, rv reflect.Value) error {
			v := math.Float32bits(float32(rv.Float()))
			return uint32Marshal(w, v)
		}, nil
	case reflect.String:
		return func(w io.Writer, rv reflect.Value) error {
			_, err := w.Write([]byte(rv.String()))
			return err
		}, nil
	case reflect.Bool:
		return func(w io.Writer, rv reflect.Value) error {
			if rv.Bool() {
				_, err := w.Write([]byte{1})
				return err
			}
			_, err := w.Write([]byte{0})
			return err
		}, nil
	default:
		return nil, errors.New("tlv8.marshal, unsupported type: " + typ.Kind().String())
	}
}

// uintMarshaller encodes on the given bitSize.
// Representation could be optimized by skipping 0s, but some libraries don't support this.
func uintMarshaller(bitSize int) func(io.Writer, reflect.Value) error {
	return func(w io.Writer, rv reflect.Value) error {
		v := rv.Uint()
		buf := make([]byte, bitSize/8)
		i := 0
		for v := v; v != 0; v >>= 8 {
			buf[i] = byte(v)
			i++
		}
		_, err := w.Write(buf)
		return err
	}
}

func uint32Marshal(w io.Writer, v uint32) error {
	buf := make([]byte, 32/8)
	i := 0
	for v := v; v != 0; v >>= 8 {
		buf[i] = byte(v)
		i++
	}
	_, err := w.Write(buf)
	return err
}

func writeTLV(w io.Writer, tag byte, value []byte) error {
	if len(value) == 0 {
		_, err := w.Write([]byte{tag, 0})
		return err
	}

	start := 0
	for start < len(value) {
		l := len(value) - start
		if l > 255 {
			l = 255
		}
		chunk := value[start : start+l]
		_, err := w.Write([]byte{tag, byte(l)})
		if err != nil {
			return err
		}
		_, err = w.Write(chunk)
		if err != nil {
			return err
		}
		start += l
	}
	return nil
}
