package tlv8

import (
	"strconv"
	"strings"
)

func parseTag(s string) (uint8, error) {
	// pairing constants
	switch s {
	case "kTLVType_Method":
		return 0x00, nil
	case "kTLVType_Identifier":
		return 0x01, nil
	case "kTLVType_Salt":
		return 0x02, nil
	case "kTLVType_PublicKey":
		return 0x03, nil
	case "kTLVType_Proof":
		return 0x04, nil
	case "kTLVType_EncryptedData":
		return 0x05, nil
	case "kTLVType_State":
		return 0x06, nil
	case "kTLVType_Error":
		return 0x07, nil
	case "kTLVType_RetryDelay":
		return 0x08, nil
	case "kTLVType_Certificate":
		return 0x09, nil
	case "kTLVType_Signature":
		return 0x0A, nil
	case "kTLVType_Permissions":
		return 0x0B, nil
	case "kTLVType_FragmentData":
		return 0x0C, nil
	case "kTLVType_FragmentLast":
		return 0x0D, nil
	case "kTLVType_Flags":
		return 0x13, nil
	case "kTLVType_Separator":
		return 0xFF, nil
	}

	bitSize := 8
	// hexadecimal notation
	if strings.HasPrefix(s, "0x") {
		u, err := strconv.ParseUint(s[2:], 16, bitSize)
		return uint8(u), err
	}

	// decimal
	u, err := strconv.ParseUint(s, 10, bitSize)
	return uint8(u), err
}
