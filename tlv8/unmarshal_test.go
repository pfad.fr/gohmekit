package tlv8

import (
	"testing"

	"gotest.tools/v3/assert"
)

func TestUnmarshal(t *testing.T) {
	t.Run("simple struct", func(t *testing.T) {
		var request struct {
			State      byte   `tlv8:"kTLVType_State"`
			Method     byte   `tlv8:"0"`
			Identifier []byte `tlv8:"1"`
		}
		data := []byte{
			6, 1, 1,
			0, 1, 2,
			1, 3, 1, 2, 3,
		}
		err := Unmarshal(data, &request)
		assert.NilError(t, err)
		assert.Equal(t, request.State, byte(1))
		assert.Equal(t, request.Method, byte(2))
		assert.DeepEqual(t, request.Identifier, []byte{1, 2, 3})

		actual, err := Marshal(request)
		assert.NilError(t, err)
		assert.DeepEqual(t, data, actual)
	})
	t.Run("embedded struct", func(t *testing.T) {
		type embed struct {
			State byte `tlv8:"6"`
		}
		var request struct {
			embed
			Method     byte   `tlv8:"0"`
			Identifier []byte `tlv8:"1"`
		}
		data := []byte{
			6, 1, 1,
			0, 1, 2,
			1, 3, 1, 2, 3,
		}
		err := Unmarshal(data, &request)
		assert.NilError(t, err)
		assert.Equal(t, request.State, byte(1))
		assert.Equal(t, request.Method, byte(2))
		assert.DeepEqual(t, request.Identifier, []byte{1, 2, 3})

		actual, err := Marshal(request)
		assert.NilError(t, err)
		assert.DeepEqual(t, data, actual)
	})
}
