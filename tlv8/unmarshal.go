package tlv8

import (
	"bytes"
	"fmt"
	"io"
	"math"
	"reflect"
	"sync"
)

func Unmarshal(data []byte, v any) error {
	return NewDecoder(bytes.NewReader(data)).Decode(v)
}

type Decoder struct {
	r io.Reader
}

func NewDecoder(r io.Reader) Decoder {
	return Decoder{
		r: r,
	}
}

func (d Decoder) Decode(v any) error {
	typ := reflect.TypeOf(v)
	if typ.Kind() != reflect.Ptr || v == nil {
		return InvalidUnmarshalError{typ}
	}
	unmarshaller, err := prepareUnmarshaller(typ.Elem())
	if err != nil {
		return err
	}
	return unmarshaller(d.r, reflect.ValueOf(v).Elem())
}

// valueUnarshaller should only return an error, when an error happens on io.Writer.
type valueUnmarshaller func(io.Reader, reflect.Value) error

func structUnmarshaller(typ reflect.Type) (map[byte]func(io.Reader, reflect.Value, map[int]map[byte]bool) error, error) {
	m := make(map[byte]func(io.Reader, reflect.Value, map[int]map[byte]bool) error)

	for i := 0; i < typ.NumField(); i++ {
		i := i

		tf := typ.Field(i)
		tag, ok := tf.Tag.Lookup("tlv8")
		if !ok && !tf.Anonymous {
			continue
		}

		// embedded as-is
		if tag == "" || tag == "-" { //nolint:nestif
			switch tf.Type.Kind() {
			case reflect.Struct:
				stm, err := structUnmarshaller(tf.Type)
				if err != nil {
					return nil, err
				}
				for k, v := range stm {
					tag := k
					unmarshaller := v
					if _, ok := m[tag]; ok {
						return nil, fmt.Errorf(`multiple targets for tlv8:"%d"`, tag)
					}
					m[tag] = func(r io.Reader, rv reflect.Value, sFilled map[int]map[byte]bool) error {
						// we use the same sFilled, because even if there are multiple substructs,
						// we know that the `byte(tag)` are disjoint.
						return unmarshaller(r, rv.Field(i), sFilled)
					}
				}
			case reflect.Slice:
				childType := tf.Type.Elem()
				if childType.Kind() != reflect.Struct {
					return nil, InvalidUnmarshalError{typ}
				}
				stm, err := structUnmarshaller(childType)
				if err != nil {
					return nil, err
				}
				atm := anonymousSliceUnmarshaller(stm, childType)
				for k, v := range atm {
					tag := k
					unmarshaller := v
					if _, ok := m[tag]; ok {
						return nil, fmt.Errorf(`multiple targets for tlv8:"%d"`, tag)
					}
					m[tag] = func(r io.Reader, rv reflect.Value, sFilled map[int]map[byte]bool) error {
						if sFilled[i] == nil {
							sFilled[i] = make(map[byte]bool)
						}
						return unmarshaller(r, rv.Field(i), sFilled[i])
					}
				}
			default:
				return nil, InvalidUnmarshalError{typ}
			}
			continue
		}

		// prefix with tag and length
		k, err := parseTag(tag)
		if err != nil {
			return m, err
		}

		if _, ok := m[k]; ok {
			return nil, fmt.Errorf(`multiple targets for tlv8:"%d"`, k)
		}
		if tf.Type.Kind() == reflect.Slice { //nolint:nestif
			childType := tf.Type.Elem()
			if childType.Kind() != reflect.Uint8 {
				itemUnmarshaller, err := prepareUnmarshaller(childType) //nolint:govet
				if err != nil {
					return nil, err
				}
				m[k] = func(r io.Reader, rv reflect.Value, _ map[int]map[byte]bool) error {
					child := reflect.New(childType).Elem()
					if errf := itemUnmarshaller(r, child); errf != nil {
						return errf
					}
					rv.Field(i).Set(reflect.Append(rv.Field(i), child))
					return nil
				}
				continue
			}
		}
		unmarshaller, err := prepareUnmarshaller(tf.Type)
		if err != nil {
			return nil, err
		}
		m[k] = func(r io.Reader, rv reflect.Value, _ map[int]map[byte]bool) error {
			return unmarshaller(r, rv.Field(i))
		}
	}
	return m, nil
}

var unmarshallerCacheLock sync.RWMutex
var unmarshallerCache = make(map[reflect.Type]valueUnmarshaller)

func getCachedUnmarshaller(typ reflect.Type) valueUnmarshaller {
	unmarshallerCacheLock.RLock()
	defer unmarshallerCacheLock.RUnlock()
	return unmarshallerCache[typ]
}

func prepareUnmarshaller(typ reflect.Type) (vu valueUnmarshaller, err error) { //nolint:gocyclo,cyclop
	if cached := getCachedUnmarshaller(typ); cached != nil {
		return cached, nil
	}
	defer func() {
		if err == nil && vu != nil {
			unmarshallerCacheLock.Lock()
			defer unmarshallerCacheLock.Unlock()
			unmarshallerCache[typ] = vu
		}
	}()

	switch typ.Kind() {
	case reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
		return func(r io.Reader, rv reflect.Value) error {
			data, err := io.ReadAll(r)
			if err != nil {
				return err
			}
			u := uint64(0)
			for i, b := range data {
				u += uint64(b) << (i * 8)
			}
			rv.SetUint(u)
			return nil
		}, nil
	case reflect.Int32:
		return func(r io.Reader, rv reflect.Value) error {
			data, err := io.ReadAll(r)
			if err != nil {
				return err
			}
			u := uint64(0)
			for i, b := range data {
				u += uint64(b) << (i * 8)
			}
			rv.SetInt(int64(u))
			return nil
		}, nil
	case reflect.Float32:
		return func(r io.Reader, rv reflect.Value) error {
			data, err := io.ReadAll(r)
			if err != nil {
				return err
			}
			u := uint32(0)
			for i, b := range data {
				u += uint32(b) << (i * 8)
			}
			f := math.Float32frombits(u)
			rv.SetFloat(float64(f))
			return nil
		}, nil
	case reflect.Bool:
		return func(r io.Reader, rv reflect.Value) error {
			data, err := io.ReadAll(r)
			if err != nil {
				return err
			}
			if len(data) > 0 && data[0] == 1 {
				rv.SetBool(true)
			}
			return nil
		}, nil
	case reflect.String:
		return func(r io.Reader, rv reflect.Value) error {
			data, err := io.ReadAll(r)
			if err != nil {
				return err
			}
			rv.SetString(string(data))
			return nil
		}, nil
	case reflect.Struct:
		m, err := structUnmarshaller(typ)
		if err != nil {
			return nil, err
		}
		return func(r io.Reader, rv reflect.Value) error {
			sFilled := make(map[int]map[byte]bool)
			return readValues(r, func(tag byte, data []byte) error {
				if unmarshaller, ok := m[tag]; ok {
					return unmarshaller(bytes.NewReader(data), rv, sFilled)
				}
				return nil
			})
		}, nil
	case reflect.Slice:
		childType := typ.Elem()
		switch childType.Kind() {
		case reflect.Struct:
			m, err := structUnmarshaller(childType)
			if err != nil {
				return nil, err
			}
			am := anonymousSliceUnmarshaller(m, childType)
			return func(r io.Reader, rv reflect.Value) error {
				filledTags := make(map[byte]bool)
				return readValues(r, func(tag byte, data []byte) error {
					if unmarshaller, ok := am[tag]; ok {
						return unmarshaller(bytes.NewReader(data), rv, filledTags)
					}
					return nil
				})
			}, nil
		case reflect.Uint8:
			// byte slice
			return func(r io.Reader, rv reflect.Value) error {
				data, err := io.ReadAll(r)
				if err != nil {
					return err
				}
				rv.SetBytes(data)
				return nil
			}, nil
		default:
			return nil, InvalidUnmarshalError{typ}
		}
	case reflect.Pointer:
		unmarshaller, err := prepareUnmarshaller(typ.Elem())
		if err != nil {
			return nil, err
		}
		return func(r io.Reader, rv reflect.Value) error {
			ptr := reflect.New(typ.Elem())
			err := unmarshaller(r, ptr.Elem())
			if err != nil {
				return err
			}
			rv.Set(ptr)
			return nil
		}, nil
	default:
		return nil, InvalidUnmarshalError{typ}
	}
}

func anonymousSliceUnmarshaller(fields map[byte]func(io.Reader, reflect.Value, map[int]map[byte]bool) error, childType reflect.Type) map[byte]func(r io.Reader, rv reflect.Value, filledTags map[byte]bool) error {
	out := make(map[byte]func(r io.Reader, rv reflect.Value, filledTags map[byte]bool) error, len(fields))
	for k, v := range fields {
		tag := k
		unmarshaller := v

		out[tag] = func(r io.Reader, rv reflect.Value, filledTags map[byte]bool) error {
			appendNew := filledTags[tag] // if this tag is set, append a new item
			if !appendNew {
				// if no other field is set, append a new item
				appendNew = true
				for outTag := range out {
					if filledTags[outTag] {
						// except if some other tag already has a value
						appendNew = false
						break
					}
				}
			}

			filledTags[tag] = true
			sFilled := make(map[int]map[byte]bool)
			// adjust last element
			if !appendNew {
				child := rv.Index(rv.Len() - 1)
				return unmarshaller(r, child, sFilled)
			}

			// append new element
			for outTag := range out {
				if tag != outTag && filledTags[outTag] {
					filledTags[outTag] = false
				}
			}
			child := reflect.New(childType).Elem()
			err := unmarshaller(r, child, sFilled)
			if err != nil {
				return err
			}
			rv.Set(reflect.Append(rv, child))
			return nil
		}
	}
	return out
}

func readValues(r io.Reader, cb func(tag byte, data []byte) error) error {
	tag, err := readByte(r)
	if err != nil {
		if err == io.ErrUnexpectedEOF { //nolint:errorlint
			err = nil
		}
		return err
	}

	buf := []byte{}
	for {
		length, err := readByte(r)
		if err != nil {
			return err
		}

		if length > 0 {
			value, err := readFull(r, length) //nolint:govet
			if err != nil {
				return err
			}
			buf = append(buf, value...)
		}

		nextTag, err := readByte(r)
		if err != nil {
			if err == io.ErrUnexpectedEOF { //nolint:errorlint
				// exit case
				// ignore EOF error
				err = cb(tag, buf)
			}
			return err
		}

		if length != 255 || nextTag != tag {
			err = cb(tag, buf)
			if err != nil {
				return err
			}
			buf = []byte{}
		}
		tag = nextTag
	}
}

func readByte(r io.Reader) (byte, error) {
	b, err := readFull(r, 1)
	return b[0], err
}

func readFull(r io.Reader, l byte) ([]byte, error) {
	b := make([]byte, l)
	_, err := io.ReadFull(r, b)
	if err == io.EOF { //nolint:errorlint
		err = io.ErrUnexpectedEOF
	}
	return b, err
}

type InvalidUnmarshalError struct {
	Type reflect.Type
}

func (e InvalidUnmarshalError) Error() string {
	if e.Type == nil {
		return "tlv8: Unmarshal(nil)"
	}
	if e.Type.Kind() != reflect.Ptr {
		return "tlv8: Unmarshal(non-pointer " + e.Type.String() + ")"
	}

	return "tlv8: Unmarshal(nil " + e.Type.String() + ")"
}
