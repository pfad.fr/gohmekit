package pairing_test

import (
	"bytes"
	"crypto/ed25519"
	"io"
	"net"
	"net/http"
	"testing"
	"time"

	"code.pfad.fr/gohmekit/pairing"
	"code.pfad.fr/gohmekit/storage"
	"code.pfad.fr/gohmekit/tlv8"
	"github.com/brutella/hc/db"
	"github.com/brutella/hc/hap"
	"github.com/brutella/hc/hap/pair"
	"github.com/brutella/hc/util"
	"gotest.tools/v3/assert"
)

// Table 5-3.
const (
	kTLVMethod_PairSetup     = 0
	kTLVMethod_AddPairing    = 3
	kTLVMethod_RemovePairing = 4
	kTLVMethod_ListPairings  = 5
)

// Table 5-5.
const (
	kTLVError_Unavailable = 0x06
)

func tlv8Marshal(t *testing.T, v any) io.Reader {
	m, err := tlv8.Marshal(v)
	if err != nil {
		t.Fatal(err)
	}
	return bytes.NewReader(m)
}

func TestPairSetupVerifyIntegration(t *testing.T) {
	type request struct {
		State      byte   `tlv8:"kTLVType_State"`
		Method     byte   `tlv8:"kTLVType_Method"`
		Identifier []byte `tlv8:"kTLVType_Identifier"`
		PublicKey  []byte `tlv8:"kTLVType_PublicKey"`
	}
	type response struct {
		State byte `tlv8:"kTLVType_State"`
		Error byte `tlv8:"kTLVType_Error"`
	}

	deviceDatabase := storage.NewMemDatabase()
	_, privateKey, err := ed25519.GenerateKey(nil)
	assert.NilError(t, err)
	device, err := pairing.NewDeviceWithPin(pairing.NewRandomPairingID(), "001-02-003", privateKey)
	assert.NilError(t, err)

	s := pairing.NewServer(&http.Server{
		ReadTimeout: 5 * time.Second,
	}, device, deviceDatabase)

	addr := startServer(t, s)

	clientStorage, err := util.NewFileStorage(t.TempDir())
	assert.NilError(t, err)
	clientDatabase := db.NewDatabaseWithStorage(clientStorage)

	client, err := hap.NewDevice("HomeKit Client", clientDatabase)
	assert.NilError(t, err)

	httpClient, encryptClientConn := newHTTPClient()

	unauthorizedPairings, err := httpClient.Post(addr+"pairings", pairing.ContentType, nil)
	assert.NilError(t, err)
	assert.Equal(t, 470, unauthorizedPairings.StatusCode)
	_, err = io.ReadAll(unauthorizedPairings.Body)
	assert.NilError(t, err)
	assert.NilError(t, unauthorizedPairings.Body.Close())

	//////////////////////////
	// setup of the pairing //
	//////////////////////////
	setupController := pair.NewSetupClientController("001-02-003", client, clientDatabase)

	// 5.6.1) C -> S
	srpStartResponse, err := httpClient.Post(addr+"pair-setup", pairing.ContentType, setupController.InitialPairingRequest())
	assert.NilError(t, err)
	assert.Equal(t, 200, srpStartResponse.StatusCode)

	// 5.6.2) S -> C
	srpVerifyRequest, err := pair.HandleReaderForHandler(srpStartResponse.Body, setupController)
	assert.NilError(t, err)
	assert.NilError(t, srpStartResponse.Body.Close())

	// 5.6.3) C -> S
	srpVerifyResponse, err := httpClient.Post(addr+"pair-setup", pairing.ContentType, srpVerifyRequest)
	assert.NilError(t, err)
	assert.Equal(t, 200, srpVerifyResponse.StatusCode)

	// 5.6.4) S -> C
	exchangeRequest, err := pair.HandleReaderForHandler(srpVerifyResponse.Body, setupController)
	assert.NilError(t, err)
	assert.NilError(t, srpVerifyResponse.Body.Close())

	// 5.6.3) C -> S
	exchangeResponse, err := httpClient.Post(addr+"pair-setup", pairing.ContentType, exchangeRequest)
	assert.NilError(t, err)
	assert.Equal(t, 200, exchangeResponse.StatusCode)

	setupCompleted, err := pair.HandleReaderForHandler(exchangeResponse.Body, setupController)
	assert.NilError(t, err)
	assert.NilError(t, exchangeResponse.Body.Close())
	assert.Equal(t, setupCompleted, nil)

	////////////////////////
	// verify the pairing //
	////////////////////////
	verify, err := pairing.NewVerifyClientController(
		HCDevice{Device: client},
		HCDatabase{Database: clientDatabase},
	)
	assert.NilError(t, err)

	// 5.7.1) C -> S
	verifyStartResponse, err := httpClient.Post(addr+"pair-verify", pairing.ContentType, bytes.NewReader(verify.StartRequest()))
	assert.NilError(t, err)
	assert.Equal(t, 200, verifyStartResponse.StatusCode)

	// 2) S -> C
	verifyFinishRequest, sharedSecret, err := verify.FinishRequest(verifyStartResponse.Body)
	assert.NilError(t, err)

	// 3) C -> S
	verifyFinishResponse, err := httpClient.Post(addr+"pair-verify", pairing.ContentType, bytes.NewReader(verifyFinishRequest))
	assert.NilError(t, err)
	assert.Equal(t, 200, verifyFinishResponse.StatusCode)

	// // 4) S -> C
	err = verify.FinishResponse(verifyFinishResponse.Body)
	assert.NilError(t, err)

	// encrypt the connection
	var sharedSecretArray [32]byte
	copy(sharedSecretArray[:], sharedSecret)
	err = encryptClientConn(sharedSecretArray)
	assert.NilError(t, err)

	t.Log("list the pairing")
	////////////////////////
	// list the pairings  //
	////////////////////////
	pairingList, err := httpClient.Post(addr+"pairings", pairing.ContentType, tlv8Marshal(t, request{
		State:  1,
		Method: kTLVMethod_ListPairings,
	}))
	assert.NilError(t, err)
	assert.Equal(t, 200, pairingList.StatusCode)

	type controller struct {
		Identifier  []byte   `tlv8:"kTLVType_Identifier"`
		PublicKey   []byte   `tlv8:"kTLVType_PublicKey"`
		Permissions byte     `tlv8:"kTLVType_Permissions"`
		Separator   struct{} `tlv8:"kTLVType_Separator"`
	}
	type listResponse struct {
		State       byte         `tlv8:"kTLVType_State"`
		Controllers []controller `tlv8:""`
	}
	var listed listResponse
	err = tlv8.NewDecoder(pairingList.Body).Decode(&listed)
	assert.NilError(t, err)
	assert.Equal(t, 1, len(listed.Controllers))
	assert.DeepEqual(t, []byte("HomeKit Client"), listed.Controllers[0].Identifier)

	////////////////////////
	// add pairing  //
	////////////////////////
	newPublic, _, err := ed25519.GenerateKey(nil)
	assert.NilError(t, err)
	pairingAdd, err := httpClient.Post(addr+"pairings", pairing.ContentType, tlv8Marshal(t, request{
		State:      1,
		Method:     kTLVMethod_AddPairing,
		Identifier: []byte("new controller"),
		PublicKey:  newPublic,
	}))
	assert.NilError(t, err)
	assert.Equal(t, 200, pairingList.StatusCode)

	var added response
	err = tlv8.NewDecoder(pairingAdd.Body).Decode(&added)
	assert.NilError(t, err)
	assert.Equal(t, byte(0), added.Error)

	////////////////////////
	// list the pairings  //
	////////////////////////
	pairingList, err = httpClient.Post(addr+"pairings", pairing.ContentType, tlv8Marshal(t, request{
		State:  1,
		Method: kTLVMethod_ListPairings,
	}))
	assert.NilError(t, err)
	assert.Equal(t, 200, pairingList.StatusCode)

	listed = listResponse{}
	err = tlv8.NewDecoder(pairingList.Body).Decode(&listed)
	assert.NilError(t, err)
	assert.Equal(t, 2, len(listed.Controllers))

	////////////////////////
	// remove pairing  //
	////////////////////////
	pairingRemove, err := httpClient.Post(addr+"pairings", pairing.ContentType, tlv8Marshal(t, request{
		State:      1,
		Method:     kTLVMethod_RemovePairing,
		Identifier: []byte("new controller"),
	}))
	assert.NilError(t, err)
	assert.Equal(t, 200, pairingRemove.StatusCode)

	var removed response
	err = tlv8.NewDecoder(pairingRemove.Body).Decode(&removed)
	assert.NilError(t, err)
	assert.Equal(t, byte(0), removed.Error)

	////////////////////////
	// list the pairings  //
	////////////////////////
	pairingList, err = httpClient.Post(addr+"pairings", pairing.ContentType, tlv8Marshal(t, request{
		State:  1,
		Method: kTLVMethod_ListPairings,
	}))
	assert.NilError(t, err)
	assert.Equal(t, 200, pairingList.StatusCode)

	listed = listResponse{}
	err = tlv8.NewDecoder(pairingList.Body).Decode(&listed)
	assert.NilError(t, err)
	assert.Equal(t, 1, len(listed.Controllers))
	assert.DeepEqual(t, []byte("HomeKit Client"), listed.Controllers[0].Identifier)
}

func newHTTPClient() (*http.Client, func(sharedKey [32]byte) error) {
	dial, encrypt := pairing.NewEncryptableDialer((&net.Dialer{
		Timeout:   5 * time.Second,
		KeepAlive: 5 * time.Second,
	}).DialContext)
	httpClient := http.Client{
		Transport: &http.Transport{
			Proxy:                 http.ProxyFromEnvironment,
			DialContext:           dial,
			ForceAttemptHTTP2:     false,
			MaxIdleConns:          1,
			IdleConnTimeout:       5 * time.Second,
			TLSHandshakeTimeout:   5 * time.Second,
			ExpectContinueTimeout: 5 * time.Second,
		},
	}
	return &httpClient, encrypt
}

func ExampleNewEncryptableDialer() {
	dial, encrypt := pairing.NewEncryptableDialer((&net.Dialer{
		Timeout:   5 * time.Second,
		KeepAlive: 5 * time.Second,
	}).DialContext)
	httpClient := http.Client{
		Transport: &http.Transport{
			Proxy:                 http.ProxyFromEnvironment,
			DialContext:           dial,
			ForceAttemptHTTP2:     false,
			MaxIdleConns:          1,
			IdleConnTimeout:       5 * time.Second,
			TLSHandshakeTimeout:   5 * time.Second,
			ExpectContinueTimeout: 5 * time.Second,
		},
	}
	// do whatever you need with the httpClient
	// call encrypt(sharedKey) to encrypt further communications.
	_ = encrypt
	_ = httpClient
}

func TestPairSetupAlreadyPaired(t *testing.T) {
	deviceDatabase := storage.NewMemDatabase()
	_, privateKey, err := ed25519.GenerateKey(nil)
	assert.NilError(t, err)
	device, err := pairing.NewDeviceWithPin(pairing.NewRandomPairingID(), "001-02-003", privateKey)
	assert.NilError(t, err)

	s := pairing.NewServer(&http.Server{
		ReadTimeout: 5 * time.Second,
	}, device, deviceDatabase)

	addr := startServer(t, s)

	err = deviceDatabase.AddLongTermPublicKey(pairing.Controller{PairingID: []byte("HomeKit client"), LongTermPublicKey: []byte{0x01}})
	assert.NilError(t, err)

	httpClient, _ := newHTTPClient()

	srpStartResponse, err := httpClient.Post(addr+"pair-setup", pairing.ContentType, tlv8Marshal(t, struct {
		State  byte `tlv8:"kTLVType_State"`
		Method byte `tlv8:"kTLVType_Method"`
	}{
		State:  1,
		Method: kTLVMethod_PairSetup,
	}))
	assert.NilError(t, err)
	assert.Equal(t, 200, srpStartResponse.StatusCode)

	type response struct {
		State byte `tlv8:"kTLVType_State"`
		Error byte `tlv8:"kTLVType_Error"`
	}
	var resp response
	err = tlv8.NewDecoder(srpStartResponse.Body).Decode(&resp)
	assert.NilError(t, err)
	assert.Equal(t, resp.Error, byte(kTLVError_Unavailable)) // kTLVError_Unavailable
}
