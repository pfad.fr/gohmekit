//nolint:godot
package pairing

import "code.pfad.fr/gohmekit/tlv8"

// Table 5-3.
const (
	kTLVMethod_PairSetup = 0
	// kTLVMethod_PairSetupWithAuth = 1 // not supported (yet)
	// kTLVMethod_PairVerify        = 2 // not used in the spec...
	kTLVMethod_AddPairing    = 3
	kTLVMethod_RemovePairing = 4
	kTLVMethod_ListPairings  = 5
)

// Table 5-5.
const (
	kTLVError_Unknown        = 0x01
	kTLVError_Authentication = 0x02
	// kTLVError_Backoff        = 0x03
	// kTLVError_MaxPeers       = 0x04
	// kTLVError_MaxTries       = 0x05
	kTLVError_Unavailable = 0x06
	// kTLVError_Busy           = 0x07
)

// Table 5-6.
const (
	// kTLVType_Method        = 0x00
	// kTLVType_Identifier    = 0x01
	// kTLVType_Salt          = 0x02
	// kTLVType_PublicKey     = 0x03
	// kTLVType_Proof         = 0x04
	// kTLVType_EncryptedData = 0x05
	kTLVType_State = 0x06
	// kTLVType_Error         = 0x07
	// kTLVType_RetryDelay    = 0x08
	// kTLVType_Certificate   = 0x09
	// kTLVType_Signature     = 0x0A
	// kTLVType_Permissions   = 0x0B
	// kTLVType_FragmentData  = 0x0C
	// kTLVType_FragmentLast  = 0x0D
	// kTLVType_Flags         = 0x13
	// kTLVType_Separator     = 0xFF
)

type ktlvState struct {
	State byte `tlv8:"kTLVType_State"`
}

func ktlvError(state, err byte) ([]byte, error) {
	return tlv8.Marshal(struct {
		ktlvState
		Error byte `tlv8:"kTLVType_Error"`
	}{
		ktlvState: ktlvState{state},
		Error:     err,
	})
}
