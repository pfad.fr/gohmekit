package pairing

import "net"

type encryptableListener struct {
	net.Listener
}

var _ net.Listener = encryptableListener{}

func (l encryptableListener) Accept() (net.Conn, error) {
	conn, err := l.Listener.Accept()
	return &encryptableConn{
		Conn: conn,
	}, err
}
