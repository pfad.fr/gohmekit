package pairing

import (
	"context"
	"io"
	"net"
	"net/http"

	"code.pfad.fr/gohmekit/hapip"
	"github.com/go-kit/log"
)

const ContentType = "application/pairing+tlv8"

type option func(*HTTPServer)

// WithLogger adds structured logging to the pairing server.
func WithLogger(logger log.Logger) option {
	return func(h *HTTPServer) {
		h.Logger = logger
	}
}

// WithIdentify allows to specify a function to call when the device should physically identify itself (before pairing).
func WithIdentify(cb func()) option {
	return func(h *HTTPServer) {
		h.Identify = cb
	}
}

// NewServer creates a new pairing server. Once the accessory is paired, it will forward all decrypted
// communications to the given server.
func NewServer(server *http.Server, device AccessoryDevice, db Database, options ...option) *HTTPServer {
	h := &HTTPServer{
		Logger:   log.NewNopLogger(),
		Device:   device,
		Database: db,

		server: server,
	}
	for _, o := range options {
		o(h)
	}
	return h
}

// HTTPServer must be created with NewServer and can be adjusted afterwards.
type HTTPServer struct {
	Identify func()     // when called, the device must identify itself (by sound, light...)
	Logger   log.Logger // github.com/go-kit/log.NewNopLogger() if you don't want any log

	Device   AccessoryDevice
	Database Database

	server *http.Server
}

// Listener returns a new listener on the TCP network address of the underlying http.Server (server.Addr).
//
// If the address is blank, ":http" is used.
func (srv *HTTPServer) Listener() (net.Listener, error) {
	addr := srv.server.Addr
	if addr == "" {
		addr = ":http"
	}
	return net.Listen("tcp", addr)
}

// ListenAndServe listens on the TCP network address of the underlying
// http.Server (server.Addr) and then calls Serve to handle requests on incoming connections.
//
// If the address is blank, ":http" is used.
//
// ListenAndServe always returns a non-nil error. After Shutdown or Close,
// the returned error is ErrServerClosed.
func (srv *HTTPServer) ListenAndServe() error {
	ln, err := srv.Listener()
	if err != nil {
		return err
	}
	return srv.Serve(ln)
}

// Shutdown gracefully shuts down the underlying http.Server.
func (srv *HTTPServer) Shutdown(ctx context.Context) error {
	return srv.server.Shutdown(ctx)
}

// Serve accepts incoming connections on the Listener l, creating a
// new service goroutine for each.
//
// Serve always returns a non-nil error and closes l.
// After Shutdown or Close, the returned error is ErrServerClosed.
func (srv *HTTPServer) Serve(ln net.Listener) error {
	srv.server.Handler = srv.wrapHandler(srv.server.Handler)
	if connContext := srv.server.ConnContext; connContext != nil {
		srv.server.ConnContext = func(ctx context.Context, c net.Conn) context.Context {
			ctx = saveConnInContext(ctx, c)
			return connContext(ctx, c)
		}
	} else {
		srv.server.ConnContext = saveConnInContext
	}
	return srv.server.Serve(encryptableListener{ln})
}

type contextKey struct {
	key string
}

var connContextKey = &contextKey{"http-conn"}

func saveConnInContext(ctx context.Context, c net.Conn) context.Context {
	return context.WithValue(ctx, connContextKey, c)
}
func getEncryptableConn(r *http.Request) *encryptableConn {
	return r.Context().Value(connContextKey).(*encryptableConn) // panic if the underlying conn is not encryptable
}

func (srv *HTTPServer) onlyEncryptedMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
		conn := getEncryptableConn(r)
		if conn == nil || conn.Session == nil {
			// HTTP 470: authorization required
			if err := hapip.WriteStatus(rw, 470, hapip.StatusInsufficientPrivileges); err != nil {
				srv.Logger.Log("middleware", "onlyEncrypted", "err", err)
			}
			return
		}

		next.ServeHTTP(rw, r)
	})
}

func (srv *HTTPServer) wrapHandler(authenticated http.Handler) http.Handler {
	mux := http.NewServeMux()
	mux.Handle("/", srv.onlyEncryptedMiddleware(authenticated))

	mux.HandleFunc("/pair-setup", srv.pairingMiddleware(srv.pairSetup))
	mux.HandleFunc("/pair-verify", srv.pairingMiddleware(srv.pairVerify))
	mux.Handle("/pairings", srv.onlyEncryptedMiddleware(srv.pairingMiddleware(srv.pairings)))
	mux.HandleFunc("/identify", srv.identify)
	return mux
}

func (srv *HTTPServer) identify(rw http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		rw.WriteHeader(http.StatusMethodNotAllowed)
		return
	}
	if srv.Database.IsPaired() {
		if err := hapip.WriteStatus(rw, http.StatusBadRequest, hapip.StatusInsufficientPrivileges); err != nil {
			srv.Logger.Log("endpoint", "identify", "err", err)
		}
		return
	}
	if srv.Identify != nil {
		srv.Identify()
	}
	rw.WriteHeader(http.StatusNoContent)
}

type statePeeker struct {
	buf []byte
}

func (sp *statePeeker) Write(p []byte) (int, error) {
	if len(sp.buf) > 3 {
		return len(p), nil
	}
	max := 3 - len(sp.buf)
	if len(p) < max {
		max = len(p)
	}
	sp.buf = append(sp.buf, p[:max]...)
	return len(p), nil
}
func (sp statePeeker) State() byte {
	if len(sp.buf) < 3 || sp.buf[0] != kTLVType_State || sp.buf[1] != 1 {
		return 255
	}
	return sp.buf[2]
}

func (srv *HTTPServer) pairingMiddleware(requestHandler func(*encryptableConn, io.Reader) ([]byte, error)) http.HandlerFunc {
	return func(rw http.ResponseWriter, r *http.Request) {
		logger := log.With(srv.Logger, "uri", r.URL.EscapedPath())
		if r.Method != http.MethodPost {
			rw.WriteHeader(http.StatusMethodNotAllowed)
			return
		}

		conn := getEncryptableConn(r)
		sp := &statePeeker{}
		resp, err := requestHandler(conn, io.TeeReader(r.Body, sp))

		rw.Header().Set("Content-Type", ContentType)
		if err != nil {
			rw.WriteHeader(http.StatusInternalServerError)
			state := sp.State()
			logger.Log("state", state, "resp", resp, "err", err)
			// overwrite response

			resp, _ = ktlvError(state+1, kTLVError_Unknown)
		}
		_, _ = rw.Write(resp)
	}
}
