package pairing

import (
	"crypto/ed25519"
	"errors"
	"fmt"
	"io"

	"code.pfad.fr/gohmekit/tlv8"
)

type pairSetupRequest struct {
	ktlvState
	Method byte `tlv8:"kTLVType_Method"`

	Identifier    []byte `tlv8:"kTLVType_Identifier"`
	PublicKey     []byte `tlv8:"kTLVType_PublicKey"`
	Proof         []byte `tlv8:"kTLVType_Proof"`
	EncryptedData []byte `tlv8:"kTLVType_EncryptedData"`
}

func (srv *HTTPServer) pairSetup(conn *encryptableConn, r io.Reader) ([]byte, error) {
	var req pairSetupRequest
	err := tlv8.NewDecoder(r).Decode(&req)
	if err != nil {
		return nil, err
	}

	switch req.State {
	case 1:
		return srv.pairSetupStartResponse(conn, req)
	case 3:
		return srv.pairSetupVerifyResponse(conn, req)
	case 5:
		return srv.pairSetupExchangeResponse(conn, req)
	default:
		return nil, fmt.Errorf("unexpected state: %d", req.State)
	}
}

func (srv *HTTPServer) pairSetupStartResponse(conn *encryptableConn, req pairSetupRequest) ([]byte, error) {
	if req.Method != kTLVMethod_PairSetup {
		return nil, fmt.Errorf("unsupported method: %d", req.Method)
	}

	if srv.Database.IsPaired() {
		return ktlvError(2, kTLVError_Unavailable)
	}

	session, salt, err := srv.Device.SRPSession()
	if err != nil {
		return nil, err
	}

	conn.pairSetup.SRPSession = session
	conn.pairSetup.SRPSharedSecret = nil

	return tlv8.Marshal(struct {
		ktlvState
		Salt      []byte `tlv8:"kTLVType_Salt"`
		PublicKey []byte `tlv8:"kTLVType_PublicKey"`
	}{
		ktlvState: ktlvState{2},
		PublicKey: session.PublicKey(),
		Salt:      salt,
	})
}

func (srv *HTTPServer) pairSetupVerifyResponse(conn *encryptableConn, req pairSetupRequest) ([]byte, error) {
	if conn.pairSetup.SRPSession == nil {
		return nil, errors.New("shared secret has not been constructed yet")
	}

	iOSPublicKey := req.PublicKey
	sessionKey, err := conn.pairSetup.SRPSession.PairSetupSharedSecret(iOSPublicKey)
	if err != nil {
		return nil, err
	}

	clientProof := req.Proof
	serverProof, ok := conn.pairSetup.SRPSession.ExchangeProof(clientProof)
	if !ok {
		return ktlvError(4, kTLVError_Authentication)
	}

	conn.pairSetup.SRPSharedSecret = sessionKey

	return tlv8.Marshal(struct {
		ktlvState
		Proof []byte `tlv8:"kTLVType_Proof"`
	}{
		ktlvState: ktlvState{4},
		Proof:     serverProof,
	})
}

func (srv *HTTPServer) pairSetupExchangeResponse(conn *encryptableConn, req pairSetupRequest) ([]byte, error) {
	if len(conn.pairSetup.SRPSharedSecret) == 0 {
		return nil, errors.New("session key has not been constructed yet")
	}

	aead, err := conn.pairSetup.SRPSharedSecret.AEAD()
	if err != nil {
		return nil, err
	}

	encryptedData := req.EncryptedData
	decryptedData, err := aead.Open(encryptedData[:0], paddedNonce("PS-Msg05"), encryptedData, nil)
	if err != nil {
		return ktlvError(6, kTLVError_Authentication)
	}

	type subTLV struct {
		Identifier []byte `tlv8:"kTLVType_Identifier"`
		PublicKey  []byte `tlv8:"kTLVType_PublicKey"`
		Signature  []byte `tlv8:"kTLVType_Signature"`
	}
	var decodedData subTLV
	err = tlv8.Unmarshal(decryptedData, &decodedData)
	if err != nil {
		return nil, fmt.Errorf("could not decode encrypted data: %w", err)
	}

	iOSDevicePairingID := decodedData.Identifier
	iOSDeviceLTPK := decodedData.PublicKey
	iOSDeviceSignature := decodedData.Signature

	hash, err := conn.pairSetup.SRPSharedSecret.ControllerSign()
	if err != nil {
		return nil, fmt.Errorf("could not decode encrypted data: %w", err)
	}

	var iOSDeviceInfo []byte
	iOSDeviceInfo = append(iOSDeviceInfo, hash...)
	iOSDeviceInfo = append(iOSDeviceInfo, iOSDevicePairingID...)
	iOSDeviceInfo = append(iOSDeviceInfo, iOSDeviceLTPK...)

	if !ed25519.Verify(iOSDeviceLTPK, iOSDeviceInfo, iOSDeviceSignature) {
		return ktlvError(6, kTLVError_Authentication)
	}

	err = srv.Database.AddLongTermPublicKey(Controller{PairingID: iOSDevicePairingID, LongTermPublicKey: iOSDeviceLTPK})
	if err != nil {
		return nil, fmt.Errorf("could not decode encrypted data: %w", err)
	}

	hash, err = conn.pairSetup.SRPSharedSecret.AccessorySign()
	if err != nil {
		return nil, fmt.Errorf("could not decode encrypted data: %w", err)
	}

	var accessoryInfo []byte
	accessoryInfo = append(accessoryInfo, hash...)
	accessoryInfo = append(accessoryInfo, srv.Device.PairingID()...)
	accessoryInfo = append(accessoryInfo, srv.Device.OwnLongTermPublicKey()...)

	accessorySignature, err := srv.Device.Ed25519Sign(accessoryInfo)
	if err != nil {
		return nil, fmt.Errorf("could not decode encrypted data: %w", err)
	}

	cleartextData, err := tlv8.Marshal(subTLV{
		Identifier: srv.Device.PairingID(),
		PublicKey:  srv.Device.OwnLongTermPublicKey(),
		Signature:  accessorySignature,
	})
	if err != nil {
		return nil, err
	}

	encryptedData = aead.Seal(cleartextData[:0], paddedNonce("PS-Msg06"), cleartextData, nil)

	conn.pairSetup.SRPSession = nil
	conn.pairSetup.SRPSharedSecret = nil

	srv.Logger.Log("completed", "pair-setup", "id", string(iOSDevicePairingID))

	return tlv8.Marshal(struct {
		ktlvState
		EncryptedData []byte `tlv8:"kTLVType_EncryptedData"`
	}{
		ktlvState:     ktlvState{6},
		EncryptedData: encryptedData,
	})
}
