package pairing_test

import (
	"code.pfad.fr/gohmekit/pairing"
	"github.com/brutella/hc/db"
)

var _ pairing.Database = HCDatabase{}

type HCDatabase struct {
	db.Database
}

func (d HCDatabase) IsPaired() bool {
	entities, _ := d.Entities()
	for _, e := range entities {
		if len(e.PrivateKey) == 0 {
			return true
		}
	}
	return false
}

func (d HCDatabase) GetLongTermPublicKey(id []byte) ([]byte, error) {
	e, err := d.EntityWithName(string(id))
	if err != nil {
		return nil, err
	}
	return e.PublicKey, nil
}

func (d HCDatabase) AddLongTermPublicKey(c pairing.Controller) error {
	return d.SaveEntity(db.NewEntity(string(c.PairingID), c.LongTermPublicKey, nil))
}

func (d HCDatabase) RemoveLongTermPublicKey(id []byte) error {
	d.DeleteEntity(db.NewEntity(string(id), nil, nil))
	return nil
}

func (d HCDatabase) ListLongTermPublicKey() ([]pairing.Controller, error) {
	entities, err := d.Entities()
	if err != nil {
		return nil, err
	}
	c := make([]pairing.Controller, 0, len(entities))
	for _, e := range entities {
		c = append(c, pairing.Controller{
			PairingID:         []byte(e.Name),
			LongTermPublicKey: e.PublicKey,
		})
	}
	return c, nil
}
