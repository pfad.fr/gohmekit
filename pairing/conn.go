package pairing

import (
	"log"
	"net"
	"sync"

	"code.pfad.fr/gohmekit/pairing/crypto"
)

type encryptableConn struct {
	net.Conn

	sync.Mutex
	Session       *crypto.Session
	readRemain    []byte
	EncryptWrites bool // when the connection has just been encrypted, the next write must still be in cleartext

	pairSetup struct {
		SRPSession      AccessorySRPSession
		SRPSharedSecret crypto.SRPSharedSecret
	}
	pairVerify struct {
		SharedSecret       []byte
		iOSDevicePublicKey []byte
		ownPublicKey       []byte
	}
}

var _ net.Conn = &encryptableConn{}

func (c *encryptableConn) StartEncryptedSession(sharedKey [32]byte, isAccessory bool) (err error) {
	c.Mutex.Lock()
	defer c.Mutex.Unlock()

	c.Session, err = crypto.NewSession(sharedKey, isAccessory)
	if c.Session != nil && !isAccessory {
		c.EncryptWrites = true
	}
	return err
}

// Read reads bytes from the connection. The read bytes are decrypted when possible.
func (c *encryptableConn) Read(b []byte) (int, error) {
	if len(c.readRemain) > 0 {
		n := copy(b, c.readRemain)
		if len(c.readRemain) == n {
			c.readRemain = nil
		} else {
			c.readRemain = c.readRemain[n:]
		}
		return n, nil
	}

	n, err := c.Conn.Read(b)
	if n == 0 {
		return n, err
	}

	c.Mutex.Lock()
	sess := c.Session
	c.EncryptWrites = sess != nil
	c.Mutex.Unlock()

	if sess == nil {
		return n, err
	}

	for {
		cleartext, err := sess.Open(b[:n])
		if err != nil {
			log.Println(err)
			c.Conn.Close()
			return 0, err
		}

		// must read more from connection
		if len(cleartext) == 0 {
			n, err = c.Conn.Read(b)
			if n == 0 {
				return n, err
			}
			continue
		}
		n = copy(b, cleartext)
		if n < len(cleartext) {
			c.readRemain = cleartext[n:]
		}
		return n, err
	}
}

// Write writes bytes to the connection.
// The written bytes are encrypted when possible.
func (c *encryptableConn) Write(b []byte) (int, error) {
	c.Mutex.Lock()
	var sess *crypto.Session
	if c.EncryptWrites {
		sess = c.Session
	}
	c.Mutex.Unlock()

	l := len(b)
	if sess != nil {
		b = sess.Seal(b)
	}

	_, err := c.Conn.Write(b)
	if err != nil {
		return 0, err
	}
	return l, nil
}
