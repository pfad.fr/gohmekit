package pairing

import (
	"crypto/ed25519"
	"errors"
	"fmt"
	"io"

	"code.pfad.fr/gohmekit/pairing/crypto"
	"code.pfad.fr/gohmekit/tlv8"
)

type pairVerifyRequest struct {
	ktlvState

	PublicKey     []byte `tlv8:"kTLVType_PublicKey"`
	EncryptedData []byte `tlv8:"kTLVType_EncryptedData"`
}

func (srv *HTTPServer) pairVerify(conn *encryptableConn, r io.Reader) ([]byte, error) {
	var req pairVerifyRequest
	err := tlv8.NewDecoder(r).Decode(&req)
	if err != nil {
		return nil, err
	}

	switch req.State {
	case 1:
		return srv.pairVerifyStartResponse(conn, req)
	case 3:
		return srv.pairVerifyFinishResponse(conn, req)
	default:
		return nil, fmt.Errorf("unexpected state: %d", req.State)
	}
}

func (srv *HTTPServer) pairVerifyStartResponse(conn *encryptableConn, req pairVerifyRequest) ([]byte, error) {
	key, err := crypto.NewKeyOnCurve25519()
	if err != nil {
		return nil, err
	}

	conn.pairVerify.ownPublicKey = key.PublicKey()
	conn.pairVerify.iOSDevicePublicKey = req.PublicKey

	conn.pairVerify.SharedSecret, err = key.PairVerifySharedSecret(conn.pairVerify.iOSDevicePublicKey)
	if err != nil {
		return nil, err
	}

	var accessoryInfo []byte
	accessoryInfo = append(accessoryInfo, key.PublicKey()...)
	accessoryInfo = append(accessoryInfo, srv.Device.PairingID()...)
	accessoryInfo = append(accessoryInfo, conn.pairVerify.iOSDevicePublicKey...)

	accessorySignature, err := srv.Device.Ed25519Sign(accessoryInfo)
	if err != nil {
		return nil, err
	}

	cleartextData, err := tlv8.Marshal(struct {
		Identifier []byte `tlv8:"kTLVType_Identifier"`
		Signature  []byte `tlv8:"kTLVType_Signature"`
	}{
		Identifier: srv.Device.PairingID(),
		Signature:  accessorySignature,
	})
	if err != nil {
		return nil, err
	}

	aead, err := crypto.PairVerifyAEAD(conn.pairVerify.SharedSecret)
	if err != nil {
		return nil, err
	}

	encryptedData := aead.Seal(cleartextData[:0], paddedNonce("PV-Msg02"), cleartextData, nil)

	return tlv8.Marshal(struct {
		ktlvState
		PublicKey     []byte `tlv8:"kTLVType_PublicKey"`
		EncryptedData []byte `tlv8:"kTLVType_EncryptedData"`
	}{
		ktlvState:     ktlvState{2},
		PublicKey:     key.PublicKey(),
		EncryptedData: encryptedData,
	})
}

func (srv *HTTPServer) pairVerifyFinishResponse(conn *encryptableConn, req pairVerifyRequest) ([]byte, error) {
	if len(conn.pairVerify.SharedSecret) == 0 {
		return nil, errors.New("shared secret has not been constructed yet")
	}

	aead, err := crypto.PairVerifyAEAD(conn.pairVerify.SharedSecret)
	if err != nil {
		return nil, err
	}

	encryptedData := req.EncryptedData
	decryptedData, err := aead.Open(encryptedData[:0], paddedNonce("PV-Msg03"), encryptedData, nil)
	if err != nil {
		return ktlvError(4, kTLVError_Authentication)
	}

	var decodedData struct {
		Identifier []byte `tlv8:"kTLVType_Identifier"`
		Signature  []byte `tlv8:"kTLVType_Signature"`
	}
	err = tlv8.Unmarshal(decryptedData, &decodedData)
	if err != nil {
		return nil, fmt.Errorf("could not decode encrypted data: %w", err)
	}

	iOSDevicePairingID := decodedData.Identifier
	iOSDeviceSignature := decodedData.Signature

	iOSDeviceLTPK, err := srv.Database.GetLongTermPublicKey(iOSDevicePairingID)
	if err != nil {
		return nil, err
	}

	var iOSDeviceInfo []byte
	iOSDeviceInfo = append(iOSDeviceInfo, conn.pairVerify.iOSDevicePublicKey...)
	iOSDeviceInfo = append(iOSDeviceInfo, iOSDevicePairingID...)
	iOSDeviceInfo = append(iOSDeviceInfo, conn.pairVerify.ownPublicKey...)

	if !ed25519.Verify(iOSDeviceLTPK, iOSDeviceInfo, iOSDeviceSignature) {
		return ktlvError(4, kTLVError_Authentication)
	}

	var sharedKey [32]byte
	copy(sharedKey[:], conn.pairVerify.SharedSecret)

	conn.pairVerify.ownPublicKey = nil
	conn.pairVerify.iOSDevicePublicKey = nil
	conn.pairVerify.SharedSecret = nil

	srv.Logger.Log("completed", "pair-verify", "id", string(iOSDevicePairingID))

	err = conn.StartEncryptedSession(sharedKey, true)
	if err != nil {
		return nil, err
	}
	return tlv8.Marshal(ktlvState{4})
}
