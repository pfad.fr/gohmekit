package pairing_test

import (
	"crypto/ed25519"

	"code.pfad.fr/gohmekit/pairing"
	"code.pfad.fr/gohmekit/pairing/crypto"
	"github.com/brutella/hc/hap"
)

var _ pairing.Device = HCDevice{}

type HCDevice struct {
	hap.Device
}

func (h HCDevice) PairingID() []byte {
	return []byte(h.Device.Name())
}

func (h HCDevice) Ed25519Sign(msg []byte) ([]byte, error) {
	return ed25519.Sign(h.Device.PrivateKey(), msg), nil
}

func (h HCDevice) OwnLongTermPublicKey() []byte {
	return h.Device.PublicKey()
}

var _ pairing.AccessoryDevice = DeviceWithPin{}

type DeviceWithPin struct {
	hap.SecuredDevice
}

func (h DeviceWithPin) PairingID() []byte {
	return []byte(h.SecuredDevice.Name())
}

func (h DeviceWithPin) Ed25519Sign(msg []byte) ([]byte, error) {
	return ed25519.Sign(h.SecuredDevice.PrivateKey(), msg), nil
}

func (h DeviceWithPin) OwnLongTermPublicKey() []byte {
	return h.SecuredDevice.PublicKey()
}

func (h DeviceWithPin) SRPSession() (pairing.AccessorySRPSession, []byte, error) {
	return crypto.NewSRPSession([]byte(h.SecuredDevice.Pin()))
}
