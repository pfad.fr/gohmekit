package pairing

// Database interface for the accessory to store its state.
type Database interface {
	IsPaired() bool
	GetLongTermPublicKey([]byte) ([]byte, error)
	AddLongTermPublicKey(Controller) error
	RemoveLongTermPublicKey(id []byte) error
	ListLongTermPublicKey() ([]Controller, error)
}

// Controller is used to store the devices in the Database.
type Controller struct {
	PairingID         []byte
	LongTermPublicKey []byte
}
