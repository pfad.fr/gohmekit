package pairing_test

import (
	"net"
	"net/http"
	"testing"
	"time"

	"code.pfad.fr/gohmekit/pairing"
	"code.pfad.fr/gohmekit/storage"
	"github.com/go-kit/log"
	"gotest.tools/v3/assert"
)

type testingLogger struct {
	*testing.T
}

func (t testingLogger) Log(keyvals ...interface{}) error {
	t.Helper()
	t.T.Log(append([]interface{}{time.Now().Second()}, keyvals...)...)
	return nil
}

func startServer(t *testing.T, s *pairing.HTTPServer) string {
	s.Logger = log.With(testingLogger{t}, "side", "acc")
	ln, err := net.Listen("tcp", ":0")
	assert.NilError(t, err)
	t.Cleanup(func() {
		assert.NilError(t, ln.Close())
	})
	go s.Serve(ln) //nolint:errcheck
	return "http://" + ln.Addr().String() + "/"
}

func TestServerIdentify(t *testing.T) {
	deviceDatabase := storage.NewMemDatabase()
	called := 0

	s := pairing.NewServer(&http.Server{}, nil, deviceDatabase,
		pairing.WithIdentify(func() {
			called++
		}),
	)

	addr := startServer(t, s)
	t.Log(addr)

	resp, err := http.Post(addr+"identify", "", nil)
	assert.NilError(t, err)
	assert.Equal(t, 204, resp.StatusCode)
	assert.Equal(t, 1, called)

	err = deviceDatabase.AddLongTermPublicKey(pairing.Controller{})
	assert.NilError(t, err)

	resp, err = http.Post(addr+"identify", "", nil)
	assert.NilError(t, err)
	assert.Equal(t, 400, resp.StatusCode)
	assert.Equal(t, 1, called)
}
