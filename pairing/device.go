package pairing

import (
	"bytes"
	"crypto/ed25519"
	"crypto/rand"
	"encoding/hex"
	"errors"
	"fmt"
	"io"
	"math/big"
	"regexp"

	"code.pfad.fr/gohmekit/pairing/crypto"
)

func formatPairingID(id []byte) []byte {
	out := &bytes.Buffer{}

	wh := hex.NewEncoder(out)
	isFirst := true
	for _, b := range id {
		if !isFirst {
			out.WriteByte(':')
		} else {
			isFirst = false
		}
		wh.Write([]byte{b}) //nolint:errcheck // bytes.Buffer.Write always return nil error
	}

	return bytes.ToUpper(out.Bytes())
}

// Device interface must be implemented by the controller to support pairing.
type Device interface {
	PairingID() []byte
	Ed25519Sign([]byte) ([]byte, error)
	OwnLongTermPublicKey() []byte
}

// AccessoryDevice interface must be implemented by the accessory to support pairing.
type AccessoryDevice interface {
	Device
	SRPSession() (sess AccessorySRPSession, salt []byte, err error)
}

type AccessorySRPSession interface {
	PublicKey() []byte
	PairSetupSharedSecret([]byte) ([]byte, error)
	ExchangeProof([]byte) ([]byte, bool)
}

var pinRegex = regexp.MustCompile(`^[0-9]{3}-[0-9]{2}-[0-9]{3}$`)

// NewRandomPin generates a random 48-bits pairingID.
func NewRandomPairingID() []byte {
	buf := make([]byte, 6)
	_, err := io.ReadFull(rand.Reader, buf)
	if err != nil {
		panic(err)
	}
	return formatPairingID(buf)
}

// NewRandomPin generates a random pin (XXX-XX-XXX).
func NewRandomPin() string {
	a, err := rand.Int(rand.Reader, big.NewInt(999))
	if err != nil {
		panic(err)
	}
	b, err := rand.Int(rand.Reader, big.NewInt(99))
	if err != nil {
		panic(err)
	}
	c, err := rand.Int(rand.Reader, big.NewInt(999))
	if err != nil {
		panic(err)
	}
	return fmt.Sprintf("%03d-%02d-%03d", a.Int64(), b.Int64(), c.Int64())
}

// NewDeviceWithPin creates a new AccessoryDevice with the given id, pin and private key.
func NewDeviceWithPin(deviceID []byte, pin string, ed25519PrivateKey []byte) (AccessoryDevice, error) {
	if len(ed25519PrivateKey) != ed25519.PrivateKeySize {
		return nil, fmt.Errorf("expected private key lenth: %d, got: %d", len(ed25519PrivateKey), ed25519.PrivateKeySize)
	}

	if !pinRegex.Match([]byte(pin)) {
		return nil, errors.New("pin must be of the form XXX-XX-XXX")
	}

	publicKey := make([]byte, ed25519.PublicKeySize)
	copy(publicKey, ed25519PrivateKey[32:])

	return device{
		id:         deviceID,
		privateKey: ed25519PrivateKey,
		publicKey:  publicKey,
		pin:        []byte(pin),
	}, nil
}

type device struct {
	id         []byte
	privateKey []byte
	publicKey  []byte
	pin        []byte
}

func (d device) PairingID() []byte {
	return d.id
}

func (d device) Ed25519Sign(msg []byte) ([]byte, error) {
	return ed25519.Sign(d.privateKey, msg), nil
}

func (d device) OwnLongTermPublicKey() []byte {
	return d.publicKey
}

func (d device) SRPSession() (AccessorySRPSession, []byte, error) {
	return crypto.NewSRPSession(d.pin)
}
