package pairing_test

import (
	"crypto/ed25519"
	"net"
	"net/http"
	"testing"

	"code.pfad.fr/gohmekit/pairing"
	"code.pfad.fr/gohmekit/storage"
	"github.com/brutella/hc/db"
	"github.com/brutella/hc/hap"
	"github.com/brutella/hc/hap/pair"
	"github.com/brutella/hc/util"
	"gotest.tools/v3/assert"
)

// Tests the pairing key verification.
func TestPairVerifyIntegration(t *testing.T) {
	database := storage.NewMemDatabase()
	_, privateKey, err := ed25519.GenerateKey(nil)
	assert.NilError(t, err)
	bridge, err := pairing.NewDeviceWithPin(pairing.NewRandomPairingID(), "001-02-003", privateKey)
	assert.NilError(t, err)

	s := pairing.NewServer(&http.Server{}, bridge, database)

	addr := startServer(t, s)

	clientStorage, err := util.NewFileStorage(t.TempDir())
	assert.NilError(t, err)

	clientDatabase := db.NewDatabaseWithStorage(clientStorage)
	bridgeEntity := db.NewEntity(string(bridge.PairingID()), bridge.OwnLongTermPublicKey(), nil)
	err = clientDatabase.SaveEntity(bridgeEntity)
	assert.NilError(t, err)

	client, err := hap.NewDevice("HomeKit Client", clientDatabase)
	assert.NilError(t, err)
	err = database.AddLongTermPublicKey(pairing.Controller{PairingID: []byte(client.Name()), LongTermPublicKey: client.PublicKey()})
	assert.NilError(t, err)

	clientController := pair.NewVerifyClientController(client, clientDatabase)

	tlvVerifyStepStartRequest := clientController.InitialKeyVerifyRequest()

	httpClient, encryptClientConn := newHTTPClient()
	_ = encryptClientConn

	// 1) C -> S
	resp, err := httpClient.Post(addr+"pair-verify", pairing.ContentType, tlvVerifyStepStartRequest)
	assert.NilError(t, err)
	assert.Equal(t, 200, resp.StatusCode)

	// 2) S -> C
	tlvFinishRequest, err := pair.HandleReaderForHandler(resp.Body, clientController)
	assert.NilError(t, err)
	assert.NilError(t, resp.Body.Close())

	// 3) C -> S
	resp, err = httpClient.Post(addr+"pair-verify", pairing.ContentType, tlvFinishRequest)
	assert.NilError(t, err)
	assert.Equal(t, 200, resp.StatusCode)

	// // 4) S -> C
	response, err := pair.HandleReaderForHandler(resp.Body, clientController)
	assert.NilError(t, err)
	assert.NilError(t, resp.Body.Close())
	assert.Equal(t, response, nil)
}

func BenchmarkPairVerify(b *testing.B) {
	database := storage.NewMemDatabase()
	_, privateKey, err := ed25519.GenerateKey(nil)

	assert.NilError(b, err)
	bridge, err := pairing.NewDeviceWithPin(pairing.NewRandomPairingID(), "001-02-003", privateKey)
	assert.NilError(b, err)

	s := pairing.NewServer(&http.Server{}, bridge, database)

	ln, err := net.Listen("tcp", ":0")
	assert.NilError(b, err)
	defer func() {
		assert.NilError(b, ln.Close())
	}()
	go s.Serve(ln) //nolint:errcheck
	addr := "http://" + ln.Addr().String() + "/"

	clientStorage, err := util.NewFileStorage(b.TempDir())
	assert.NilError(b, err)

	clientDatabase := db.NewDatabaseWithStorage(clientStorage)
	bridgeEntity := db.NewEntity(string(bridge.PairingID()), bridge.OwnLongTermPublicKey(), nil)
	err = clientDatabase.SaveEntity(bridgeEntity)
	assert.NilError(b, err)

	client, err := hap.NewDevice("HomeKit Client", clientDatabase)
	assert.NilError(b, err)
	err = database.AddLongTermPublicKey(pairing.Controller{PairingID: []byte(client.Name()), LongTermPublicKey: client.PublicKey()})
	assert.NilError(b, err)

	for i := 0; i < b.N; i++ {
		clientController := pair.NewVerifyClientController(client, clientDatabase)

		tlvVerifyStepStartRequest := clientController.InitialKeyVerifyRequest()

		httpClient, encryptClientConn := newHTTPClient()
		_ = encryptClientConn

		// 1) C -> S
		resp, err := httpClient.Post(addr+"pair-verify", pairing.ContentType, tlvVerifyStepStartRequest)
		assert.NilError(b, err)
		assert.Equal(b, 200, resp.StatusCode)

		// 2) S -> C
		tlvFinishRequest, err := pair.HandleReaderForHandler(resp.Body, clientController)
		assert.NilError(b, err)
		assert.NilError(b, resp.Body.Close())

		// 3) C -> S
		resp, err = httpClient.Post(addr+"pair-verify", pairing.ContentType, tlvFinishRequest)
		assert.NilError(b, err)
		assert.Equal(b, 200, resp.StatusCode)

		// // 4) S -> C
		response, err := pair.HandleReaderForHandler(resp.Body, clientController)
		assert.NilError(b, err)
		assert.NilError(b, resp.Body.Close())
		assert.Equal(b, response, nil)
	}
}
