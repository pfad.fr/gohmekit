package pairing

import (
	"testing"

	"gotest.tools/v3/assert"
)

func TestFormatDeviceID(t *testing.T) {
	cc := map[string]struct {
		input  []byte
		output string
	}{
		"empty": {
			input:  nil,
			output: "",
		},
		"one byte": {
			input:  []byte{1},
			output: "01",
		},
		"uppercase": {
			input:  []byte{10},
			output: "0A",
		},
		"two bytes": {
			input:  []byte{1, 2},
			output: "01:02",
		},
		"6 bytes (48 bits)": {
			input:  []byte{1, 32, 42, 56, 255, 0},
			output: "01:20:2A:38:FF:00",
		},
	}
	for name, c := range cc {
		t.Run(name, func(t *testing.T) {
			out := formatPairingID(c.input)
			assert.Equal(t, c.output, string(out))
		})
	}
}
