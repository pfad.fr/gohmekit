package pairing

import (
	"bytes"
	"fmt"
	"io"

	"code.pfad.fr/gohmekit/tlv8"
)

type pairingsRequest struct {
	ktlvState
	Method byte `tlv8:"kTLVType_Method"`

	// for pairingsAdd and pairingsRemove
	Identifier []byte `tlv8:"kTLVType_Identifier"`
	// for pairingsAdd
	PublicKey []byte `tlv8:"kTLVType_PublicKey"`
}

func (srv *HTTPServer) pairings(_ *encryptableConn, r io.Reader) ([]byte, error) {
	var req pairingsRequest
	err := tlv8.NewDecoder(r).Decode(&req)
	if err != nil {
		return nil, err
	}

	if req.State != 1 {
		return nil, fmt.Errorf("unexpected state: %d", req.State)
	}

	switch req.Method {
	case kTLVMethod_AddPairing:
		return srv.pairingsAdd(req)
	case kTLVMethod_RemovePairing:
		return srv.pairingsRemove(req)
	case kTLVMethod_ListPairings:
		return srv.pairingsList(req)
	default:
		return nil, fmt.Errorf("unexpected method: %d", req.Method)
	}
}

func (srv *HTTPServer) pairingsAdd(req pairingsRequest) ([]byte, error) {
	saved, err := srv.Database.GetLongTermPublicKey(req.Identifier)
	if err == nil && len(saved) > 0 {
		if !bytes.Equal(saved, req.PublicKey) {
			return ktlvError(2, kTLVError_Unknown)
		}
		// TODO: update permissions?

		return tlv8.Marshal(ktlvState{2})
	}

	err = srv.Database.AddLongTermPublicKey(Controller{
		PairingID:         req.Identifier,
		LongTermPublicKey: req.PublicKey,
	})
	if err != nil {
		return nil, err
	}
	return tlv8.Marshal(ktlvState{2})
}

func (srv *HTTPServer) pairingsRemove(req pairingsRequest) ([]byte, error) {
	err := srv.Database.RemoveLongTermPublicKey(req.Identifier)
	if err != nil {
		return nil, err
	}

	return tlv8.Marshal(ktlvState{2})
}

func (srv *HTTPServer) pairingsList(_ pairingsRequest) ([]byte, error) {
	controllers, err := srv.Database.ListLongTermPublicKey()
	if err != nil {
		return nil, err
	}

	type controller struct {
		Identifier  []byte   `tlv8:"kTLVType_Identifier"`
		PublicKey   []byte   `tlv8:"kTLVType_PublicKey"`
		Permissions byte     `tlv8:"kTLVType_Permissions"`
		Separator   struct{} `tlv8:"kTLVType_Separator"`
	}
	response := struct {
		ktlvState
		Controllers []controller `tlv8:""`
	}{
		ktlvState:   ktlvState{2},
		Controllers: make([]controller, 0, len(controllers)),
	}

	for _, c := range controllers {
		response.Controllers = append(response.Controllers, controller{
			Identifier: c.PairingID,
			PublicKey:  c.LongTermPublicKey,
			// Permissions: 0,
		})
	}

	return tlv8.Marshal(response)
}
