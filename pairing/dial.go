package pairing

import (
	"context"
	"errors"
	"net"
	"time"
)

// NewEncryptableDialer should be used for homekit client, to wrap a (&net.Dialer{...}).DialContext (see Example).
func NewEncryptableDialer(dial func(ctx context.Context, network string, address string) (net.Conn, error)) (dialContext func(ctx context.Context, network string, address string) (net.Conn, error), encrypt func(sharedKey [32]byte) error) {
	if dial == nil {
		dial = (&net.Dialer{
			Timeout:   30 * time.Second,
			KeepAlive: 30 * time.Second,
		}).DialContext
	}

	var previousNetwork, prevousAddress string
	var previousConn *encryptableConn

	encryptableDial := func(ctx context.Context, network string, address string) (net.Conn, error) {
		if previousConn != nil {
			if network != previousNetwork || address != prevousAddress {
				return nil, errors.New("a connection has already been made to another address")
			}
			return previousConn, errors.New("to reuse the connection, drain and close the body")
		}
		conn, err := dial(ctx, network, address)
		if conn == nil {
			return conn, err
		}
		previousConn = &encryptableConn{
			Conn: conn,
		}
		previousNetwork = network
		prevousAddress = address
		return previousConn, err
	}

	encryptConns := func(sharedKey [32]byte) error {
		return previousConn.StartEncryptedSession(sharedKey, false)
	}

	return encryptableDial, encryptConns
}
