package pairing

import (
	"crypto/ed25519"
	"fmt"
	"io"

	"code.pfad.fr/gohmekit/pairing/crypto"
	"code.pfad.fr/gohmekit/tlv8"
)

// NewVerifyClientController implements the client logic for the pairing-verify step.
func NewVerifyClientController(client Device, database Database) (*VerifyClientController, error) {
	key, err := crypto.NewKeyOnCurve25519()
	if err != nil {
		return nil, err
	}
	return &VerifyClientController{
		key:      key,
		client:   client,
		database: database,
	}, nil
}

// VerifyClientController implements the client logic for the pairing-verify step.
type VerifyClientController struct {
	key      crypto.KeyOnCurve25519
	client   Device
	database Database
}

// StartRequest is the initial pairing-verify request.
func (c VerifyClientController) StartRequest() []byte {
	buf, err := tlv8.Marshal(struct {
		ktlvState
		PublicKey []byte `tlv8:"kTLVType_PublicKey"`
	}{
		ktlvState: ktlvState{1},
		PublicKey: c.key.PublicKey(),
	})
	if err != nil {
		panic(err)
	}
	return buf
}

func paddedNonce(s string) []byte {
	n := make([]byte, 12)
	copy(n[4:], s)
	return n
}

// FinishRequest checks the accessory initial response and generate the finish request.
func (c VerifyClientController) FinishRequest(r io.Reader) (response []byte, sharedSecret []byte, err error) {
	var data struct {
		ktlvState
		PublicKey     []byte `tlv8:"kTLVType_PublicKey"`
		EncryptedData []byte `tlv8:"kTLVType_EncryptedData"`
	}
	err = tlv8.NewDecoder(r).Decode(&data)
	if err != nil {
		return nil, nil, err
	}

	if data.State != 2 {
		return nil, nil, fmt.Errorf("unexpected state: %d", data.State)
	}

	accessoryPublicKey := data.PublicKey
	if len(accessoryPublicKey) != 32 {
		return nil, nil, fmt.Errorf("unexpected accessory key lenth: %d", len(accessoryPublicKey))
	}
	sharedSecret, err = c.key.PairVerifySharedSecret(accessoryPublicKey)
	if err != nil {
		return nil, nil, fmt.Errorf("could not compute sharedSecret: %w", err)
	}

	aead, err := crypto.PairVerifyAEAD(sharedSecret)
	if err != nil {
		return nil, nil, err
	}

	{
		encryptedData := data.EncryptedData
		decryptedData, err := aead.Open(encryptedData[:0], paddedNonce("PV-Msg02"), encryptedData, nil)
		if err != nil {
			return nil, nil, fmt.Errorf("could not verify data: %w", err)
		}

		var decodedData struct {
			Identifier []byte `tlv8:"kTLVType_Identifier"`
			Signature  []byte `tlv8:"kTLVType_Signature"`
		}
		err = tlv8.Unmarshal(decryptedData, &decodedData)
		if err != nil {
			return nil, nil, fmt.Errorf("could not decode encrypted data: %w", err)
		}
		accessoryPairingID := decodedData.Identifier
		accessorySignature := decodedData.Signature

		accessoryLTPK, err := c.database.GetLongTermPublicKey(accessoryPairingID)
		if err != nil {
			return nil, nil, err
		}

		var accessoryInfo []byte
		accessoryInfo = append(accessoryInfo, accessoryPublicKey...)
		accessoryInfo = append(accessoryInfo, accessoryPairingID...)
		accessoryInfo = append(accessoryInfo, c.key.PublicKey()...)
		if !ed25519.Verify(accessoryLTPK, accessoryInfo, accessorySignature) {
			return nil, nil, fmt.Errorf("could not validate signature of AccessoryInfo")
		}
	}

	{
		// construct response
		var iOSDeviceInfo []byte
		iOSDeviceInfo = append(iOSDeviceInfo, c.key.PublicKey()...)
		iOSDeviceInfo = append(iOSDeviceInfo, c.client.PairingID()...)
		iOSDeviceInfo = append(iOSDeviceInfo, accessoryPublicKey...)

		iOSDeviceSignature, err := c.client.Ed25519Sign(iOSDeviceInfo)
		if err != nil {
			return nil, nil, fmt.Errorf("could not sign iOSDeviceInfo: %w", err)
		}

		cleartextData, err := tlv8.Marshal(struct {
			Identifier []byte `tlv8:"kTLVType_Identifier"`
			Signature  []byte `tlv8:"kTLVType_Signature"`
		}{
			Identifier: c.client.PairingID(),
			Signature:  iOSDeviceSignature,
		})
		if err != nil {
			return nil, nil, err
		}

		encryptedData := aead.Seal(cleartextData[:0], paddedNonce("PV-Msg03"), cleartextData, nil)

		response, err = tlv8.Marshal(struct {
			ktlvState
			EncryptedData []byte `tlv8:"kTLVType_EncryptedData"`
		}{
			ktlvState:     ktlvState{3},
			EncryptedData: encryptedData,
		})
		return response, sharedSecret, err
	}
}

// FinishResponse checks the response of the accessory. From now on, the connection must be encrypted
// using the sharedSecret computed in the FinishRequest step.
func (c VerifyClientController) FinishResponse(r io.Reader) error {
	var data struct {
		ktlvState
		Error byte `tlv8:"kTLVType_Error"`
	}
	err := tlv8.NewDecoder(r).Decode(&data)
	if err != nil {
		return err
	}

	if data.State != 4 {
		return fmt.Errorf("unexpected state: %d", data.State)
	}
	if data.Error != 0 {
		return fmt.Errorf("unexpected error: %d", data.Error)
	}
	return nil
}
