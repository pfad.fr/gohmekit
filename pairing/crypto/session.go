package crypto

import (
	"crypto/cipher"
	"encoding/binary"
	"fmt"
)

func NewSession(sharedKey [32]byte, isAccessory bool) (*Session, error) {
	salt := []byte("Control-Salt")
	readKey := []byte("Control-Read-Encryption-Key")
	writeKey := []byte("Control-Write-Encryption-Key")
	if isAccessory {
		writeKey, readKey = readKey, writeKey
	}

	sess := &Session{}
	aeadGenerator := hkdfChacha20poly1305Generator(sharedKey[:], salt)
	var err error
	sess.readAEAD, err = aeadGenerator(readKey)
	if err != nil {
		return nil, err
	}

	sess.writeAEAD, err = aeadGenerator(writeKey)
	if err != nil {
		return nil, err
	}

	return sess, nil
}

type Session struct {
	readAEAD   cipher.AEAD
	readCount  uint64
	readRemain []byte

	writeAEAD  cipher.AEAD
	writeCount uint64
}

func (s *Session) Seal(plaintext []byte) []byte {
	if len(plaintext) == 0 {
		return nil
	}

	var encrypted []byte
	offset := 0
	for len(plaintext)-offset > 1024 {
		encrypted = append(encrypted, s.sealChunk(plaintext[offset:offset+1024])...)
		offset += 1024
	}

	return append(encrypted, s.sealChunk(plaintext[offset:])...)
}

func (s *Session) sealChunk(plaintext []byte) []byte {
	nonce := make([]byte, 12)
	binary.LittleEndian.PutUint64(nonce[4:], s.writeCount)
	s.writeCount++

	// apparently in chacha20poly1305, ciphertext and plaintext have the same length
	// https://go.googlesource.com/crypto/+/master/chacha20poly1305/chacha20poly1305.go
	dst := make([]byte, 2, 2+len(plaintext)+s.writeAEAD.Overhead())

	binary.LittleEndian.PutUint16(dst, uint16(len(plaintext)))

	return s.writeAEAD.Seal(dst, nonce, plaintext, dst[:2])
}

// Open will decrypt and authenticate an incoming message.
// If not enough bytes are present, it will return (nil,nil), but
// keep the provided bytes in memory, to use them on the next call.
func (s *Session) Open(p []byte) ([]byte, error) {
	s.readRemain = append(s.readRemain, p...)

	var dst []byte
	i, b, err := s.openChunk(s.readRemain)
	for b != nil && err == nil {
		s.readRemain = s.readRemain[i:]
		dst = append(dst, b...)
		i, b, err = s.openChunk(s.readRemain)
	}
	s.readRemain = s.readRemain[i:]
	return dst, err
}

func (s *Session) openChunk(p []byte) (int, []byte, error) {
	if len(p) < 2+s.readAEAD.Overhead() {
		// surely not enought data
		return 0, nil, nil
	}

	length := binary.LittleEndian.Uint16(p)
	if length > 1024 {
		return 0, nil, fmt.Errorf("frame too large: %d", length)
	}

	cipherLength := int(length) + s.readAEAD.Overhead()
	if len(p) < 2+cipherLength {
		// not enought data yet
		return 0, nil, nil
	}

	nonce := make([]byte, 12)
	binary.LittleEndian.PutUint64(nonce[4:], s.readCount)
	s.readCount++

	out, err := s.readAEAD.Open(nil, nonce, p[2:cipherLength+2], p[:2])
	return 2 + cipherLength, out, err
}
