package crypto

import (
	"crypto/cipher"
)

type SRPSharedSecret []byte

func (s SRPSharedSecret) AEAD() (cipher.AEAD, error) {
	return hkdfChacha20poly1305Generator(s, []byte("Pair-Setup-Encrypt-Salt"))([]byte("Pair-Setup-Encrypt-Info"))
}

func (s SRPSharedSecret) ControllerSign() ([]byte, error) {
	return hkdfSha512(s, []byte("Pair-Setup-Controller-Sign-Salt"), []byte("Pair-Setup-Controller-Sign-Info"))
}

func (s SRPSharedSecret) AccessorySign() ([]byte, error) {
	return hkdfSha512(s, []byte("Pair-Setup-Accessory-Sign-Salt"), []byte("Pair-Setup-Accessory-Sign-Info"))
}
