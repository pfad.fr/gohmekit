package crypto

import (
	"crypto/cipher"
)

func PairVerifyAEAD(sharedSecret []byte) (cipher.AEAD, error) {
	return hkdfChacha20poly1305Generator(sharedSecret, []byte("Pair-Verify-Encrypt-Salt"))([]byte("Pair-Verify-Encrypt-Info"))
}
