package crypto_test

import (
	"bytes"
	"io"
	"testing"

	"code.pfad.fr/gohmekit/pairing/crypto"
	hcrypto "github.com/brutella/hc/crypto"
	"gotest.tools/v3/assert"
)

func TestShortSeal(t *testing.T) {
	data := []byte{0x01, 0x02, 0x03}
	key := [32]byte{
		0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08,
		0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08,
		0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08,
		0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08}

	accSession, err := crypto.NewSession(key, true)
	assert.NilError(t, err)

	client, err := hcrypto.NewSecureClientSessionFromSharedKey(key)
	assert.NilError(t, err)

	// Set count to min 2 bytes to test byte order handling
	encrypted := accSession.Seal(data)

	decrypted, err := client.Decrypt(bytes.NewReader(encrypted))
	assert.NilError(t, err)

	orig, err := io.ReadAll(decrypted)
	assert.NilError(t, err)
	assert.DeepEqual(t, orig, data)
}

func TestLongSeal(t *testing.T) {
	data := bytes.Repeat([]byte("1234567890"), 300)
	assert.Equal(t, 3000, len(data))

	key := [32]byte{
		0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08,
		0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08,
		0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08,
		0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08}

	accSession, err := crypto.NewSession(key, true)
	assert.NilError(t, err)

	client, err := hcrypto.NewSecureClientSessionFromSharedKey(key)
	assert.NilError(t, err)

	// Set count to min 2 bytes to test byte order handling
	encrypted := accSession.Seal(data)

	decrypted, err := client.Decrypt(bytes.NewReader(encrypted))
	assert.NilError(t, err)

	orig, err := io.ReadAll(decrypted)
	assert.NilError(t, err)
	assert.DeepEqual(t, orig, data)
}

func TestShortOpen(t *testing.T) {
	data := []byte{0x01, 0x02, 0x03}
	key := [32]byte{
		0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08,
		0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08,
		0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08,
		0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08}

	client, err := hcrypto.NewSecureClientSessionFromSharedKey(key)
	assert.NilError(t, err)

	// Set count to min 2 bytes to test byte order handling
	clientEncrypted, err := client.Encrypt(bytes.NewReader(data))
	assert.NilError(t, err)

	b, err := io.ReadAll(clientEncrypted)
	assert.NilError(t, err)

	accSession, err := crypto.NewSession(key, true)
	assert.NilError(t, err)

	out, err := accSession.Open(b)
	assert.NilError(t, err)

	assert.DeepEqual(t, out, data)
}

func TestOpenByChunks(t *testing.T) {
	data := []byte{0x01, 0x02, 0x03}
	key := [32]byte{
		0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08,
		0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08,
		0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08,
		0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08}

	client, err := hcrypto.NewSecureClientSessionFromSharedKey(key)
	assert.NilError(t, err)

	// Set count to min 2 bytes to test byte order handling
	clientEncrypted, err := client.Encrypt(bytes.NewReader(data))
	assert.NilError(t, err)

	b, err := io.ReadAll(clientEncrypted)
	assert.NilError(t, err)

	accSession, err := crypto.NewSession(key, true)
	assert.NilError(t, err)

	out, err := accSession.Open(b[0:2])
	assert.Check(t, nil == out)
	assert.NilError(t, err)

	out, err = accSession.Open(b[2:20])
	assert.Check(t, nil == out)
	assert.NilError(t, err)

	out, err = accSession.Open(b[20:])
	assert.NilError(t, err)

	assert.DeepEqual(t, out, data)
}

func TestLongOpen(t *testing.T) {
	data := bytes.Repeat([]byte("1234567890"), 300)
	assert.Equal(t, 3000, len(data))

	key := [32]byte{
		0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08,
		0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08,
		0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08,
		0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08}

	client, err := hcrypto.NewSecureClientSessionFromSharedKey(key)
	assert.NilError(t, err)

	// Set count to min 2 bytes to test byte order handling
	clientEncrypted, err := client.Encrypt(bytes.NewReader(data))
	assert.NilError(t, err)

	b, err := io.ReadAll(clientEncrypted)
	assert.NilError(t, err)

	accSession, err := crypto.NewSession(key, true)
	assert.NilError(t, err)

	out, err := accSession.Open(b)
	assert.NilError(t, err)

	assert.DeepEqual(t, out, data)
}
