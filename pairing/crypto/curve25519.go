package crypto

import (
	"crypto/rand"

	"golang.org/x/crypto/curve25519"
)

type KeyOnCurve25519 struct {
	privateKey []byte
	publicKey  []byte
}

func NewKeyOnCurve25519() (KeyOnCurve25519, error) {
	k := KeyOnCurve25519{
		privateKey: make([]byte, 32),
	}
	_, err := rand.Read(k.privateKey)
	if err != nil {
		return k, err
	}

	k.publicKey, err = curve25519.X25519(k.privateKey, curve25519.Basepoint)
	if err != nil {
		return k, err
	}

	return k, nil
}

func (k KeyOnCurve25519) PublicKey() []byte {
	return k.publicKey
}

func (k KeyOnCurve25519) PairVerifySharedSecret(otherPublicKey []byte) ([]byte, error) {
	return curve25519.X25519(k.privateKey, otherPublicKey)
}
