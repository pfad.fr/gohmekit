package crypto

import (
	"crypto/sha512"

	"github.com/tadglines/go-pkgs/crypto/srp"
)

type SRPSession struct {
	session *srp.ServerSession
}

// keyDerivativeFuncRFC2945 returns the SRP-6a key derivative function which does
// x = H(s | H(I | ":" | P))
//
// taken from github.com/brutella/hc@v1.2.4/hap/pair/srp.go.
func keyDerivativeFuncRFC2945(h srp.HashFunc, username []byte) srp.KeyDerivationFunc {
	return func(salt, pin []byte) []byte {
		h := h()
		h.Write(username)
		h.Write([]byte(":"))
		h.Write(pin)
		t2 := h.Sum(nil)
		h.Reset()
		h.Write(salt)
		h.Write(t2)
		return h.Sum(nil)
	}
}

func NewSRPSession(pin []byte) (*SRPSession, []byte, error) {
	username := []byte("Pair-Setup")
	srp, err := srp.NewSRP("rfc5054.3072", sha512.New, keyDerivativeFuncRFC2945(sha512.New, username))
	if err != nil {
		return nil, nil, err
	}

	srp.SaltLength = 16
	salt, v, err := srp.ComputeVerifier(pin)
	if err != nil {
		return nil, nil, err
	}

	return &SRPSession{srp.NewServerSession(username, salt, v)}, salt, nil
}

func (s SRPSession) PublicKey() []byte {
	return s.session.GetB()
}

func (s SRPSession) PairSetupSharedSecret(otherPublicKey []byte) ([]byte, error) {
	return s.session.ComputeKey(otherPublicKey)
}

func (s SRPSession) ExchangeProof(clientProof []byte) ([]byte, bool) {
	if !s.session.VerifyClientAuthenticator(clientProof) {
		return nil, false
	}
	return s.session.ComputeAuthenticator(clientProof), true
}
