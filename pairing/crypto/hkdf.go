package crypto

import (
	"crypto/cipher"
	"crypto/sha512"
	"io"

	"golang.org/x/crypto/chacha20poly1305"
	"golang.org/x/crypto/hkdf"
)

func read256bitsKey(r io.Reader) ([]byte, error) {
	key := make([]byte, 32) // 256 bit
	_, err := io.ReadFull(r, key)
	return key, err
}

// returns a 256-bit key.
func hkdfSha512(master, salt, info []byte) ([]byte, error) {
	hkdf := hkdf.New(sha512.New, master, salt, info)
	return read256bitsKey(hkdf)
}

func hkdfChacha20poly1305Generator(master, salt []byte) func(info []byte) (cipher.AEAD, error) {
	hash := sha512.New
	pkr := hkdf.Extract(hash, master, salt)
	return func(info []byte) (cipher.AEAD, error) {
		r := hkdf.Expand(hash, pkr, info)

		key, err := read256bitsKey(r)
		if err != nil {
			return nil, err
		}

		return chacha20poly1305.New(key)
	}
}
