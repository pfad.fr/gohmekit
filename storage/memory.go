package storage

import (
	"errors"

	"code.pfad.fr/gohmekit/pairing"
)

// NewMemDatabase is an in-memory database for testing (not thread-safe!)
func NewMemDatabase() pairing.Database {
	return memDatabase{}
}

type memDatabase map[string][]byte

func (m memDatabase) IsPaired() bool {
	return len(m) > 0
}
func (m memDatabase) GetLongTermPublicKey(id []byte) ([]byte, error) {
	if key, ok := m[string(id)]; ok {
		return key, nil
	}
	return nil, errors.New("unknown key")
}
func (m memDatabase) AddLongTermPublicKey(c pairing.Controller) error {
	m[string(c.PairingID)] = c.LongTermPublicKey
	return nil
}
func (m memDatabase) RemoveLongTermPublicKey(id []byte) error {
	delete(m, string(id))
	return nil
}
func (m memDatabase) ListLongTermPublicKey() ([]pairing.Controller, error) {
	c := make([]pairing.Controller, 0, len(m))
	for id, key := range m {
		c = append(c, pairing.Controller{
			PairingID:         []byte(id),
			LongTermPublicKey: key,
		})
	}
	return c, nil
}
