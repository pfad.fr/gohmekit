package storage

import (
	"crypto/ed25519"
	"encoding/json"
	"errors"
	"fmt"
	"io/fs"
	"log"
	"os"
	"path/filepath"
	"sync"

	"code.pfad.fr/gohmekit/discovery"
	"code.pfad.fr/gohmekit/pairing"
)

type jsonData struct {
	// for Database
	LongTermPublicKeys map[string][]byte

	// for AccessoryDevice
	PairingID         string
	Pin               string
	Ed25519PrivateKey []byte
	// for c# field
	Version uint16
}

func loadJsonData(path string) (*jsonData, error) {
	f, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	var data jsonData
	if err := json.NewDecoder(f).Decode(&data); err != nil {
		return nil, err
	}
	return &data, nil
}

func (j jsonData) NewAccessoryDevice() (pairing.AccessoryDevice, error) {
	return pairing.NewDeviceWithPin([]byte(j.PairingID), j.Pin, j.Ed25519PrivateKey)
}

// Option is to set some default values on first run.
type Option func(*jsonData)

// WithPairingID will set the pairing ID if it wasn't previously set.
func WithPairingID(id []byte) Option {
	return func(j *jsonData) {
		if len(j.PairingID) > 0 {
			return
		}
		j.PairingID = string(id)
	}
}

// WithPin will set the pin if it wasn't previously set.
func WithPin(pin string) Option {
	return func(j *jsonData) {
		if j.Pin != "" {
			return
		}
		j.Pin = pin
	}
}

// WithEd25519PrivateKey will set the private key if it wasn't previously set.
func WithEd25519PrivateKey(key []byte) Option {
	return func(j *jsonData) {
		if len(j.Ed25519PrivateKey) > 0 {
			return
		}
		j.Ed25519PrivateKey = key
	}
}

// NewJSONFile will use (and create if missing) a JSON file to act as a storage.
// It will use the given options, only if the concerned parameters are not already set.
// It will generate random pin and private key if let unspecified.
func NewJSONFile(path string, options ...Option) (*JSONFile, error) {
	data, err := loadJsonData(path)
	if err != nil {
		if !errors.Is(err, fs.ErrNotExist) {
			return nil, err
		}

		err = os.MkdirAll(filepath.Dir(path), 0700)
		if err != nil {
			return nil, fmt.Errorf("could not mkdir: %w", err)
		}
		data = &jsonData{}
	}

	for _, o := range options {
		o(data)
	}

	if len(data.PairingID) == 0 {
		data.PairingID = string(pairing.NewRandomPairingID())
	}
	if data.Pin == "" {
		data.Pin = pairing.NewRandomPin()
	}
	if len(data.LongTermPublicKeys) == 0 {
		data.LongTermPublicKeys = make(map[string][]byte)
		log.Println("generated pin:", data.Pin)
	}
	if len(data.Ed25519PrivateKey) == 0 {
		_, data.Ed25519PrivateKey, err = ed25519.GenerateKey(nil)
		if err != nil {
			return nil, err
		}
	}

	// increment version on every "boot"
	data.Version++
	if data.Version == 0 {
		data.Version = 1
	}

	j := &JSONFile{
		Path: path,
		data: data,
	}
	if err = j.overwriteLocked(); err != nil {
		return nil, err
	}
	j.AccessoryDevice, err = j.data.NewAccessoryDevice()
	if err != nil {
		return nil, err
	}
	return j, nil
}

// JSONFile implements pairing.Database and pairing.AccessoryDevice, storing their settings in a JSON file.
type JSONFile struct {
	Path string
	pairing.AccessoryDevice

	mu              sync.RWMutex
	data            *jsonData
	isPairedWatcher func(bool)
	// versionWatcher  func(uint16)
}

// the caller must held a Write lock.
func (j *JSONFile) overwriteLocked() error {
	f, err := os.Create(j.Path)
	if err != nil {
		return err
	}
	defer f.Close()
	if err := json.NewEncoder(f).Encode(j.data); err != nil {
		return err
	}
	return f.Close()
}

// /////////////////////////////
// pairing.Database interface //
// /////////////////////////////

var _ pairing.Database = &JSONFile{}

// IsPaired is defined by the pairing.Database interface.
func (j *JSONFile) IsPaired() bool {
	j.mu.RLock()
	defer j.mu.RUnlock()

	return len(j.data.LongTermPublicKeys) > 0
}

// IsPairedWatcher will trigger the callback on pairing change.
// It will overwrite any existing callback.
func (j *JSONFile) IsPairedWatcher(cb func(bool)) bool {
	j.mu.Lock()
	j.isPairedWatcher = cb
	j.mu.Unlock()

	return j.IsPaired()
}

// GetLongTermPublicKey is defined by the pairing.Database interface.
func (j *JSONFile) GetLongTermPublicKey(id []byte) ([]byte, error) {
	j.mu.RLock()
	defer j.mu.RUnlock()

	if key, ok := j.data.LongTermPublicKeys[string(id)]; ok {
		return key, nil
	}
	return nil, errors.New("unknown key")
}

// AddLongTermPublicKey is defined by the pairing.Database interface.
func (j *JSONFile) AddLongTermPublicKey(c pairing.Controller) error {
	j.mu.Lock()
	defer j.mu.Unlock()

	wasPaired := len(j.data.LongTermPublicKeys) > 0
	j.data.LongTermPublicKeys[string(c.PairingID)] = c.LongTermPublicKey
	isPaired := len(j.data.LongTermPublicKeys) > 0
	if isPaired != wasPaired && j.isPairedWatcher != nil {
		go j.isPairedWatcher(isPaired)
	}

	return j.overwriteLocked()
}

// RemoveLongTermPublicKey is defined by the pairing.Database interface.
func (j *JSONFile) RemoveLongTermPublicKey(id []byte) error {
	j.mu.Lock()
	defer j.mu.Unlock()
	delete(j.data.LongTermPublicKeys, string(id))
	return j.overwriteLocked()
}

// ListLongTermPublicKey is defined by the pairing.Database interface.
func (j *JSONFile) ListLongTermPublicKey() ([]pairing.Controller, error) {
	j.mu.RLock()
	defer j.mu.RUnlock()

	c := make([]pairing.Controller, 0, len(j.data.LongTermPublicKeys))
	for id, key := range j.data.LongTermPublicKeys {
		c = append(c, pairing.Controller{
			PairingID:         []byte(id),
			LongTermPublicKey: key,
		})
	}
	return c, nil
}

// ////////////////////////////////////
// pairing.AccessoryDevice interface //
// ////////////////////////////////////

var _ pairing.AccessoryDevice = &JSONFile{}

// VersionWatcher will never trigger the callback, since the version is changed
// on every restart.
func (j *JSONFile) VersionWatcher(_ func(uint16)) uint16 {
	// support updates?
	// j.mu.Lock()
	// j.versionWatcher = cb
	// j.mu.Unlock()

	j.mu.RLock()
	defer j.mu.RUnlock()
	return j.data.Version
}

// ///////////////////
// discovery helper //
// ///////////////////

// DiscoveryService returns a discovery service, ready to be announced.
func (j *JSONFile) DiscoveryService(serviceName string, port int, category discovery.Category) discovery.Service {
	return discovery.Service{
		Name:            serviceName,
		ModelName:       serviceName,
		Port:            port,
		DeviceID:        string(j.PairingID()),
		VersionWatcher:  j.VersionWatcher,
		IsPairedWatcher: j.IsPairedWatcher,
		Category:        category,
	}
}
