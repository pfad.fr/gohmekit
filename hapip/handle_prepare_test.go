package hapip_test

import (
	"encoding/json"
	"io"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
	"time"

	"code.pfad.fr/gohmekit/hapip"
	"code.pfad.fr/gohmekit/hapip/characteristic/service/accessory"
	"gotest.tools/v3/assert"
)

func TestPrepare(t *testing.T) {
	handler := hapip.Handler{
		Logger:   NewTestingLogger(t),
		Notifier: nil,
		Accessories: map[uint16]hapip.Accessory{
			1: accessory.Basic{
				Manufacturer:     "Acme",
				Model:            "Bridge1,1",
				Name:             "Acme Light Bridge",
				SerialNumber:     "037A2BABF19D",
				FirmwareRevision: "100.1.1",

				HAPProtocolVersion: "1.1.0",
			},
		},
	}
	handler.Initialize()

	handleReq := func(req *http.Request) (int, hapip.Status, error) {
		resp := httptest.NewRecorder()
		handler.ServeHTTP(resp, req)
		var status struct {
			Status hapip.Status `json:"status"`
		}
		err := json.NewDecoder(resp.Body).Decode(&status)
		return resp.Result().StatusCode, status.Status, err
	}

	httpStatus, hapStatus, err := handleReq(httptest.NewRequest("PUT", "/prepare", strings.NewReader(`{
		"ttl": 500,
		"pid":13291860906060093577
	}`)))
	assert.Equal(t, 200, httpStatus)
	assert.Equal(t, hapip.Status(0), hapStatus)
	assert.NilError(t, err)

	// missing PID
	httpStatus, hapStatus, err = handleReq(httptest.NewRequest("PUT", "/characteristics", strings.NewReader(`{
		"characteristics": []
	  }`)))
	assert.Equal(t, 400, httpStatus)
	assert.Equal(t, hapip.Status(-70410), hapStatus)
	assert.NilError(t, err)

	// wrong PID
	httpStatus, hapStatus, err = handleReq(httptest.NewRequest("PUT", "/characteristics", strings.NewReader(`{
		"characteristics": [],
		"pid": 11122333
	  }`)))
	assert.Equal(t, 400, httpStatus)
	assert.Equal(t, hapip.Status(-70410), hapStatus)
	assert.NilError(t, err)

	// correct PID, within deadline
	httpStatus, hapStatus, err = handleReq(httptest.NewRequest("PUT", "/characteristics", strings.NewReader(`{
		"characteristics": [],
		"pid": 13291860906060093577
	  }`)))
	assert.Equal(t, 204, httpStatus)
	assert.Equal(t, hapip.Status(0), hapStatus)
	assert.Check(t, err == io.EOF)

	// 1ms deadline
	httpStatus, hapStatus, err = handleReq(httptest.NewRequest("PUT", "/prepare", strings.NewReader(`{
		"ttl": 1,
		"pid":123456
	}`)))
	assert.Equal(t, 200, httpStatus)
	assert.Equal(t, hapip.Status(0), hapStatus)
	assert.NilError(t, err)

	time.Sleep(2 * time.Millisecond)
	// correct PID, too late
	httpStatus, hapStatus, err = handleReq(httptest.NewRequest("PUT", "/characteristics", strings.NewReader(`{
		"characteristics": [],
		"pid":123456
	  }`)))
	assert.Equal(t, 400, httpStatus)
	assert.Equal(t, hapip.Status(-70410), hapStatus)
	assert.NilError(t, err)
}
