package hapip

import (
	"encoding/json"
	"fmt"
	"net/http"
)

func (h Handler) postResource(rw http.ResponseWriter, req *http.Request) error {
	var resource struct {
		AID    uint16 `json:"aid"`
		Type   string `json:"resource-type"`
		Width  int    `json:"image-width"`
		Height int    `json:"image-height"`
	}
	err := json.NewDecoder(req.Body).Decode(&resource)
	if err != nil {
		rw.WriteHeader(http.StatusBadRequest)
		return err
	}
	aid := resource.AID
	if aid == 0 {
		aid = 1
	}
	acc := h.Accessories[aid]
	if acc == nil {
		rw.WriteHeader(http.StatusBadRequest)
		return fmt.Errorf("unknown accessory: %d", resource.AID)
	}
	accr, ok := acc.(AccessoryResource)
	if !ok {
		rw.WriteHeader(http.StatusBadRequest)
		return fmt.Errorf("accessory[%d] does not support Resource()", resource.AID)
	}
	h.Logger.Log("resource", resource.Type, "w", resource.Width, "h", resource.Height)
	return accr.Resource(rw, resource.Type, resource.Width, resource.Height)
}
