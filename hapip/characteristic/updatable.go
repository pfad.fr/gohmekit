package characteristic

import (
	"context"
	"sync"

	"code.pfad.fr/gohmekit/hapip"
)

var _ hapip.CharacteristicNotifier = &Updatable[string]{}

// Updatable is an updatable characteristic, which value can be updated with Update.
//
// When the updated value is different from the last value, all listeners will be notified.
type Updatable[Format format] struct {
	mu sync.Mutex
	Static[Format]
	notify func(ctx context.Context, val, old interface{})
	read   func(ctx context.Context) (Format, error)
}

// Read fullfils the hapip.CharacteristicReader interface (for use by hapip.Handler).
func (u *Updatable[Format]) Read(ctx context.Context) (interface{}, error) {
	if u.read != nil {
		v, err := u.read(ctx)
		if err != nil {
			return v, err
		}
		u.update(ctx, v)
		return v, nil
	}

	u.mu.Lock()
	defer u.mu.Unlock()
	return u.Static.value, nil
}

// Notify fullfils the hapip.CharacteristicNotifier interface (for use by hapip.Handler).
func (u *Updatable[Format]) Notify(cb func(ctx context.Context, val, old interface{})) {
	u.mu.Lock()
	defer u.mu.Unlock()
	u.notify = cb
}

// Update updates the underlying value and notifies the listeners on change.
func (u *Updatable[Format]) Update(val Format) {
	u.update(context.Background(), val)
}
func (u *Updatable[Format]) update(ctx context.Context, val Format) {
	u.mu.Lock()
	defer u.mu.Unlock()
	old := u.Static.value
	if val == old {
		return
	}

	u.Static.value = val
	cb := u.notify

	if cb != nil {
		go cb(ctx, val, old)
	}
}

// WithRemoteRead updates the characteristic (and returns itself), so that it gets its
// value from the given read function on Read.
// Must be called before any Read happens.
func (u *Updatable[Format]) WithRemoteRead(read func(ctx context.Context) (Format, error)) *Updatable[Format] {
	u.read = read
	return u
}
