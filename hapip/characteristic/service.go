package characteristic

import "code.pfad.fr/gohmekit/hapip"

type Service struct {
	Typ            string
	Characteristic []hapip.Characteristic
	Primary        bool
}

func (s Service) Type() string {
	return s.Typ
}

func (s Service) Characteristics() []hapip.Characteristic {
	return s.Characteristic
}

func (s Service) IsPrimary() bool {
	return s.Primary
}

func (s Service) AsPrimary() Service {
	s.Primary = true
	return s
}

var _ hapip.Service = Service{}

type Accessory []hapip.Service

func (a Accessory) Services() []hapip.Service {
	return a
}

var _ hapip.Accessory = Accessory{}
