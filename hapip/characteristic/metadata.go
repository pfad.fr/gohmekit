package characteristic

import (
	"fmt"

	"code.pfad.fr/gohmekit/hapip"
)

type format interface {
	bool | uint8 | uint16 | uint32 | uint64 | int32 | float64 | string | TLV8
}

var _ hapip.Characteristic = Metadata[string]{}

type Metadata[Format format] struct {
	Typ         string
	Description string

	Unit        string
	MaxLen      int
	MinValue    *Format
	MaxValue    *Format
	MinStep     Format
	ValidValues []Format
	ValidRange  []Format
	MaxDataLen  int
}

func (m Metadata[Format]) Type() string {
	return m.Typ
}
func (m Metadata[Format]) Meta() hapip.CharacteristicMeta {
	meta := hapip.CharacteristicMeta{
		Format:      m.format(),
		Description: m.Description,
		Unit:        m.Unit,
		MaxLen:      m.MaxLen,

		MaxDataLen: m.MaxDataLen,
	}
	if m.MinValue != nil {
		meta.MinValue = m.MinValue
	}
	if m.MaxValue != nil {
		meta.MaxValue = m.MaxValue
	}
	var nilF Format
	if m.MinStep != nilF {
		meta.MinStep = m.MinStep
	}
	if m.ValidValues != nil {
		meta.ValidValues = m.ValidValues
	}
	if m.ValidRange != nil {
		meta.ValidRange = m.ValidRange
	}
	return meta
}
func (m Metadata[Format]) format() string {
	var v Format
	switch any(v).(type) {
	case bool:
		return "bool"
	case uint8:
		return "uint8"
	case uint16:
		return "uint16"
	case uint32:
		return "uint32"
	case uint64:
		return "uint64"
	case int32:
		return "int"
	case float64:
		return "float"
	case string:
		return "string"
	case TLV8:
		return "tlv8"
	default:
		panic(fmt.Sprintf("unexpected type: %T", m.MinValue))
	}
}

func ValuePointer[Format format](v Format) *Format {
	return &v
}
