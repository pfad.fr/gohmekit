package characteristic

import (
	"context"
	"testing"

	"code.pfad.fr/gohmekit/tlv8"
	"gotest.tools/v3/assert"
)

func TestJSONMarshal(t *testing.T) {
	s := SupportedVideoStreamConfigurationResponse{
		Codecs: []struct {
			Type       byte                   `tlv8:"1"`
			Parameters []VideoCodecParameters `tlv8:"2"`
			Attributes []VideoAttributes      `tlv8:"3"`
		}{
			{
				Type: 0,
				Parameters: []VideoCodecParameters{
					{
						ProfileID:         0,
						Level:             0,
						PacketizationMode: 0,
					},
				},
				Attributes: []VideoAttributes{
					{1920, 1080, 30}, // 1080p
					{1280, 720, 30},  // 720p
					{640, 360, 30},
					{480, 270, 30},
					{320, 180, 30},
					{1280, 960, 30},
					{1024, 768, 30}, // XVGA
					{640, 480, 30},  // VGA
					{480, 360, 30},  // HVGA
					{320, 240, 15},  // QVGA (Apple Watch)
				},
			},
		},
	}
	c := SupportedVideoStreamConfiguration(s)
	v, err := c.Read(context.Background())
	assert.NilError(t, err)

	_, ok := v.([]byte)
	t.Log(v)
	assert.Check(t, ok)
}

func TestSupportedRTPConfigurationResponse(t *testing.T) {
	response := SupportedRTPConfigurationResponse{
		{
			CryptoSuite: 0,
			Separator:   struct{}{},
		},
		{
			CryptoSuite: 1,
			Separator:   struct{}{},
		},
	}
	buf, err := tlv8.Marshal(response)
	assert.NilError(t, err)
	assert.DeepEqual(t, []byte{
		2, 1, 0,
		0, 0,
		2, 1, 1,
		0, 0,
	}, buf)
}
