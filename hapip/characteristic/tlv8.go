package characteristic

import (
	"context"
	"encoding/json"
	"sync"

	"code.pfad.fr/gohmekit/hapip"
	"code.pfad.fr/gohmekit/tlv8"
)

// TLV8 is a pointer to []byte to be comparable.
type TLV8 *[]byte

func TLV8Value(b []byte) TLV8 {
	return &b
}

var _ hapip.CharacteristicReader = StaticTLV8[string]{}

// Static is a static characteristic. Its value can't be changed after instanciation.
//
// To make it dynamic use Static.WithRemoteRead.
type StaticTLV8[Response any] struct {
	Typ         string
	Description string

	value Response
}

func (s StaticTLV8[Response]) Type() string {
	return s.Typ
}
func (s StaticTLV8[Response]) Meta() hapip.CharacteristicMeta {
	return hapip.CharacteristicMeta{
		Format:      "tlv8",
		Description: s.Description,
	}
}

// Read fullfils the hapip.CharacteristicReader interface (for use by hapip.Handler).
func (s StaticTLV8[Response]) Read(_ context.Context) (interface{}, error) {
	return tlv8.Marshal(s.value)
}

// UpdatableTLV8 is an updatableTLV8 characteristic, which value can be updated with Update.
//
// When the updated value is different from the last value, all listeners will be notified.
type UpdatableTLV8[Response any] struct {
	mu sync.Mutex
	StaticTLV8[Response]
	notify func(ctx context.Context, val, old interface{})
}

// Read fullfils the hapip.CharacteristicReader interface (for use by hapip.Handler).
func (u *UpdatableTLV8[Response]) Read(ctx context.Context) (interface{}, error) {
	u.mu.Lock()
	defer u.mu.Unlock()
	return u.StaticTLV8.Read(ctx)
}

// Notify fullfils the hapip.CharacteristicNotifier interface (for use by hapip.Handler).
func (u *UpdatableTLV8[Response]) Notify(cb func(ctx context.Context, val, old interface{})) {
	u.mu.Lock()
	defer u.mu.Unlock()
	u.notify = cb
}

// Update updates the underlying value and notifies the listeners on change.
func (u *UpdatableTLV8[Response]) Update(val Response) {
	u.update(context.Background(), val)
}

func (u *UpdatableTLV8[Response]) update(ctx context.Context, val Response) {
	u.mu.Lock()
	defer u.mu.Unlock()
	old := u.StaticTLV8.value
	// always update the value
	// if val == old {
	// 	return
	// }

	u.StaticTLV8.value = val
	cb := u.notify

	if cb != nil {
		go cb(ctx, val, old)
	}
}

// WritableTLV8 is a writableTLV8 characteristic, which value can be updated by the remote controller or
// by the software.
//
// When the updated (or written) value is different from the last value, all listeners will be notified.
type WritableTLV8[Request any, Response any] struct {
	UpdatableTLV8[Response]
	write func(val Request) (Response, error)
}

// Write fullfils the hapip.CharacteristicWriter interface (for use by hapip.Handler).
func (u *WritableTLV8[Request, Response]) Write(ctx context.Context, s json.RawMessage) error {
	var buf []byte
	if err := json.Unmarshal(s, &buf); err != nil {
		return err
	}
	var val Request
	if err := tlv8.Unmarshal(buf, &val); err != nil {
		return err
	}
	resp, err := u.write(val)
	if err != nil {
		return err
	}
	u.update(ctx, resp)
	return nil
}

type WriteOnlyTLV8[Request any] struct {
	StaticTLV8[struct{}]
	write func(val Request) error
}

func (w WriteOnlyTLV8[Request]) Write(ctx context.Context, s json.RawMessage) error {
	var buf []byte
	if err := json.Unmarshal(s, &buf); err != nil {
		return err
	}
	var val Request
	if err := tlv8.Unmarshal(buf, &val); err != nil {
		return err
	}
	return w.write(val)
}
