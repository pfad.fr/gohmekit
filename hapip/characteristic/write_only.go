package characteristic

import (
	"context"
	"encoding/json"

	"code.pfad.fr/gohmekit/hapip"
)

var _ hapip.CharacteristicWriter = WriteOnly[string]{}

type WriteOnly[Format format] struct {
	Metadata[Format]
	write func(val Format) error
}

func (w WriteOnly[Format]) Write(ctx context.Context, buf json.RawMessage) error {
	var val Format
	if err := unmarshal(buf, &val); err != nil {
		return err
	}
	return w.write(val)
}

func unmarshal[Format format](buf json.RawMessage, v *Format) error {
	switch any(v).(type) { //nolint:gocritic
	case *bool:
		switch string(buf) {
		case "1":
			buf = []byte("true")
		case "0":
			buf = []byte("false")
		}
	}
	return json.Unmarshal(buf, v)
}
