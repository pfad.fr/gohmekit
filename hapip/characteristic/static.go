package characteristic

import (
	"context"

	"code.pfad.fr/gohmekit/hapip"
)

var _ hapip.CharacteristicReader = Static[string]{}

// Static is a static characteristic. Its value can't be changed after instanciation.
//
// To make it dynamic use Static.WithRemoteRead.
type Static[Format format] struct {
	Metadata[Format]

	value Format
}

// Read fullfils the hapip.CharacteristicReader interface (for use by hapip.Handler).
func (s Static[Format]) Read(_ context.Context) (interface{}, error) {
	return s.value, nil
}

// WithRemoteRead returns a ReadCharacterisitc which will gets its (dynamic) value from the given read function.
func (s Static[Format]) WithRemoteRead(read func() (Format, error)) StaticRemoteRead[Format] {
	return StaticRemoteRead[Format]{
		Metadata: s.Metadata,
		read:     read,
	}
}

var _ hapip.CharacteristicReader = StaticRemoteRead[string]{}

// StaticRemoteRead calls an underlying read function to get the actual value.
type StaticRemoteRead[Format format] struct {
	Metadata[Format]

	read func() (Format, error)
}

// Read fullfils the hapip.CharacteristicReader interface (for use by hapip.Handler).
func (s StaticRemoteRead[Format]) Read(_ context.Context) (interface{}, error) {
	return s.read()
}
