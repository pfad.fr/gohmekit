package characteristic

import (
	"context"
	"sync"

	"code.pfad.fr/gohmekit/hapip"
)

type programmableSwitchEventForIP struct {
	Metadata[uint8]
	mu     sync.Mutex
	notify func(ctx context.Context, val, old interface{})
}

func (u *programmableSwitchEventForIP) Read(ctx context.Context) (interface{}, error) {
	var empty interface{} // to force json omitempty give a "value": null
	return &empty, nil
}
func (u *programmableSwitchEventForIP) Notify(cb func(ctx context.Context, val, old interface{})) {
	u.mu.Lock()
	defer u.mu.Unlock()
	u.notify = cb
}
func (u *programmableSwitchEventForIP) update(val ProgrammableSwitchEvent_Type) {
	u.mu.Lock()
	cb := u.notify
	u.mu.Unlock()

	if cb != nil {
		cb(context.Background(), val, nil)
	}
}

// ProgrammableSwitchEvent represents a Programmable Switch Event characteristic (73)
// For IP accessories, the accessory must set the value of Paired Read to null(i.e. ”value”: null) in the attribute database.
// A read of this characteristic must always return a null value for IP accessories
//
// UUID: 00000073-0000-1000-8000-0026BB765291.
func ProgrammableSwitchEventForIP() (char hapip.CharacteristicNotifier, localWrite func(v ProgrammableSwitchEvent_Type)) {
	c := &programmableSwitchEventForIP{
		Metadata: ProgrammableSwitchEvent_Meta,
	}
	return c, c.update
}
