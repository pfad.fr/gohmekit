package characteristic

// // // // //
// SupportedVideoStreamConfiguration
// // // // //

type SupportedVideoStreamConfigurationResponse struct {
	Codecs []struct {
		Type       byte                   `tlv8:"1"`
		Parameters []VideoCodecParameters `tlv8:"2"`
		Attributes []VideoAttributes      `tlv8:"3"`
	} `tlv8:"1"`
}

type VideoCodecParameters struct {
	ProfileID         byte `tlv8:"1"`
	Level             byte `tlv8:"2"`
	PacketizationMode byte `tlv8:"3"`
	CvoEnabled        bool `tlv8:"4"`
	CvoID             byte `tlv8:"5"` // in range [1:14]
}
type VideoAttributes struct {
	Width     uint16 `tlv8:"1"`
	Height    uint16 `tlv8:"2"`
	Framerate byte   `tlv8:"3"`
}

// // // // //
// SupportedAudioStreamConfiguration
// // // // //

type SupportedAudioStreamConfigurationResponse struct {
	Codecs []struct {
		Type       byte                   `tlv8:"1"`
		Parameters []AudioCodecParameters `tlv8:"2"`
	} `tlv8:"1"`
	ComfortNoise bool `tlv8:"2"`
}

type AudioCodecParameters struct {
	Channels        uint8 `tlv8:"1"`
	ConstantBitrate bool  `tlv8:"2"`
	SampleRate      byte  `tlv8:"3"`
	RTPTime         uint8 `tlv8:"4"` // 20ms, 30ms, 40 ms & 60ms
}

// // // // //
// StreamingStatus
// // // // //

type StreamingStatusResponse struct {
	Status byte `tlv8:"1"`
}

// // // // //
// SelectedRTPStreamConfiguration
// // // // //

type SelectedRTPStreamConfigurationRequest struct {
	Session struct {
		Identifier []byte `tlv8:"1"`
		Command    byte   `tlv8:"2"`
	} `tlv8:"1"`
	VideoParameters struct {
		CodecType       byte                 `tlv8:"1"`
		CodecParameters VideoCodecParameters `tlv8:"2"`
		Attributes      VideoAttributes      `tlv8:"3"`
		RTP             RTPParameters        `tlv8:"4"`
	} `tlv8:"2"`
	AudioParameters struct {
		CodecType       byte                 `tlv8:"1"`
		CodecParameters AudioCodecParameters `tlv8:"2"`
		RTP             RTPParameters        `tlv8:"3"`
		ComfortNoise    bool                 `tlv8:"4"`
	} `tlv8:"3"`
}

type RTPParameters struct {
	Type             byte    `tlv8:"1"`
	SSRC             uint32  `tlv8:"2"`
	MaxBitrate       uint16  `tlv8:"3"`
	MinRTCPInterval  float32 `tlv8:"4"`
	MaxMTU           uint16  `tlv8:"5"` // only for video
	ComfortNoiseType byte    `tlv8:"6"` // only for audio
}

type SelectedRTPStreamConfigurationResponse SelectedRTPStreamConfigurationRequest

// // // // //
// SetupEndpoints
// // // // //

type SetupEndpointsRequest struct {
	SessionID      []byte  `tlv8:"1"`
	ControllerAddr Address `tlv8:"3"`
	VideoSRTP      SRTP    `tlv8:"4"`
	AudioSRTP      SRTP    `tlv8:"5"`
}

type Address struct {
	IPv6         bool   `tlv8:"1"`
	IP           string `tlv8:"2"`
	VideoRtpPort uint16 `tlv8:"3"`
	AudioRtpPort uint16 `tlv8:"4"`
}

type SRTP struct {
	// Crypto Suite
	//
	//  - 0: AES_CM_128_HMAC_SHA1_80
	//  - 1: AES_256_CM_HMAC_SHA1_80
	//  - 2: Disabled
	CryptoSuite byte   `tlv8:"1"`
	MasterKey   []byte `tlv8:"2"` // 16 (AES_CM_128) or 32 (AES_256_CM)
	MasterSalt  []byte `tlv8:"3"` // 14 byte
}

type SetupEndpointsResponse struct {
	SessionID     []byte  `tlv8:"1"`
	Status        byte    `tlv8:"2"`
	AccessoryAddr Address `tlv8:"3"` // AccessoryAddr.IPv6 must be the same as ControllerAddr.IPv6
	VideoSRTP     SRTP    `tlv8:"4"`
	AudioSRTP     SRTP    `tlv8:"5"`
	VideoSSRC     uint32  `tlv8:"6"`
	AudioSSRC     uint32  `tlv8:"7"`
}

// // // // //
// SupportedRTPConfiguration
// // // // //

type SupportedRTPConfigurationResponse []struct {
	CryptoSuite byte     `tlv8:"2"`
	Separator   struct{} `tlv8:"0"`
}

func NewSupportedRTPConfigurationResponse(cryptoSuite ...byte) SupportedRTPConfigurationResponse {
	r := make(SupportedRTPConfigurationResponse, 0, len(cryptoSuite))
	for _, c := range cryptoSuite {
		r = append(r, struct {
			CryptoSuite byte     "tlv8:\"2\""
			Separator   struct{} "tlv8:\"0\""
		}{
			CryptoSuite: c,
		})
	}
	return r
}

type LogsResponse struct{}                                  // TODO
type SupportedCameraRecordingConfigurationResponse struct{} // TODO
type SupportedVideoRecordingConfigurationResponse struct{}  // TODO
type SupportedAudioRecordingConfigurationResponse struct{}  // TODO

type DisplayOrderRequest struct{}                          // TODO
type DisplayOrderResponse struct{}                         // TODO
type PairSetupRequest struct{}                             // TODO
type PairSetupResponse struct{}                            // TODO
type PairVerifyResponse struct{}                           // TODO
type PairVerifyRequest struct{}                            // TODO
type PairingPairingsRequest struct{}                       // TODO
type PairingPairingsResponse struct{}                      // TODO
type SelectedCameraRecordingConfigurationResponse struct{} // TODO
type SelectedCameraRecordingConfigurationRequest struct{}  // TODO

type LockControlPointRequest struct{} // TODO
