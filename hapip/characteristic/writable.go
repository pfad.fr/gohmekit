package characteristic

import (
	"context"
	"encoding/json"

	"code.pfad.fr/gohmekit/hapip"
)

var _ hapip.CharacteristicNotifier = &Writable[string]{}
var _ hapip.CharacteristicWriter = &Writable[string]{}

// Writable is a writable characteristic, which value can be updated by the remote controller or
// by the software.
//
// When the updated (or written) value is different from the last value, all listeners will be notified.
type Writable[Format format] struct {
	Updatable[Format]
	write func(val Format) error
}

// Write fullfils the hapip.CharacteristicWriter interface (for use by hapip.Handler).
func (u *Writable[Format]) Write(ctx context.Context, buf json.RawMessage) error {
	var val Format
	if err := unmarshal(buf, &val); err != nil {
		return err
	}
	err := u.write(val)
	if err != nil {
		return err
	}
	u.update(ctx, val)
	return nil
}

// WithRemoteRead updates the characteristic (and returns itself), so that it gets its
// value from the given read function on Read.
// Must be called before any Read happens.
func (u *Writable[Format]) WithRemoteRead(read func(ctx context.Context) (Format, error)) *Writable[Format] {
	u.read = read
	return u
}
