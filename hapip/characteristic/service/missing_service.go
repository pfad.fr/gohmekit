package service

import (
	"code.pfad.fr/gohmekit/hapip"
	"code.pfad.fr/gohmekit/hapip/characteristic"
)

const HAPProtocolInformation_Type = "A2"

// HAPProtocolInformation represents a HAP Protocol Information (A2)
// Required characteristics:
//   - version: Version (37)
//
// UUID: 000000A2-0000-1000-8000-0026BB765291.
func HAPProtocolInformation(version hapip.Characteristic, optional ...hapip.Characteristic) characteristic.Service {
	chars := []hapip.Characteristic{version}
	return characteristic.Service{
		Typ:            HAPProtocolInformation_Type,
		Characteristic: append(chars, optional...),
	}
}
