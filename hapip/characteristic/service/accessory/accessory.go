package accessory

import (
	"net/http"

	"code.pfad.fr/gohmekit/hapip"
	"code.pfad.fr/gohmekit/hapip/characteristic"
	"code.pfad.fr/gohmekit/hapip/characteristic/service"
)

// Basic implements a basic accessory.
type Basic struct {
	// required
	Identify func(bool) error
	// required
	Manufacturer, Model, Name, SerialNumber, FirmwareRevision string

	// optional
	HardwareRevision string
	AccessoryFlags   *characteristic.Updatable[uint32]

	// HAPProtocolVersion will be added as "HAP Protocol Information" service to the services if not empty
	HAPProtocolVersion string

	// Additional services provided by this accessory
	AdditionalServices []hapip.Service
}

func (i Basic) Services() []hapip.Service {
	s := []hapip.Service{i.Service()}
	if i.HAPProtocolVersion != "" {
		s = append(s, service.HAPProtocolInformation(characteristic.Version(i.HAPProtocolVersion)))
	}
	s = append(s, i.AdditionalServices...)
	return s
}

func (i Basic) Service() characteristic.Service {
	var optional []hapip.Characteristic
	if i.HardwareRevision != "" {
		optional = append(optional, characteristic.HardwareRevision(i.HardwareRevision))
	}
	if i.AccessoryFlags != nil {
		optional = append(optional, i.AccessoryFlags)
	}
	return service.AccessoryInformation(
		characteristic.Identify(i.Identify),
		characteristic.Manufacturer(notEmpty(i.Manufacturer, "-")),
		characteristic.Model(notEmpty(i.Model, "-")),
		characteristic.Name(notEmpty(i.Name, "-")),
		characteristic.SerialNumber(notEmpty(i.SerialNumber, "-")),
		characteristic.FirmwareRevision(notEmpty(i.FirmwareRevision, "-")),
		optional...,
	)
}

func notEmpty(s, fallback string) string { //nolint:unparam
	if s == "" {
		return fallback
	}
	return s
}

func (i Basic) WithResourceHandler(handler func(rw http.ResponseWriter, typ string, width, height int) error) WithResource {
	return WithResource{
		Basic:           i,
		ResourceHandler: handler,
	}
}

// WithResource implements an accessory with a /resource handler.
type WithResource struct {
	Basic

	ResourceHandler func(rw http.ResponseWriter, typ string, width, height int) error
}

func (i WithResource) Resource(rw http.ResponseWriter, typ string, width, height int) error {
	return i.ResourceHandler(rw, typ, width, height)
}
