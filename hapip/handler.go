package hapip

import (
	"context"
	"encoding/json"
	"errors"
	"io"
	"net/http"
	"strconv"
	"strings"
	"time"

	"code.pfad.fr/gohmekit/hapip/notification"
	"github.com/go-kit/log"
)

// Handler is a http.Handler to handle /accessories and /characterisitc request.
// It fields must not be changed after calling .Initialize().
type Handler struct {
	Logger   log.Logger
	Notifier Notifier

	Accessories map[uint16]Accessory

	// indexed by aid, iid
	services        map[uint16]map[uint32]Service
	characteristics map[CharacteristicID]Characteristic

	// mapping AID => ServiceID => CharacteristicIIDs
	serviceCharacteristics map[uint16]map[uint32][]uint32

	fallbackHandler http.Handler

	tw *timedWrite
}

type Accessory interface {
	Services() []Service
}

type AccessoryResource interface {
	Accessory
	Resource(rw http.ResponseWriter, typ string, width, height int) error
}

type Service interface {
	Type() string
	Characteristics() []Characteristic
	IsPrimary() bool
}
type Characteristic interface {
	Type() string
	Meta() CharacteristicMeta
}

type CharacteristicReader interface {
	Characteristic
	Read(context.Context) (interface{}, error)
}
type CharacteristicWriter interface {
	Characteristic
	Write(context.Context, json.RawMessage) error
}
type CharacteristicNotifier interface {
	Characteristic
	Notify(func(ctx context.Context, val, old interface{}))
}

type CharacteristicMeta struct {
	Format string

	// Optional
	Description string

	Unit        string
	MaxLen      int
	MinValue    interface{}
	MaxValue    interface{}
	MinStep     interface{}
	ValidValues interface{}
	ValidRange  interface{}
	MaxDataLen  int

	/*
		// TTL
		// pid
	*/
}

type characteristicData struct {
	Status *Status `json:"status,omitempty"`

	AID uint16 `json:"aid,omitempty"`
	IID uint32 `json:"iid"`

	Value interface{} `json:"value,omitempty"`

	// META
	Format      string      `json:"format,omitempty"`
	Description string      `json:"description,omitempty"` // manufacturer description (optional)
	Unit        string      `json:"unit,omitempty"`
	MaxLen      int         `json:"maxLen,omitempty"`
	MinValue    interface{} `json:"minValue,omitempty"`
	MaxValue    interface{} `json:"maxValue,omitempty"`
	MinStep     interface{} `json:"minStep,omitempty"`
	ValidValues interface{} `json:"valid-values,omitempty"`
	ValidRange  interface{} `json:"valid-values-range,omitempty"`
	MaxDataLen  int         `json:"maxDataLen,omitempty"`

	// Perms
	Permissions []string `json:"perms,omitempty"`

	// Type
	Type string `json:"type,omitempty"`

	// ev (is subscribed)
	Ev *bool `json:"ev,omitempty"`
}

func parseCharacteristicID(cid string) (CharacteristicID, error) {
	a, i, ok := strings.Cut(cid, ".")
	if !ok {
		return CharacteristicID{}, errors.New("no . in id")
	}
	var c CharacteristicID
	var err error
	v, err := strconv.ParseUint(a, 10, 16)
	c.AID = uint16(v)
	if err != nil {
		return c, err
	}
	v, err = strconv.ParseUint(i, 10, 32)
	c.IID = uint32(v)
	if err != nil {
		return c, err
	}
	return c, nil
}

type CharacteristicID struct {
	AID uint16
	IID uint32
}

func (cid CharacteristicID) String() string {
	return strconv.FormatUint(uint64(cid.AID), 10) + "." + strconv.FormatUint(uint64(cid.IID), 10)
}

// PrepareServer will create a notification.Manager, hook it to the connection events
// and wrap the server.Handler with an initialized hapip.Handler.
func PrepareServer(server *http.Server, acc map[uint16]Accessory, options ...option) *http.Server {
	logger := log.NewNopLogger()
	notifier := &notification.Manager[CharacteristicID]{
		CoalesceDuration: time.Second,
		Logger:           logger,
	}
	notifier.HookConnEvents(server)

	h := Handler{
		Logger:                 logger,
		Notifier:               notifier,
		Accessories:            acc,
		services:               make(map[uint16]map[uint32]Service),
		characteristics:        make(map[CharacteristicID]Characteristic),
		serviceCharacteristics: make(map[uint16]map[uint32][]uint32),
		fallbackHandler:        server.Handler,
	}
	for _, o := range options {
		o(&h)
	}
	h.Initialize()
	server.Handler = h
	return server
}

type option func(*Handler)

// WithLogger adds structured logging to the pairing server.
func WithLogger(logger log.Logger) option {
	return func(h *Handler) {
		h.Logger = logger
	}
}

func (h Handler) ServeHTTP(rw http.ResponseWriter, req *http.Request) {
	var err error
	switch req.URL.Path {
	case "/accessories":
		switch req.Method {
		case http.MethodGet:
			err = h.getAccessories(rw, req)
		default:
			h.Logger.Log(req.Method, req.URL.Path, "err", "unsupported method")
			err = WriteStatus(rw, http.StatusMethodNotAllowed, StatusInvalidValueInRequest)
		}
	case "/characteristics":
		switch req.Method {
		case http.MethodGet:
			err = h.getCharacteristics(rw, req)
		case http.MethodPut:
			err = h.putCharacteristics(rw, req)
		default:
			h.Logger.Log(req.Method, req.URL.Path, "err", "unsupported method")
			err = WriteStatus(rw, http.StatusMethodNotAllowed, StatusInvalidValueInRequest)
		}
	case "/resource":
		switch req.Method {
		case http.MethodPost:
			err = h.postResource(rw, req)
		default:
			h.Logger.Log(req.Method, req.URL.Path, "err", "unsupported method")
			err = WriteStatus(rw, http.StatusMethodNotAllowed, StatusInvalidValueInRequest)
		}
	case "/prepare":
		switch req.Method {
		case http.MethodPut:
			err = h.putPrepare(rw, req)
		default:
			h.Logger.Log(req.Method, req.URL.Path, "err", "unsupported method")
			err = WriteStatus(rw, http.StatusMethodNotAllowed, StatusInvalidValueInRequest)
		}
	default:
		if h.fallbackHandler != nil {
			h.fallbackHandler.ServeHTTP(rw, req)
		} else {
			h.Logger.Log(req.Method, req.URL.Path, "err", "not found")
			err = WriteStatus(rw, http.StatusNotFound, StatusInvalidValueInRequest)
		}
	}
	if err != nil {
		h.Logger.Log(req.Method, req.URL.Path, "err", err)
	}
}

func (h *Handler) Initialize() {
	h.services = make(map[uint16]map[uint32]Service)
	h.characteristics = make(map[CharacteristicID]Characteristic)
	h.serviceCharacteristics = make(map[uint16]map[uint32][]uint32)

	h.tw = &timedWrite{}

	for aid, a := range h.Accessories {
		if a == nil {
			continue
		}
		iid := uint32(1)

		services := a.Services()
		h.services[aid] = make(map[uint32]Service, len(services))
		h.serviceCharacteristics[aid] = make(map[uint32][]uint32)

		for _, s := range services {
			h.services[aid][iid] = s
			siid := iid
			iid++

			for _, c := range s.Characteristics() {
				cid := CharacteristicID{aid, iid}
				h.characteristics[cid] = c
				h.serviceCharacteristics[aid][siid] = append(h.serviceCharacteristics[aid][siid], iid)
				iid++

				if no, ok := c.(CharacteristicNotifier); ok {
					eventCanWait := true
					typ := c.Type()
					if typ == "126" || // • ”9.12 Button Event” (page 163)
						typ == "73" { // • ”9.75 Programmable Switch Event” (page 194)
						eventCanWait = false
					}
					no.Notify(func(ctx context.Context, val, old interface{}) {
						ced := characteristicEventData{
							Aid:          cid.AID,
							Iid:          cid.IID,
							Value:        val,
							initialValue: old,
						}
						var ev io.WriterTo = ced
						if eventCanWait {
							ev = coalescableCharacteristicEventData{ced}
						}

						h.Notifier.Publish(ctx, cid, ev)
					})
				}
			}
		}
	}
}

type Notifier interface {
	Subscribe(context.Context, CharacteristicID, bool) error
	IsSubscribed(context.Context, CharacteristicID) (bool, error)
	Publish(context.Context, CharacteristicID, io.WriterTo)
}
