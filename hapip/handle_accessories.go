package hapip

import (
	"context"
	"fmt"
	"net/http"
	"sync"
	"time"
)

func (h Handler) getAccessories(rw http.ResponseWriter, req *http.Request) error {
	type service struct {
		// missing service properties: hidden, linked
		IID             uint32                `json:"iid"`
		Type            string                `json:"type"`
		Characteristics []*characteristicData `json:"characteristics"`
		Primary         bool                  `json:"primary,omitempty"`
	}
	type accessory struct {
		AID      uint16    `json:"aid"`
		Services []service `json:"services"`
	}
	data := struct {
		Accessories []accessory `json:"accessories"`
	}{
		Accessories: make([]accessory, 0, len(h.Accessories)),
	}

	var wg sync.WaitGroup
	ctx, cancel := context.WithTimeout(req.Context(), time.Second) // 3s is too long
	defer cancel()

	for aid := range h.Accessories {
		svc := h.services[aid]
		if len(svc) == 0 {
			continue
		}
		ar := accessory{
			AID:      aid,
			Services: make([]service, 0, len(svc)),
		}

		for siid, s := range svc {
			chIIDs := h.serviceCharacteristics[aid][siid]
			svc := service{
				Type:            s.Type(),
				IID:             siid,
				Characteristics: make([]*characteristicData, 0, len(chIIDs)),
				Primary:         s.IsPrimary(),
			}

			for _, iid := range chIIDs {
				c := h.characteristics[CharacteristicID{aid, iid}]
				cj := &characteristicData{
					IID: iid,
				}
				cj.FillMeta(c.Meta(), true)
				cj.FillPerms(c)
				cj.FillType(c.Type())
				// id := CharacteristicID{aid, iid}
				// if ev, err := h.Notifier.IsSubscribed(ctx, id); err == nil {
				// 	cj.FillEv(ev)
				// }
				if pr, ok := c.(CharacteristicReader); ok {
					wg.Add(1)
					go func() {
						defer wg.Done()
						cj.FillValue(pr.Read(ctx))
					}()
				}

				svc.Characteristics = append(svc.Characteristics, cj)
			}
			ar.Services = append(ar.Services, svc)
		}
		data.Accessories = append(data.Accessories, ar)
	}

	wg.Wait()

	return WriteJSON(rw, http.StatusOK, data)
}

func (cj *characteristicData) FillMeta(meta CharacteristicMeta, extended bool) {
	cj.Format = meta.Format
	cj.Unit = meta.Unit
	cj.MinValue = meta.MinValue
	cj.MaxValue = meta.MaxValue
	cj.MinStep = meta.MinStep
	cj.MaxLen = meta.MaxLen

	if extended {
		cj.Description = meta.Description
		cj.ValidValues = meta.ValidValues
		cj.ValidRange = meta.ValidRange
		cj.MaxDataLen = meta.MaxDataLen
	}
}

func (cj *characteristicData) FillPerms(c Characteristic) {
	perms := make([]string, 0)
	if _, ok := c.(CharacteristicReader); ok {
		perms = append(perms, "pr")
	}
	if _, ok := c.(CharacteristicWriter); ok {
		perms = append(perms, "pw")
	}
	if _, ok := c.(CharacteristicNotifier); ok {
		perms = append(perms, "ev")
	}
	cj.Permissions = perms
}

func (cj *characteristicData) FillType(t string) {
	cj.Type = t
}
func (cj *characteristicData) FillEv(ev bool) {
	cj.Ev = &ev
}

func (cj *characteristicData) FillStatus(s Status) {
	cj.Status = &s
}

// FillValue may call FillStatus. Beware of race conditions!
func (cj *characteristicData) FillValue(v interface{}, err error) {
	cj.Value = v
	if err != nil {
		fmt.Println("err", err)
		cj.FillStatus(StatusServiceCommunicationFailure)
	}
}
