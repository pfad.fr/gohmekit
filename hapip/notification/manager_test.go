package notification

import (
	"bufio"
	"bytes"
	"errors"
	"io"
	"net"
	"net/http"
	"os"
	"testing"
	"time"

	"github.com/go-kit/log"
	"gotest.tools/v3/assert"
)

func TestManager(t *testing.T) {
	m := Manager[string]{
		Logger: log.NewNopLogger(),
	}

	ch := make(chan string)
	s := http.Server{
		Handler: http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			path := r.URL.Path
			t.Log(path)
			switch path {
			case "/subscribe":
				err := m.Subscribe(r.Context(), "123", true)
				assert.NilError(t, err)
			case "/unsubscribe":
				err := m.Subscribe(r.Context(), "123", false)
				assert.NilError(t, err)
			case "/publish":
				m.Publish(r.Context(), "123", bytes.NewReader([]byte("456")))
			case "/wait":
				ch <- "waiting"
				assert.Equal(t, "published", <-ch)
			case "/is-listening":
				v, err := m.IsSubscribed(r.Context(), "123")
				assert.NilError(t, err)
				assert.Check(t, v)
			default:
				t.Fatalf("unexpected uri: %s", path)
			}
			_, _ = w.Write([]byte("ok"))
		}),
		ConnState:   m.ConnState,
		ConnContext: m.ConnContext,
	}

	ln, err := net.Listen("tcp", ":0")
	assert.NilError(t, err)
	t.Cleanup(func() {
		ln.Close()
	})

	go s.Serve(ln) //nolint:errcheck

	addr := ln.Addr().String()
	connA, err := net.Dial("tcp", addr)
	assert.NilError(t, err)

	connB, err := net.Dial("tcp", addr)
	assert.NilError(t, err)

	body := getRequest(t, connB, "/publish") // no one is listening yet
	assert.Equal(t, "ok", body)

	body = getRequest(t, connA, "/subscribe")
	assert.Equal(t, "ok", body)

	body = getRequest(t, connB, "/subscribe") // listen to self should not send notifications
	assert.Equal(t, "ok", body)

	time.Sleep(100 * time.Millisecond) // hack to wait for connA to be idle

	body = getRequest(t, connB, "/publish")
	assert.Equal(t, "ok", body)

	buf := make([]byte, 3)
	n, err := connA.Read(buf)
	assert.NilError(t, err)
	assert.Equal(t, 3, n)
	assert.Equal(t, "456", string(buf))

	body = getRequest(t, connA, "/is-listening")
	assert.Equal(t, "ok", body)

	go func() {
		bodyWaited := getRequest(t, connA, "/wait")
		assert.Equal(t, "ok", bodyWaited)
		ch <- "waited"
	}()

	assert.Equal(t, "waiting", <-ch)
	body = getRequest(t, connB, "/publish")
	assert.Equal(t, "ok", body)
	ch <- "published"

	assert.Equal(t, "waited", <-ch)

	body = getRequest(t, connA, "/unsubscribe")
	assert.Equal(t, "ok", body)

	time.Sleep(100 * time.Millisecond) // hack to wait for connA to be idle

	body = getRequest(t, connB, "/publish") // no one is listening anymore
	assert.Equal(t, "ok", body)

	go func() {
		time.Sleep(100 * time.Millisecond) // hack to wait for event to be sent
		ch <- "waited"

		buf, _ = io.ReadAll(connA)
		ch <- string(buf)
	}()

	assert.Equal(t, "waited", <-ch)
	err = connA.Close()
	assert.NilError(t, err)
	assert.Equal(t, "", <-ch)

	err = connB.Close()
	assert.NilError(t, err)
}

func getRequest(t *testing.T, c net.Conn, uri string) string {
	_, err := c.Write([]byte(`GET ` + uri + ` HTTP/1.1
Host: lights.local:12345

`))
	assert.NilError(t, err)

	buf := bufio.NewReader(c)
	resp, err := http.ReadResponse(buf, nil)
	assert.NilError(t, err)

	defer resp.Body.Close()
	all, err := io.ReadAll(resp.Body)
	assert.NilError(t, err)
	return string(all)
}

type colaescerTest []byte

func (ct colaescerTest) WriteTo(w io.Writer) (int64, error) {
	n, err := w.Write(append(ct, '\n'))
	return int64(n), err
}
func (ct colaescerTest) Coalesce(ev io.WriterTo) (Coalescer, error) {
	switch last := ev.(type) {
	case colaescerTest:
		ct = append(ct, last...)
	case *bytes.Buffer:
		ct = append(ct, last.Bytes()...)
	default:
		panic("What the type?")
	}
	return ct, nil
}

var _ Coalescer = colaescerTest{}

func TestCoalescer(t *testing.T) {
	m := Manager[string]{
		CoalesceDuration: 100 * time.Millisecond,
		Logger:           log.NewNopLogger(),
	}

	s := http.Server{
		Handler: http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			path := r.URL.Path
			t.Log(path)
			switch path {
			case "/subscribe":
				err := m.Subscribe(r.Context(), "123", true)
				assert.NilError(t, err)
			case "/publish-colaescer":
				var ct colaescerTest = []byte("ct")
				m.Publish(r.Context(), "123", ct)
			case "/publish-immediate":
				m.Publish(r.Context(), "123", bytes.NewBuffer([]byte("im")))
			default:
				t.Fatalf("unexpected uri: %s", path)
			}
			_, _ = w.Write([]byte("ok"))
		}),
		ConnState:   m.ConnState,
		ConnContext: m.ConnContext,
	}

	ln, err := net.Listen("tcp", ":0")
	assert.NilError(t, err)
	t.Cleanup(func() {
		ln.Close()
	})

	go s.Serve(ln) //nolint:errcheck

	addr := ln.Addr().String()
	connA, err := net.Dial("tcp", addr)
	assert.NilError(t, err)

	connB, err := net.Dial("tcp", addr)
	assert.NilError(t, err)

	body := getRequest(t, connA, "/subscribe")
	assert.Equal(t, "ok", body)

	time.Sleep(100 * time.Millisecond) // hack to wait for connA to be idle

	// this one will go through immediately
	body = getRequest(t, connB, "/publish-colaescer")
	assert.Equal(t, "ok", body)

	// should get throttled...
	body = getRequest(t, connB, "/publish-colaescer")
	assert.Equal(t, "ok", body)

	// ...and merged
	body = getRequest(t, connB, "/publish-colaescer")
	assert.Equal(t, "ok", body)

	err = connA.SetDeadline(time.Now().Add(110 * time.Millisecond))
	assert.NilError(t, err)

	buf, err := io.ReadAll(connA)
	assert.Check(t, errors.Is(err, os.ErrDeadlineExceeded))
	assert.Equal(t, "ct\nctct\n", string(buf))

	// should get throttled...
	body = getRequest(t, connB, "/publish-colaescer")
	assert.Equal(t, "ok", body)

	// ...and send immediately
	body = getRequest(t, connB, "/publish-immediate")
	assert.Equal(t, "ok", body)

	err = connA.SetDeadline(time.Now().Add(10 * time.Millisecond))
	assert.NilError(t, err)

	buf, err = io.ReadAll(connA)
	assert.Check(t, errors.Is(err, os.ErrDeadlineExceeded))
	assert.Equal(t, "ctim\n", string(buf))

	err = connA.Close()
	assert.NilError(t, err)

	err = connB.Close()
	assert.NilError(t, err)
}
