package hapip

import (
	"encoding/json"
	"net/http"
)

type Status int

// taken from github.com/brutella/hc@v1.2.5/hap/constants.go

const (
	StatusSuccess                     = Status(0)
	StatusInsufficientPrivileges      = Status(-70401)
	StatusServiceCommunicationFailure = Status(-70402)
	StatusResourceBusy                = Status(-70403)
	StatusReadOnlyCharacteristic      = Status(-70404)
	StatusWriteOnlyCharacteristic     = Status(-70405)
	StatusNotificationNotSupported    = Status(-70406)
	StatusOutOfResource               = Status(-70407)
	StatusOperationTimedOut           = Status(-70408)
	StatusResourceDoesNotExist        = Status(-70409)
	StatusInvalidValueInRequest       = Status(-70410)
)

func WriteStatus(rw http.ResponseWriter, httpCode int, hapCode Status) error {
	return WriteJSON(rw, httpCode, struct {
		Status Status `json:"status"`
	}{
		Status: hapCode,
	})
}

func WriteJSON(rw http.ResponseWriter, httpCode int, data interface{}) error {
	rw.Header().Set("Content-Type", "application/hap+json")
	rw.WriteHeader(httpCode)

	return json.NewEncoder(rw).Encode(data)
}
