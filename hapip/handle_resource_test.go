package hapip_test

import (
	"image"
	"image/jpeg"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"code.pfad.fr/gohmekit/hapip"
	"code.pfad.fr/gohmekit/hapip/characteristic/service/accessory"
	"gotest.tools/v3/assert"
)

func TestResource(t *testing.T) {
	resourceCalled := 0
	handler := hapip.Handler{
		Logger:   NewTestingLogger(t),
		Notifier: nil,
		Accessories: map[uint16]hapip.Accessory{
			1: accessory.Basic{
				Manufacturer:     "Acme",
				Model:            "Bridge1,1",
				Name:             "Acme Light Bridge",
				SerialNumber:     "037A2BABF19D",
				FirmwareRevision: "100.1.1",

				HAPProtocolVersion: "1.1.0",
			}.WithResourceHandler(func(rw http.ResponseWriter, typ string, width, height int) error {
				assert.Equal(t, "image", typ)
				assert.Equal(t, 1024, width)
				assert.Equal(t, 42, height)

				resourceCalled++

				rw.Header().Set("Content-Type", "image/jpeg")
				// generate blank image
				img := image.NewRGBA(image.Rect(0, 0, width, height))
				return jpeg.Encode(rw, img, nil)
			}),
		},
	}

	req := httptest.NewRequest("POST", "/resource", strings.NewReader(`{
		"aid": 1,
		"resource-type": "image",
		"image-width": 1024,
		"image-height": 42
	}`))
	resp := httptest.NewRecorder()
	handler.ServeHTTP(resp, req)

	assert.Equal(t, 200, resp.Code)
	assert.Equal(t, "image/jpeg", resp.Header().Get("Content-Type"))
	assert.Equal(t, 1, resourceCalled)

	// f, err := os.OpenFile("rgb.jpg", os.O_WRONLY|os.O_CREATE, 0600)
	// assert.NilError(t, err)
	// defer f.Close()
	// _, err = io.Copy(f, resp.Body)
	// assert.NilError(t, err)
}
