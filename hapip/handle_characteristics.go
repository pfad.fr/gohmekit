package hapip

import (
	"context"
	"encoding/json"
	"net/http"
	"strings"
	"sync"
	"time"
)

func (h Handler) getCharacteristics(rw http.ResponseWriter, req *http.Request) error {
	// id=1.4,1.5
	v := req.FormValue("id")
	if len(v) == 0 {
		return WriteStatus(rw, http.StatusBadRequest, StatusInvalidValueInRequest)
	}

	withMeta := req.FormValue("meta") == "1"
	withPerms := req.FormValue("perms") == "1"
	withType := req.FormValue("type") == "1"
	withEv := req.FormValue("ev") == "1"

	cids := strings.Split(v, ",")

	data := struct {
		Characteristics []*characteristicData `json:"characteristics"`
	}{
		Characteristics: make([]*characteristicData, 0, len(cids)),
	}

	var wg sync.WaitGroup
	ctx, cancel := context.WithTimeout(req.Context(), time.Second) // 3s is too long
	defer cancel()

	for _, sid := range cids {
		cid, err := parseCharacteristicID(sid)
		if err != nil {
			continue
		}
		cdata := &characteristicData{
			AID: cid.AID,
			IID: cid.IID,
		}
		data.Characteristics = append(data.Characteristics, cdata)

		c := h.characteristics[cid]
		if c == nil {
			cdata.FillStatus(StatusInvalidValueInRequest)
			continue
		}

		if withMeta {
			cdata.FillMeta(c.Meta(), false)
		}

		// Should the response include the events flag?
		if withEv {
			ev, err := h.Notifier.IsSubscribed(ctx, cid)

			if err != nil {
				h.Logger.Log("cid", cid, "notifier", "IsSubscribed", "err", err)
				cdata.FillStatus(StatusServiceCommunicationFailure)
			} else {
				cdata.FillEv(ev)
			}
		}

		if withPerms {
			cdata.FillPerms(c)
		}

		if withType {
			cdata.FillType(c.Type())
		}

		// last one to prevent race condition on FillStatus
		if v, ok := c.(CharacteristicReader); ok {
			wg.Add(1)
			go func() {
				defer wg.Done()
				cdata.FillValue(v.Read(ctx))
			}()
		}
	}
	wg.Wait()

	hasErr := false
	for _, c := range data.Characteristics {
		if c.Status != nil && *c.Status != StatusSuccess {
			hasErr = true
			break
		}
	}

	httpCode := http.StatusOK
	if hasErr {
		// 6.7.4.2 Reading Multiple Characteristics
		// Characteristics that were read successfully must have a ”status” of 0 and ”value”.
		for _, c := range data.Characteristics {
			if c.Status == nil {
				c.FillStatus(StatusSuccess)
			}
		}
		httpCode = http.StatusMultiStatus
	}
	return WriteJSON(rw, httpCode, data)
}

func (h Handler) putCharacteristics(rw http.ResponseWriter, req *http.Request) error {
	type characteristicRequest struct {
		AID uint16 `json:"aid"`
		IID uint32 `json:"iid"`

		Value  json.RawMessage `json:"value,omitempty"`
		Events *bool           `json:"ev,omitempty"`
		// AuthorizationData   string       `json:"authData,omitempty"` // not supported for now
		// Remote   *bool       `json:"remote,omitempty"` // not supported for now
		Response bool `json:"r,omitempty"`
	}

	var request struct {
		Characteristics []characteristicRequest `json:"characteristics"`
		PID             *uint64                 `json:"pid,omitempty"`
	}
	err := json.NewDecoder(req.Body).Decode(&request)
	if err != nil {
		return err
	}

	if !h.tw.isValid(request.PID) {
		return WriteStatus(rw, http.StatusBadRequest, StatusInvalidValueInRequest)
	}

	data := struct {
		Characteristics []*characteristicData `json:"characteristics"`
	}{
		Characteristics: make([]*characteristicData, 0, len(request.Characteristics)),
	}

	var wg sync.WaitGroup
	statusSuccess := StatusSuccess
	for _, cr := range request.Characteristics {
		cid := CharacteristicID{cr.AID, cr.IID}
		cdata := &characteristicData{
			AID:    cid.AID,
			IID:    cid.IID,
			Status: &statusSuccess, // a status is always needed on JSON reply (http != 204)
		}
		data.Characteristics = append(data.Characteristics, cdata)

		c := h.characteristics[cid]
		if c == nil {
			cdata.FillStatus(StatusInvalidValueInRequest)
			continue
		}

		if cr.Events != nil {
			err := h.Notifier.Subscribe(req.Context(), cid, *cr.Events)
			if err != nil {
				cdata.FillStatus(StatusServiceCommunicationFailure)
				return err
			}
		}

		// last one to prevent race condition on FillStatus
		if cr.Value != nil { //nolint:nestif
			if wr, ok := c.(CharacteristicWriter); ok {
				value := cr.Value
				withResponse := cr.Response

				wg.Add(1)
				go func() {
					defer wg.Done()
					err := wr.Write(req.Context(), value)
					if err != nil {
						h.Logger.Log("cid", cid, "action", "write", "err", err)
						cdata.FillStatus(StatusServiceCommunicationFailure)
					}
					if withResponse {
						if rd, ok := c.(CharacteristicReader); ok {
							cdata.FillValue(rd.Read(req.Context()))
						} else {
							h.Logger.Log("cid", cid, "action", "write-read", "err", "unreadable characteristic")
						}
					}
				}()
			} else {
				h.Logger.Log("cid", cid, "action", "write", "err", "unwrittable characteristic")
				cdata.FillStatus(StatusServiceCommunicationFailure)
			}
		}
	}
	wg.Wait()

	hasErr := false
	for _, c := range data.Characteristics {
		if c.Status != nil && *c.Status != StatusSuccess {
			hasErr = true
			break
		}
	}

	if !hasErr {
		rw.WriteHeader(http.StatusNoContent)
		return nil
	}

	return WriteJSON(rw, http.StatusMultiStatus, data)
}
