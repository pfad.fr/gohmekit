package hapip_test

import (
	"bytes"
	"context"
	"encoding/base64"
	"encoding/json"
	"errors"
	"io"
	"net/http"
	"net/http/httptest"
	"sort"
	"strings"
	"sync"
	"testing"
	"time"

	"code.pfad.fr/gohmekit/hapip"
	"code.pfad.fr/gohmekit/hapip/characteristic"
	"code.pfad.fr/gohmekit/hapip/characteristic/service"
	"code.pfad.fr/gohmekit/hapip/characteristic/service/accessory"
	"code.pfad.fr/gohmekit/tlv8"
	"github.com/go-kit/log"
	"gotest.tools/v3/assert"
)

var _ hapip.Notifier = &testNotifier{}

type testNotifier struct {
	subscribed map[hapip.CharacteristicID]bool

	mu        sync.Mutex
	published []struct {
		cid hapip.CharacteristicID
		buf []byte
	}
	err error
}

func (n *testNotifier) Subscribe(ctx context.Context, cid hapip.CharacteristicID, v bool) error {
	n.subscribed[cid] = v
	return n.err
}
func (n *testNotifier) IsSubscribed(ctx context.Context, cid hapip.CharacteristicID) (bool, error) {
	return n.subscribed[cid], n.err
}
func (n *testNotifier) Publish(ctx context.Context, cid hapip.CharacteristicID, ev io.WriterTo) {
	n.mu.Lock()
	defer n.mu.Unlock()
	buf := &bytes.Buffer{}
	_, err := ev.WriteTo(buf)
	if err != nil {
		panic(err)
	}
	n.published = append(n.published, struct {
		cid hapip.CharacteristicID
		buf []byte
	}{
		cid: cid,
		buf: buf.Bytes(),
	})
}

func newHandler(identify func(bool) error, on func(bool) error, brightness func(int32) error, setupEntpoints func(ser characteristic.SetupEndpointsRequest) (characteristic.SetupEndpointsResponse, error)) (http.Handler, *testNotifier) {
	n := &testNotifier{
		subscribed: make(map[hapip.CharacteristicID]bool),
	}

	handler := hapip.Handler{
		Logger:   log.NewNopLogger(),
		Notifier: n,
		Accessories: map[uint16]hapip.Accessory{
			1: accessory.Basic{
				Identify:         identify,
				Manufacturer:     "Acme",
				Model:            "Bridge1,1",
				Name:             "Acme Light Bridge",
				SerialNumber:     "037A2BABF19D",
				FirmwareRevision: "100.1.1",

				HAPProtocolVersion: "1.1.0",
			},
			2: accessory.Basic{
				Identify:         nil,
				Manufacturer:     "Acme",
				Model:            "LEDBulb1,1",
				Name:             "Acme LED Light Bulb",
				SerialNumber:     "099DB48E9E28",
				FirmwareRevision: "1",

				AdditionalServices: []hapip.Service{
					service.Lightbulb(
						characteristic.On(false, on),
						characteristic.Brightness(50, brightness).WithRemoteRead(func(context.Context) (int32, error) {
							time.Sleep(10 * time.Millisecond)
							return 42, nil
						}),
					),
				},
			},
			3: accessory.Basic{
				Identify:         nil,
				Manufacturer:     "Acme",
				Model:            "LEDBulb1,1",
				Name:             "Acme LED Light Bulb",
				SerialNumber:     "099DB48E9E28",
				FirmwareRevision: "1",

				AdditionalServices: []hapip.Service{
					service.Lightbulb(
						characteristic.On(false, nil),
						characteristic.Brightness(100, brightness).WithRemoteRead(func(context.Context) (int32, error) {
							time.Sleep(10 * time.Millisecond)
							return 50, nil
						}),
						characteristic.SetupEndpoints(characteristic.SetupEndpointsResponse{}, setupEntpoints),
					),
				},
			},
		},
	}

	handler.Initialize()
	return handler, n
}

func TestAccessories(t *testing.T) {
	handler, _ := newHandler(nil, nil, nil, nil)

	req := httptest.NewRequest("GET", "/accessories", nil)
	resp := httptest.NewRecorder()
	handler.ServeHTTP(resp, req)

	assert.Equal(t, 200, resp.Code)

	var data struct {
		Accessories []struct {
			AID      uint64 `json:"aid"`
			Services []struct {
				Type            string `json:"type"`
				IID             uint64 `json:"iid"`
				Characteristics []struct {
					Type        string      `json:"type,omitempty"`
					Value       interface{} `json:"value"`
					Permissions []string    `json:"perms,omitempty"`
					Format      string      `json:"format,omitempty"`
					IID         uint64      `json:"iid"`

					Status *hapip.Status `json:"status,omitempty"`

					Description string      `json:"description,omitempty"` // manufacturer description (optional)
					Unit        string      `json:"unit,omitempty"`
					MaxLen      int         `json:"maxLen,omitempty"`
					MinValue    interface{} `json:"minValue,omitempty"`
					MaxValue    interface{} `json:"maxValue,omitempty"`
					MinStep     interface{} `json:"minStep,omitempty"`
					ValidValues interface{} `json:"valid-values,omitempty"`
					ValidRange  interface{} `json:"valid-values-range,omitempty"`
					MaxDataLen  int         `json:"maxDataLen,omitempty"`

					// ev (is subscribed)
					Ev *bool `json:"ev,omitempty"`
				} `json:"characteristics"`
			} `json:"services"`
		} `json:"accessories"`
	}

	err := json.NewDecoder(resp.Body).Decode(&data)
	assert.NilError(t, err)

	// sorted to be deterministic
	sort.Slice(data.Accessories, func(i, j int) bool {
		return data.Accessories[i].AID < data.Accessories[j].AID
	})
	for _, acc := range data.Accessories {
		sort.Slice(acc.Services, func(i, j int) bool {
			return acc.Services[i].IID < acc.Services[j].IID
		})
	}

	t.Run("Accessories", func(t *testing.T) {
		assert.Equal(t, 3, len(data.Accessories))
		t.Log(prettyPrint(t, data.Accessories[2]))
		assert.Equal(t, uint64(1), data.Accessories[0].AID)
		assert.Equal(t, 2, len(data.Accessories[0].Services))
		assert.Equal(t, "3E", data.Accessories[0].Services[0].Type)
		assert.Equal(t, "23", data.Accessories[0].Services[0].Characteristics[3].Type)
	})

	t.Run("identify", func(t *testing.T) {
		identify := data.Accessories[0].Services[0].Characteristics[0]
		t.Log(prettyPrint(t, identify))
		assert.Equal(t, "14", identify.Type)
		assert.Equal(t, nil, identify.MinValue)
		assert.Equal(t, 1, len(identify.Permissions))
	})

	t.Run("brightness", func(t *testing.T) {
		brightness := data.Accessories[1].Services[1].Characteristics[1]
		t.Log(prettyPrint(t, brightness))
		assert.Equal(t, uint64(10), brightness.IID)
		assert.Equal(t, "8", brightness.Type)
		assert.Equal(t, "int", brightness.Format)
		assert.Equal(t, 3, len(brightness.Permissions))
		assert.Equal(t, float64(100), brightness.MaxValue)
		assert.Equal(t, float64(1), brightness.MinStep)
		assert.Equal(t, float64(0), brightness.MinValue)
		assert.Equal(t, float64(42), brightness.Value)
	})

	t.Run("setup", func(t *testing.T) {
		setup := data.Accessories[2].Services[1].Characteristics[2]
		t.Log(prettyPrint(t, setup))
		assert.Equal(t, uint64(11), setup.IID)
		assert.Equal(t, "118", setup.Type)
		assert.Equal(t, "tlv8", setup.Format)
		assert.Equal(t, "AQACAQADDQEBAAIAAwIAAAQCAAAEBwEBAAIAAwAFBwEBAAIAAwAGBAAAAAAHBAAAAAA=", setup.Value)
	})
	// TODO: valid-values, valid-values-range, maxDataLen
}

func TestGetCharacteristic(t *testing.T) {
	handler, notifier := newHandler(nil, nil, nil, nil)

	type characteristic struct {
		Characteristics []struct {
			Type        string      `json:"type,omitempty"`
			Value       interface{} `json:"value"`
			Permissions []string    `json:"perms,omitempty"`
			Format      string      `json:"format,omitempty"`
			AID         uint64      `json:"aid"`
			IID         uint64      `json:"iid"`

			Status *hapip.Status `json:"status,omitempty"`

			Description string      `json:"description,omitempty"` // manufacturer description (optional)
			Unit        string      `json:"unit,omitempty"`
			MaxLen      int         `json:"maxLen,omitempty"`
			MinValue    interface{} `json:"minValue,omitempty"`
			MaxValue    interface{} `json:"maxValue,omitempty"`
			MinStep     interface{} `json:"minStep,omitempty"`
			ValidValues interface{} `json:"valid-values,omitempty"`
			ValidRange  interface{} `json:"valid-values-range,omitempty"`
			MaxDataLen  int         `json:"maxDataLen,omitempty"`

			// ev (is subscribed)
			Ev *bool `json:"ev,omitempty"`
		} `json:"characteristics"`
	}

	getCharData := func(id string) characteristic {
		req := httptest.NewRequest("GET", "/characteristics?id="+id, nil)
		resp := httptest.NewRecorder()
		handler.ServeHTTP(resp, req)
		if resp.Code != 207 {
			assert.Equal(t, 200, resp.Code)
		}
		var data characteristic
		err := json.NewDecoder(resp.Body).Decode(&data)
		assert.NilError(t, err)
		if resp.Code == 207 { // multi-status
			for _, c := range data.Characteristics {
				assert.Check(t, c.Status != nil)
			}
		}
		return data
	}

	t.Run("one characteristic", func(t *testing.T) {
		data := getCharData("2.9")
		assert.Equal(t, 1, len(data.Characteristics))

		on := data.Characteristics[0]
		t.Log(prettyPrint(t, on))
		assert.Equal(t, uint64(2), on.AID)
		assert.Equal(t, uint64(9), on.IID)
		assert.Equal(t, false, on.Value)
	})
	t.Run("two characteristics", func(t *testing.T) {
		data := getCharData("2.9,3.10")
		assert.Equal(t, 2, len(data.Characteristics))

		brightness := data.Characteristics[1]
		t.Log(prettyPrint(t, brightness))
		assert.Equal(t, uint64(3), brightness.AID)
		assert.Equal(t, uint64(10), brightness.IID)
		assert.Equal(t, float64(50), brightness.Value)
	})

	t.Run("one characteristic with meta, ev, perms, type", func(t *testing.T) {
		data := getCharData("3.10&meta=1&ev=1&perms=1&type=1")
		assert.Equal(t, 1, len(data.Characteristics))

		brightness := data.Characteristics[0]
		t.Log(prettyPrint(t, brightness))
		assert.Equal(t, uint64(3), brightness.AID)
		assert.Equal(t, uint64(10), brightness.IID)
		assert.Equal(t, float64(50), brightness.Value)
		// meta
		assert.Equal(t, "int", brightness.Format)
		assert.Equal(t, float64(100), brightness.MaxValue)
		assert.Equal(t, float64(1), brightness.MinStep)
		assert.Equal(t, float64(0), brightness.MinValue)
		// ev
		assert.Equal(t, false, *brightness.Ev)
		// perms
		assert.Equal(t, 3, len(brightness.Permissions))
		// type
		assert.Equal(t, "8", brightness.Type)
	})
	t.Run("one characteristic with ev", func(t *testing.T) {
		data := getCharData("3.10&ev=1")
		assert.Equal(t, 1, len(data.Characteristics))

		brightness := data.Characteristics[0]
		t.Log(prettyPrint(t, brightness))
		assert.Equal(t, uint64(3), brightness.AID)
		assert.Equal(t, uint64(10), brightness.IID)
		assert.Equal(t, float64(50), brightness.Value)
		// ev
		assert.Equal(t, false, *brightness.Ev)

		// meta
		assert.Equal(t, "", brightness.Format)
		assert.Equal(t, nil, brightness.MaxValue)
		assert.Equal(t, nil, brightness.MinStep)
		assert.Equal(t, nil, brightness.MinValue)
		// ev
		assert.Equal(t, false, *brightness.Ev)
		// perms
		assert.Check(t, nil == brightness.Permissions)
		// type
		assert.Equal(t, "", brightness.Type)
	})
	notifier.subscribed[hapip.CharacteristicID{AID: 3, IID: 10}] = true
	t.Run("one characteristic with ev (subscribed)", func(t *testing.T) {
		data := getCharData("3.10&ev=1")
		assert.Equal(t, 1, len(data.Characteristics))

		brightness := data.Characteristics[0]
		t.Log(prettyPrint(t, brightness))
		assert.Equal(t, uint64(3), brightness.AID)
		assert.Equal(t, uint64(10), brightness.IID)
		assert.Equal(t, float64(50), brightness.Value)
		// ev
		assert.Equal(t, true, *brightness.Ev)
	})
	t.Run("two characteristic (one non existing)", func(t *testing.T) {
		data := getCharData("3.10,42.123")
		assert.Equal(t, 2, len(data.Characteristics))

		brightness := data.Characteristics[0]
		t.Log(prettyPrint(t, brightness))
		assert.Equal(t, uint64(3), brightness.AID)
		assert.Equal(t, uint64(10), brightness.IID)
		assert.Equal(t, hapip.Status(0), *brightness.Status)

		nonExisting := data.Characteristics[1]
		t.Log(prettyPrint(t, nonExisting))
		assert.Equal(t, uint64(42), nonExisting.AID)
		assert.Equal(t, uint64(123), nonExisting.IID)
		assert.Equal(t, hapip.Status(-70410), *nonExisting.Status)
	})
	notifier.err = errors.New("test")
	t.Run("two characteristic with ev error", func(t *testing.T) {
		data := getCharData("2.9,3.10&ev=1")
		assert.Equal(t, 2, len(data.Characteristics))

		brightness := data.Characteristics[0]
		t.Log(prettyPrint(t, brightness))
		assert.Equal(t, uint64(2), brightness.AID)
		assert.Equal(t, uint64(9), brightness.IID)
		assert.Equal(t, hapip.Status(-70402), *brightness.Status)
	})
}

func TestPutCharacteristic(t *testing.T) {
	onCalled := 0
	tlv8Called := 0
	handler, notifier := newHandler(nil, func(b bool) error {
		onCalled++
		assert.Equal(t, b, true)
		return nil
	}, nil, func(ser characteristic.SetupEndpointsRequest) (characteristic.SetupEndpointsResponse, error) {
		t.Log(ser)
		assert.Equal(t, 3, 3)
		tlv8Called++
		return characteristic.SetupEndpointsResponse{
			SessionID:     ser.SessionID,
			Status:        0, // success
			AccessoryAddr: ser.ControllerAddr,
			VideoSRTP:     ser.VideoSRTP,
			AudioSRTP:     ser.AudioSRTP,
			VideoSSRC:     0,
			AudioSSRC:     0,
		}, nil
	})

	type charac struct {
		Characteristics []struct {
			Type        string       `json:"type,omitempty"`
			Value       *interface{} `json:"value,omitempty"`
			Permissions []string     `json:"perms,omitempty"`
			Format      string       `json:"format,omitempty"`
			AID         uint64       `json:"aid"`
			IID         uint64       `json:"iid"`

			Status *hapip.Status `json:"status,omitempty"`

			Description string      `json:"description,omitempty"` // manufacturer description (optional)
			Unit        string      `json:"unit,omitempty"`
			MaxLen      int         `json:"maxLen,omitempty"`
			MinValue    interface{} `json:"minValue,omitempty"`
			MaxValue    interface{} `json:"maxValue,omitempty"`
			MinStep     interface{} `json:"minStep,omitempty"`
			ValidValues interface{} `json:"valid-values,omitempty"`
			ValidRange  interface{} `json:"valid-values-range,omitempty"`
			MaxDataLen  int         `json:"maxDataLen,omitempty"`

			// ev (is subscribed)
			Ev *bool `json:"ev,omitempty"`
		} `json:"characteristics"`
	}

	putCharData := func(body string) *charac {
		req := httptest.NewRequest("PUT", "/characteristics", strings.NewReader(body))
		resp := httptest.NewRecorder()
		handler.ServeHTTP(resp, req)
		if resp.Code == 204 {
			return nil
		}
		assert.Equal(t, 207, resp.Code)
		var data charac
		err := json.NewDecoder(resp.Body).Decode(&data)
		assert.NilError(t, err)
		for _, c := range data.Characteristics {
			assert.Check(t, c.Status != nil)
		}
		return &data
	}

	t.Run("one characteristic", func(t *testing.T) {
		data := putCharData(`{"characteristics": [{
			"aid" : 2,
			"iid" : 9,
			"value" : true
		}]}`)
		assert.Check(t, nil == data)
	})
	t.Run("two characteristics, one inexisting", func(t *testing.T) {
		data := putCharData(`{"characteristics": [{
			"aid" : 2,
			"iid" : 9,
			"value" : true
		},{
			"aid" : 42,
			"iid" : 123,
			"value" : true
		}]}`)
		assert.Equal(t, 2, len(data.Characteristics))

		brightness := data.Characteristics[0]
		t.Log(prettyPrint(t, brightness))
		assert.Equal(t, uint64(2), brightness.AID)
		assert.Equal(t, uint64(9), brightness.IID)
		assert.Equal(t, hapip.Status(0), *brightness.Status)

		nonExisting := data.Characteristics[1]
		t.Log(prettyPrint(t, nonExisting))
		assert.Equal(t, uint64(42), nonExisting.AID)
		assert.Equal(t, uint64(123), nonExisting.IID)
		assert.Equal(t, hapip.Status(-70410), *nonExisting.Status)
	})
	t.Run("one characteristic subscribe", func(t *testing.T) {
		assert.Check(t, !notifier.subscribed[hapip.CharacteristicID{AID: 2, IID: 9}])
		data := putCharData(`{"characteristics": [{
			"aid" : 2,
			"iid" : 9,
			"ev" : true
		}]}`)
		assert.Check(t, nil == data)
		assert.Check(t, notifier.subscribed[hapip.CharacteristicID{AID: 2, IID: 9}])
	})

	t.Run("write TLV8", func(t *testing.T) {
		req, err := tlv8.Marshal(characteristic.SetupEndpointsRequest{
			SessionID: []byte{1, 2, 3},
		})
		assert.NilError(t, err)
		value := base64.StdEncoding.EncodeToString(req)
		data := putCharData(`{"characteristics": [{
				"aid" : 3,
				"iid" : 11,
				"value" : "` + value + `"
			}]}`)
		assert.Check(t, nil == data, prettyPrint(t, data))
		assert.Equal(t, tlv8Called, 1)
	})
}

func prettyPrint(t *testing.T, data any) string {
	buf, err := json.MarshalIndent(data, "", "	")
	assert.NilError(t, err)
	return string(buf)
}
