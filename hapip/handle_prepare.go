package hapip

import (
	"encoding/json"
	"net/http"
	"sync"
	"time"
)

type timedWrite struct {
	mu       sync.Mutex
	PID      uint64
	Deadline time.Time
}

func (tw *timedWrite) isValid(pid *uint64) bool {
	tw.mu.Lock()
	defer tw.mu.Unlock()

	if tw.Deadline.IsZero() {
		return pid == nil
	}
	isTooLate := time.Since(tw.Deadline) > 0
	if isTooLate {
		return pid == nil
	}

	// within the deadline:
	// expect the right pid
	rightPID := pid != nil && *pid == tw.PID
	if rightPID {
		tw.Deadline = time.Time{}
		return true
	}
	return false
}

func (h Handler) putPrepare(rw http.ResponseWriter, req *http.Request) error {
	var timedWrite struct {
		TTL time.Duration `json:"ttl"`
		PID uint64        `json:"pid"`
	}
	err := json.NewDecoder(req.Body).Decode(&timedWrite)
	if err != nil {
		rw.WriteHeader(http.StatusBadRequest)
		return err
	}

	tw := h.tw
	tw.mu.Lock()
	tw.PID = timedWrite.PID
	tw.Deadline = time.Now().Add(timedWrite.TTL * time.Millisecond)
	tw.mu.Unlock()

	return WriteStatus(rw, http.StatusOK, StatusSuccess)
}
