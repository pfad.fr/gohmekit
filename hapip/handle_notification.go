package hapip

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"strconv"

	"code.pfad.fr/gohmekit/hapip/notification"
)

type characteristicEventData struct {
	Aid          uint16      `json:"aid"`
	Iid          uint32      `json:"iid"`
	Value        interface{} `json:"value"`
	initialValue interface{}
}

func (ced characteristicEventData) WriteTo(w io.Writer) (int64, error) {
	em := eventMessage{
		Characteristics: []characteristicEventData{ced},
	}
	return em.WriteTo(w)
}

type coalescableCharacteristicEventData struct {
	characteristicEventData
}

func (cced coalescableCharacteristicEventData) Coalesce(ev io.WriterTo) (notification.Coalescer, error) {
	em := eventMessage{
		Characteristics: []characteristicEventData{cced.characteristicEventData},
	}
	return em.Coalesce(ev)
}

type eventMessage struct {
	Characteristics []characteristicEventData `json:"characteristics"`
}

func (em eventMessage) Coalesce(ev io.WriterTo) (notification.Coalescer, error) {
	var ced characteristicEventData
	switch typed := ev.(type) {
	case characteristicEventData:
		ced = typed
	case coalescableCharacteristicEventData:
		ced = typed.characteristicEventData
	default:
		return em, fmt.Errorf("unexpected type to coalesce: %T", ev)
	}

	for i, c := range em.Characteristics {
		if c.Aid == ced.Aid && c.Iid == ced.Iid {
			if c.initialValue == ced.Value {
				// remove since the current value is not new at all
				return em.removed(i), nil
			}
			// overwrite current value
			em.Characteristics[i].Value = ced.Value
			return em, nil
		}
	}

	em.Characteristics = append(em.Characteristics, ced)
	return em, nil
}

func (em eventMessage) removed(i int) eventMessage {
	s := em.Characteristics

	s[i] = s[len(s)-1]
	em.Characteristics = s[:len(s)-1]
	return em
}

func (em eventMessage) WriteTo(w io.Writer) (int64, error) {
	if len(em.Characteristics) == 0 {
		return 0, nil
	}

	body, err := json.Marshal(em)
	if err != nil {
		return 0, err
	}

	buf := &bytes.Buffer{}

	buf.WriteString("EVENT/1.0 200 OK\n")
	buf.WriteString("Content-Type: application/hap+json\n")
	buf.WriteString("Content-Length: " + strconv.Itoa(len(body)) + "\n")
	buf.WriteByte('\n')
	buf.Write(body)
	n, err := w.Write(buf.Bytes())
	// fmt.Println(string(body))

	return int64(n), err
}
